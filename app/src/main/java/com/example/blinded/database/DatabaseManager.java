package com.example.blinded.database;

import android.content.Context;

public class DatabaseManager {
    private static DatabaseSQLITE instance;
    public static synchronized DatabaseSQLITE getHelper(Context context)
    {
        if (instance == null) {
            instance = new DatabaseSQLITE(context);
            instance.getWritableDatabase();
        }
        return instance;
    }
}
