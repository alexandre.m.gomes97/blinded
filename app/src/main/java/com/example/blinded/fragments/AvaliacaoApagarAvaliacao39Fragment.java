package com.example.blinded.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

public class AvaliacaoApagarAvaliacao39Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Disciplina mDisciplina;
    private Avaliacao mAvaliacao;
    private Speaker sk;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        mDisciplina = bundle.getParcelable("dis");
        mAvaliacao = bundle.getParcelable("ava");
        sk = new Speaker(getContext());


        View rootView = inflater.inflate(R.layout.fragment_avaliacao_apagaravaliacao_39, container, false);

        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_39);
        title.setText( mDisciplina.getNome());

        final TextView titleAval = rootView.findViewById(R.id.textViewNomeDaAvaliacao_39);
        titleAval.setText( mAvaliacao.getNome());

        Button cl = rootView.findViewById(R.id.buttonApagarAvaliacao39);
        cl.setOnClickListener(this);

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        sk.useSpeaker("Apagar avaliação.");
        cfaInterface.fragmentoAtual(39);
    }

    @Override
    public void onStop() {
        super.onStop();
        sk.tts.stop();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.buttonApagarAvaliacao39) {// TODO: Alterar fragmento destino
            cfaInterface.esperarPorConfirmacao( 25, 2 );
        }
    }


    public void setAvaliacaoApagarAvaliacao39Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }

    /**
     * A simple {@link Fragment} subclass.
     * Use the {@link InfoDisciplinaFragment#newInstance} factory method to
     * create an instance of this fragment.
     */
    public static class InfoDisciplinaFragment extends Fragment {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private static final String ARG_PARAM1 = "param1";
        private static final String ARG_PARAM2 = "param2";

        // TODO: Rename and change types of parameters
        private String mParam1;
        private String mParam2;

        public InfoDisciplinaFragment() {
            // Required empty public constructor
        }

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment InfoDisciplinaFragment.
         */
        // TODO: Rename and change types and number of parameters
        public static InfoDisciplinaFragment newInstance(String param1, String param2) {
            InfoDisciplinaFragment fragment = new InfoDisciplinaFragment();
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, param1);
            args.putString(ARG_PARAM2, param2);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                mParam1 = getArguments().getString(ARG_PARAM1);
                mParam2 = getArguments().getString(ARG_PARAM2);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.fragment_info_disciplina, container, false);
        }
    }
}
