package com.example.blinded.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Disciplina implements Parcelable {

    private Integer id;
    private String nome;
    private String sigla;
    private Date data;
    private Integer image;
    private String descricao;
    private String ano_letivo;


    /**
     * Constructor
     * @param nome
     * @param data
     * @param image
     * @param descricao
     */

    public Disciplina() {
    }

    public Disciplina(Integer id, String nome, Date data, Integer image, String descricao) {
        this.id = id;
        this.nome = nome;
        this.data = data;
        this.image = image;
        this.descricao = descricao;
    }

    public Disciplina(Integer id, String nome, Date data, String ano_letivo, String descricao) {
        this.id = id;
        this.nome = nome;
        this.data = data;
        this.ano_letivo = ano_letivo;
        this.descricao = descricao;
    }

    public Disciplina(Integer id, String nome, Date data, String descricao) {
        this.id = id;
        this.nome = nome;
        this.data = data;
        this.descricao = descricao;
    }

    public Disciplina(Integer id, String nome, String sigla, Date data, Integer image, String descricao, String ano_letivo) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
        this.data = data;
        this.image = image;
        this.descricao = descricao;
        this.ano_letivo = ano_letivo;
    }

    /**
     * Gtters & Setters
     *
     */

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getAno_letivo() {
        return ano_letivo;
    }

    public void setAno_letivo(String ano_letivo) {
        this.ano_letivo = ano_letivo;
    }

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getNome() { return nome; }

    public void setNome(String nome) { this.nome = nome; }

    public Date getData() { return data; }

    public void setData(Date data) { this.data = data; }

    public Integer getImage() { return image; }

    public void setImage(Integer image) { this.image = image; }

    public String getDescricao() { return descricao; }

    public void setDescricao(String descricao) { this.descricao = descricao; }


    /**
     * Implementações - parcelable
     *
     */


    protected Disciplina(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        nome = in.readString();
        if (in.readByte() == 0) {
            image = null;
        } else {
            image = in.readInt();
        }
    }

    public static final Creator<Disciplina> CREATOR = new Creator<Disciplina>() {
        @Override
        public Disciplina createFromParcel(Parcel in) {
            return new Disciplina(in);
        }

        @Override
        public Disciplina[] newArray(int size) {
            return new Disciplina[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(nome);
        if (image == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(image);
        }
    }
}
