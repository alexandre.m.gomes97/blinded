package com.example.blinded.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import com.example.blinded.R;
import com.example.blinded.adapters.ListaDisciplinasAdapter;
import com.example.blinded.database.DatabaseManager;
import com.example.blinded.interfaces.FromListDisciplinasAdapter;
import com.example.blinded.interfaces.VoltarParaAActivityInformacao;
import com.example.blinded.model.AvaliacoesAluno;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;
import com.example.blinded.viewModels.AlunoViewModel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DisciplinaAlunoActivity extends AppCompatActivity implements FromListDisciplinasAdapter, VoltarParaAActivityInformacao {

    private ViewPager viewPager;
    private List<Disciplina> listaDisciplina;
    private AlunoViewModel mAlunoViewModel;
    private SharedPreferences mSharedPreferences;
    private static final String SHARED_PREFS = "sharedPreferences";
    public static final String TEXT = "idDoUtilizador";
    private Integer fragmentoAtual;

    private Button btnHelpDisciplinaAluno;
    private Button btnGetBack;
    private Speaker sk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disciplina_aluno);
        viewPager = findViewById(R.id.viewPagerDisciplinas);

        mSharedPreferences = getSharedPreferences( SHARED_PREFS, MODE_PRIVATE);

        mAlunoViewModel = new AlunoViewModel(DatabaseManager.getHelper(this.getApplicationContext()));

        sk = new Speaker(this);

        try {
            listaDisciplina = mAlunoViewModel.obterInscricoesDoAluno( Integer.valueOf( mSharedPreferences.getString(TEXT, "") ) );
        } catch (ParseException e) {
            e.printStackTrace();
        }

        viewPager.setAdapter(new ListaDisciplinasAdapter(this,listaDisciplina, this, this));
        btnGetBack = findViewById(R.id.btnBackInicioAvaliacao3);
        btnGetBack.setClickable(false);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a37));
                btnGetBack.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);
    }

    @Override
    public void onAttachFragment(@NonNull Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    public void onclickBtnHelpAvalicao(View view){
        sk.useSpeaker(getResources().getString(R.string.a34));
        while ((sk.tts.isSpeaking()));
    }


    public void onClickVerAvaliacoes(View view) {

    }

    @Override
    protected void onStop(){
        super.onStop();

        sk.tts.stop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        sk.tts.stop();
    }

    public void onClickHelp(View view){
        btnGetBack.setClickable(false);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a37));
                btnGetBack.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);
    }

    @Override
    public void buttonCodeFragmentCodeEDisciplinaId(Integer button, Integer fragment, Integer disciplinaId) {

        switch (button)
        {
            case 0:
                // Chamar o ver avaliações da disciplina
                //mAlunoViewModel.obterAvaliacoesPorDisciplina(disciplinaId);

                Intent intent = new Intent(getApplicationContext(), ListaDeAvaliacoesActivity.class);
                intent.putExtra("disciplinaSelecionada", disciplinaId);
                startActivity(intent);

                break;
            case 1:
                // Chamar o desiscrever da disciplina
                mAlunoViewModel.desinscreverALunoDeDisciplina( Integer.valueOf( mSharedPreferences.getString(TEXT,"") ) , disciplinaId);
                // TODO: Colocar dialog de cancelamento com clicque no ecra nos proxmos 5 segundos.
                break;

        }

    }

    @Override
    public void voltar(Integer fragmentoAtual) {

    }

    @Override
    public void fragmentoAtualInfo(Integer fragment) {
        fragmentoAtual = fragment;
    }
}
