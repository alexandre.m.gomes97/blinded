package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.AvaliacaoVerInscricoesInscrito331Fragment;
import com.example.blinded.fragments.Informacao000Fragment;
import com.example.blinded.model.Aluno;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;

import java.util.ArrayList;

public class IncricoesSwipeAdapter extends FragmentStatePagerAdapter {
    private Avaliacao mAvaliacao;
    private Disciplina mDisciplina;
    private ArrayList<Aluno> mAlunos;

    public IncricoesSwipeAdapter(FragmentManager fm, Avaliacao a, Disciplina d, ArrayList<Aluno> aluno) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mAvaliacao = a;
        mDisciplina = d;
        mAlunos = aluno;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if ( mAlunos == null || mAlunos.size() == 0 ) {
            Informacao000Fragment pagina000 = new Informacao000Fragment();
            Bundle bundle00 = new Bundle();
            bundle00.putString( "data", "Esta avaliação ainda não tem qualquer inscrito." );
            bundle00.putInt( "fragAtual",331);
            pagina000.setArguments(bundle00);
            return pagina000;
        } else {
            AvaliacaoVerInscricoesInscrito331Fragment pagina331 = new AvaliacaoVerInscricoesInscrito331Fragment();
            Bundle bundle331 = new Bundle();
            bundle331.putParcelable("dis", mDisciplina);
            bundle331.putParcelable("ava", mAvaliacao);
            bundle331.putParcelable("alu", mAlunos.get(position));
            pagina331.setArguments(bundle331);
            return pagina331;
        }

    }




    @Override
    public int getCount() {
            if ( mAlunos == null || mAlunos.size() == 0 )
                return 1;
            else
                return mAlunos.size();
    }
}
