package com.example.blinded.adapters;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.DisciplinaCriada22Fragment;
import com.example.blinded.fragments.ProfessorCriarDisciplina201Fragment;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

import java.util.ArrayList;

public class DisciplinasSwipeAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Disciplina> mDisciplinas;
    private Speaker sk;

    public DisciplinasSwipeAdapter(FragmentManager fm, ArrayList<Disciplina> d, Context c) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mDisciplinas = d;
        sk = new Speaker(c);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        if ( position == mDisciplinas.size() ) {
            ProfessorCriarDisciplina201Fragment pagina201 = new ProfessorCriarDisciplina201Fragment();
            return pagina201;
        } else {
            DisciplinaCriada22Fragment pagina22 = new DisciplinaCriada22Fragment();
            Bundle bundle22 = new Bundle();
            bundle22.putParcelable("dis", mDisciplinas.get(position));

            pagina22.setArguments(bundle22);
            return pagina22;
        }

    }


    @Override
    public int getCount() {
        return mDisciplinas.size() + 1 ;
    }
}

