package com.example.blinded.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Handler;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.blinded.R;
import com.example.blinded.activity.RegistarActivity;
import com.example.blinded.model.Speaker;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegistarEmailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegistarEmailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private EditText et;
    private TextView textView;
    private Speaker sk;

    public RegistarEmailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegistarEmailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegistarEmailFragment newInstance(String param1, String param2) {
        RegistarEmailFragment fragment = new RegistarEmailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_registar_email, container, false);
        sk = new Speaker(getContext());
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a13));
            }
        }, 1000);
        et = rootView.findViewById(R.id.editTextEmailRegistar);

        textView = rootView.findViewById(R.id.textViewRegistarEmail);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sk.useSpeaker("Diga o email, para usar no registo da aplicação, um segundo após a frase terminar.");

                while(sk.tts.isSpeaking()){}

                getSpeechInput(v);
            }
        });
//        Button button = rootView.findViewById(R.id.btnNext);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    ((RegistarActivity)getActivity()).setComponent(et.getText().toString(), 2);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//            }
//        });

        // Inflate the layout for this fragment
        return rootView;
    }

    public String manageClick() {
        return et.getText().toString();
    }

    // Metodo para receber o conteudo do microfone e transformar em texto
    public void getSpeechInput(View view){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        startActivityForResult(intent,10);
    }

    public void speakerA12OnClick(View view)
    {
        sk.useSpeaker(getResources().getString(R.string.a13));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 10:
                if(resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String[] aux = result.get(0).split(" ",2);

                    et.setText(aux[0] + aux[1]);
                }
                break;
        }
    }

}
