package com.example.blinded.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import  com.example.blinded.R;
import com.example.blinded.adapters.PesquisarDisciplinaAdapter;
import com.example.blinded.database.DatabaseManager;
import com.example.blinded.interfaces.FragmentToActivityCode;
import com.example.blinded.model.AulaAluno;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;
import com.example.blinded.viewModels.AlunoViewModel;
import com.example.blinded.viewModels.UtilizadorViewModel;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class PesquisarDisciplinaAlunoActivity extends AppCompatActivity implements FragmentToActivityCode {


    private ViewPager viewPager;
    private List<Disciplina> listaDisplinas;
    private AlunoViewModel mAlunoViewModel;
    private static final String SHARED_PREFS = "sharedPreferences";
    public static final String TEXT = "idDoUtilizador";
    private SharedPreferences sharedPreferences;
    private String loggedUser;
    private Speaker sk;
    private Button varBtnBackPesquisarDisciplinaAluno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesquisar_disciplina_aluno);
        viewPager = findViewById(R.id.viewPagerPesquisarDisciplinas);

        sharedPreferences = getSharedPreferences( SHARED_PREFS, MODE_PRIVATE);

        mAlunoViewModel = new AlunoViewModel(DatabaseManager.getHelper(this));

        loggedUser = sharedPreferences.getString(TEXT, "");


        // Obter todas as disciplinas existentes
        listaDisplinas = mAlunoViewModel.obterTodasAsDisciplinasEmQueOUtilizadorNaoEstaInscrito( Integer.valueOf( loggedUser ) );//obterTodasAsDisciplinas();

        if (listaDisplinas == null || listaDisplinas.size() == 0) {
            // TODO: Criar e fazer o start de um fragmento ou de uma activity que indiue que não existe qualquer disciplina em que o utilizador se consiga inscrever, no sistema
            TextInputLayout textInputLayout = findViewById(R.id.textInputLayout);
            textInputLayout.setVisibility( View.INVISIBLE);
            TextView txb = findViewById(R.id.txb);
            txb.setText("O sistema não tem qualquer disciplina para ser apresentada.");
            txb.setGravity(Gravity.CENTER);
        } else {
            // Enviar dados para a view e instacialas
            viewPager.setAdapter(new PesquisarDisciplinaAdapter(this,listaDisplinas, this));
        }

        sk = new Speaker(this);
        varBtnBackPesquisarDisciplinaAluno = findViewById(R.id.btnBackPesquisarDisciplinaAluno);
        varBtnBackPesquisarDisciplinaAluno.setClickable(false);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a36));
                varBtnBackPesquisarDisciplinaAluno.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);
    }

    public void onClickHelp(View view)
    {
        varBtnBackPesquisarDisciplinaAluno.setClickable(false);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a36));
                varBtnBackPesquisarDisciplinaAluno.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        listaDisplinas = mAlunoViewModel.obterTodasAsDisciplinasEmQueOUtilizadorNaoEstaInscrito( Integer.valueOf( loggedUser ) );//obterTodasAsDisciplinas();
        viewPager.setAdapter(new PesquisarDisciplinaAdapter(this,listaDisplinas, this));
    }

    @Override
    public void fragmentCodeEDisciplinaId(Integer fragment, Integer disciplinaId) {

        switch (fragment)
        {
            case 14:
                // Increver o aluno logado na disciplina com o id disciplinaId
                String loggedUser1 = sharedPreferences.getString(TEXT, "");

                if ( !loggedUser1.isEmpty()) {
                    mAlunoViewModel.isncreverAluno(Integer.valueOf(loggedUser1), disciplinaId);
                    Intent intent = new Intent( getBaseContext(), DisciplinaAlunoActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getBaseContext().startActivity(intent);
                } else
                    Toast.makeText(this, "impossível inscrevr aluno", Toast.LENGTH_LONG).show();
                break;
        }

    }
}
