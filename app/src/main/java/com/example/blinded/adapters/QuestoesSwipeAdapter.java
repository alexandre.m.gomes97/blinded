package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.AvaliacaoApagarAvaliacao39Fragment;
import com.example.blinded.fragments.AvaliacaoEditarAvaliacao38Fragment;
import com.example.blinded.fragments.AvaliacaoQuestao27Fragment;
import com.example.blinded.fragments.AvaliacaoVerInscricoes33Fragment;
import com.example.blinded.fragments.AvaliacaoVerQuestoes26Fragment;
import com.example.blinded.fragments.Informacao000Fragment;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.QuestaoAluno;

import java.util.ArrayList;

public class QuestoesSwipeAdapter extends FragmentStatePagerAdapter {


    private Avaliacao mAvaliacao;
    private Disciplina mDisciplina;
    private ArrayList<QuestaoAluno> mQuestoes;

    public QuestoesSwipeAdapter(FragmentManager fm, Avaliacao a, Disciplina d, ArrayList<QuestaoAluno> q ) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mAvaliacao = a;
        mDisciplina = d;
        mQuestoes = q;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        if ( mQuestoes == null || mQuestoes.size() == 0 ) {
            Informacao000Fragment pagina000 = new Informacao000Fragment();
            Bundle bundle00 = new Bundle();
            bundle00.putString( "data", "Esta avaliação ainda não tem qualquer questão" );
            bundle00.putInt( "fragAtual", 27);
            pagina000.setArguments(bundle00);
            return pagina000;
        } else {
            AvaliacaoQuestao27Fragment pagina27 = new AvaliacaoQuestao27Fragment();
            Bundle bundle27 = new Bundle();
            bundle27.putParcelable("dis", mDisciplina);
            bundle27.putParcelable("ava", mAvaliacao);
            bundle27.putParcelable("que", mQuestoes.get(position));
            pagina27.setArguments(bundle27);
            return pagina27;
        }
    }




    @Override
    public int getCount() {
        if ( mQuestoes == null || mQuestoes.size() == 0 )
            return 1;
        else
            return mQuestoes.size();
    }
}
