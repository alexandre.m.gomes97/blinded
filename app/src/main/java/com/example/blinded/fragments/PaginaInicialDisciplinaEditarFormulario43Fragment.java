package com.example.blinded.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;
import com.google.android.material.textfield.TextInputEditText;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class PaginaInicialDisciplinaEditarFormulario43Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Disciplina mDisciplina;
    private TextInputEditText tietNome, tietDescricao;
    private Speaker sk;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sk = new Speaker(getContext());
        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        mDisciplina = bundle.getParcelable("dis");

        View rootView = inflater.inflate(R.layout.fragment_pagina_inicial_disciplina_editar_formulario_43, container, false);

        TextView tvDiscipline = rootView.findViewById( R.id.textViewNomeDaDisciplinaDepoisDeCriada_43 );
        tvDiscipline.setText(mDisciplina.getNome());

        tietNome = rootView.findViewById( R.id.textInputEditTextDisciplina_43 );
        tietNome.setText(mDisciplina.getNome());

        tietDescricao = rootView.findViewById( R.id.textInputEditTextDescricaoDisciplina_43 );
        tietDescricao.setText(mDisciplina.getDescricao());

        Button cl = rootView.findViewById(R.id.buttonConfirm);
        cl.setOnClickListener(this);
        TextView textView = rootView.findViewById(R.id.textViewHitBoxEditarDisciplina);
        textView.setOnClickListener(this);

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        sk.useSpeaker("Para editar a disciplina com os dados apresentados, clique no fundo do ecrã.");
        cfaInterface.fragmentoAtual( 43 );
    }

    @Override
    public void onPause() {
        super.onPause();
        sk.tts.stop();
    }

    @Override
    public void onClick(View v) {

        switch ( v.getId() )
        {
            case R.id.buttonConfirm:
                sk.tts.stop();
                sk.useSpeaker(" O novo nome da disciplina é," + tietNome.getText().toString() +  ", e a nova descrição é ,"  + tietDescricao.getText().toString());
                while(sk.tts.isSpeaking());
                Disciplina disciplina = new Disciplina( mDisciplina.getId(), tietNome.getText().toString(), mDisciplina.getData(), mDisciplina.getImage(), tietDescricao.getText().toString() );
                try {

                    cfaInterface.updateOrCreateDisciplina( disciplina );
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.textViewHitBoxEditarDisciplina:
                sk.tts.stop();
                try{
                    Thread.sleep(200);
                }catch (Exception e){ }

                sk.useSpeaker("Diga o novo nome da disciplina, um segundo após esta frase terminar.");
                while(sk.tts.isSpeaking()){}
                getSpeechInput(v);
                break;

        }

    }

    public void setPaginaInicialDisciplinaEditarFormulario43Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }


    public void getSpeechInput(View view){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        startActivityForResult(intent,10);
    }

    public void getSpeechInput2(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        startActivityForResult(intent,20);
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 10:
                if(resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    tietNome.setText(result.get(0));
                    sk.useSpeaker("Agora diga a nova descrição, um segundo após esta frase terminar");
                    while(sk.tts.isSpeaking()){}
                    getSpeechInput2();
                }
                break;
            case 20:
                if(resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    tietDescricao.setText(result.get(0));
                }
                break;
        }
    }

}
