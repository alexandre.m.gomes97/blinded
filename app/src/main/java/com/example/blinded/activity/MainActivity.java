package com.example.blinded.activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import com.example.blinded.R;
import com.example.blinded.database.DatabaseManager;
import com.example.blinded.database.DatabaseSQLITE;
import com.example.blinded.model.Speaker;

import java.text.ParseException;

public class MainActivity extends AppCompatActivity {

    private Button btnRegistar;
    private Button btnAutenticar;
    private Speaker tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        DatabaseSQLITE db = DatabaseManager.getHelper(this.getApplicationContext());
//        try {
//            db.populateDatabase();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        tts = new Speaker(this);
        // Inicializar as variaveis
        btnRegistar = findViewById(R.id.btnRegistarProfessor);
        btnAutenticar = findViewById(R.id.btnAutenticar);
        btnRegistar.setClickable(false);
        btnAutenticar.setClickable(false);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tts.useSpeaker(getResources().getString(R.string.a1));
                btnRegistar.setClickable(true);
                btnAutenticar.setClickable(true);//speak after 1000ms
            }
        }, 1000);
    }

    @Override
    protected void onStop(){
        super.onStop();
        tts.tts.stop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        tts.tts.stop();
    }

    public void onClick(View view) {
        Intent intent = new Intent(getApplicationContext(), RegistarActivity.class);
        startActivity(intent);
    }

    public void onClickAutenticar(View view) {
        Intent intent = new Intent(getApplicationContext(), AutenticarActivity.class);
        startActivity(intent);
    }

    public void onClickHelp(View view){
        tts.useSpeaker(getResources().getString(R.string.a2));
    }
}
