package com.example.blinded.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.QuestaoAluno;
import com.example.blinded.model.Speaker;
import com.google.android.material.textfield.TextInputEditText;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class AvaliacaoCriarAvaliacaoSelecionarNome411Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity mCfa;
    private EditText mEditTextNomeDaDisciplina;
    private Button mButton;
    private Avaliacao mAvaliacao;
    private TextView labelCriar;
    private Speaker sk;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        Disciplina d = bundle.getParcelable("dis");
        mAvaliacao = getArguments().getParcelable("ava");

        View rootView = inflater.inflate(R.layout.avaliacao_criar_avaliacao_selecionar_nome_411, container, false);

        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_411);
        title.setText( d.getNome());

        mEditTextNomeDaDisciplina = rootView.findViewById(R.id.textInputEditTextDisciplina_411);
        if ( mAvaliacao != null )
            mEditTextNomeDaDisciplina.setText( mAvaliacao.getNome() );

        mButton = rootView.findViewById(R.id.buttonConfirm411);
        mButton.setOnClickListener(this);
        sk = new Speaker(getContext());
        labelCriar = rootView.findViewById(R.id.textViewHitBox);
        labelCriar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sk.useSpeaker("Diga o nome da avaliação a criar.");
                while(sk.tts.isSpeaking()){}
                getSpeechInput(v);
            }
        });
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        sk.useSpeaker("Clique no centro do ecrã, quando quiser dizer o nome da avaliação.");
        mCfa.fragmentoAtual(411);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.buttonConfirm411:
                sk.useSpeaker("A avaliação " + mEditTextNomeDaDisciplina.getText().toString() + " foi criada com sucesso!");
                String s = mEditTextNomeDaDisciplina.getText().toString();
                try {

                    if ( mAvaliacao != null )
                        mCfa.nomeSelecionado( s, 1 ); // editar
                    else
                        mCfa.nomeSelecionado( s, 0 ); // Criar
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if ( mAvaliacao != null) {
                    try {
                        mCfa.alterarFragmento(31);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        mCfa.alterarFragmento(25);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }


    public void getSpeechInput(View view){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        startActivityForResult(intent,10);
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 10:
                if(resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    mEditTextNomeDaDisciplina.setText(result.get(0));
                }
                break;
        }
    }


    public void setAvaliacaoCriarAvaliacaoSelecionarData411(Disciplina20Activity disciplina20Activity) {
        mCfa = disciplina20Activity;
    }
}
