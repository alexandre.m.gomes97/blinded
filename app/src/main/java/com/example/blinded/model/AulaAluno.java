package com.example.blinded.model;

import java.io.Serializable;

public class AulaAluno implements Serializable {
    private String titulo;

    public AulaAluno(String _titulo){
        titulo = _titulo;
    }

    public String getTitulo(){return titulo;}
    public void setTitulo(String titulo){this.titulo = titulo;}
}
