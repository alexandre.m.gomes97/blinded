package com.example.blinded.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.blinded.R;
import com.example.blinded.model.Speaker;

import java.util.ArrayList;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginUsernameFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class LoginUsernameFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private EditText et;
    private TextView lbT;
    Speaker sk;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginUsernameFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginUsernameFragment newInstance(String param1, String param2) {
        LoginUsernameFragment fragment = new LoginUsernameFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public LoginUsernameFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_registar_username, container, false);

        et = rootView.findViewById(R.id.editText);
        lbT = rootView.findViewById(R.id.textViewRegistarNome);
        sk = new Speaker(getContext());
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a12));
            }
        }, 1000);
         // Abrir o metodo para ao ser ciclado o butao ativar o voice mode
        lbT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sk.useSpeaker("Diga o nome, que irá usar para se registar na aplicação, um segundo após a frase terminar.");

                while(sk.tts.isSpeaking()){}

                getSpeechInput(v);
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    public void speakerA12OnClick(View view)
    {
        sk.useSpeaker(getResources().getString(R.string.a12));
    }

    @Override
    public void onStop(){
        super.onStop();

        sk.tts.stop();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        sk.tts.stop();
    }

    public String manageClick() {
        return et.getText().toString();
    }

    // Metodo para receber o conteudo do microfone e transformar em texto
    public void getSpeechInput(View view){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        startActivityForResult(intent,10);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 10:
                if(resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    et.setText(result.get(0));
                }
                break;
        }
    }
}
