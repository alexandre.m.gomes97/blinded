package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.AvaliacaoQuestaoAdicionarQuestao32Fragment;
import com.example.blinded.fragments.AvaliacaoQuestaoEditar30Fragment;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.QuestaoAluno;

public class EditarQuestaoSwipeAdapter extends FragmentStatePagerAdapter {

    private Avaliacao mAvaliacao;
    private Disciplina mDisciplina;
    private QuestaoAluno mQuestaoAluno;

    public EditarQuestaoSwipeAdapter(FragmentManager fm, Avaliacao a, Disciplina d, QuestaoAluno q) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mAvaliacao = a;
        mDisciplina = d;
        mQuestaoAluno = q;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        AvaliacaoQuestaoEditar30Fragment pagina30 = new AvaliacaoQuestaoEditar30Fragment();
        Bundle bundle30 = new Bundle();
        bundle30.putParcelable( "dis", mDisciplina );
        bundle30.putParcelable( "ava", mAvaliacao );
        bundle30.putParcelable( "que", mQuestaoAluno );
        pagina30.setArguments(bundle30);
        return pagina30;

    }




    @Override
    public int getCount() {
        return 1;
    }
}
