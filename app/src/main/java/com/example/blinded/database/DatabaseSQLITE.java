package com.example.blinded.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.blinded.StringDateParser;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.QuestaoAluno;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class DatabaseSQLITE extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "BlindEdDataBase.db";
    private static final String UTILIZADOR_TABLE_NAME = "Utilizador";
    private static final String DISCIPLINA_TABLE_NAME = "Dsciplina";
    private static final String AVALIACAO_TABLE_NAME = "Avaliacao";
    private static final String QUESTAO_TABLE_NAME = "Questao";
    private static final String RESPOSTA_TABLE_NAME = "Resposta";
    private static final String CORRECCAO_TABLE_NAME = "Correccao";
    private static final String QUESTAO_AVALIACAO_TABLE_NAME = "Questao_avaliacao";
    private static final String DISCIPLINA_AVALIACAO_TABLE_NAME = "Disciplina_avaliacao";
    private static final String UTILIZADOR_DISCIPLINA_TABLE_NAME = "Utilizador_disciplina";
    private static final String UTILIZADOR_AVALIACAO_TABLE_NAME = "Utilizador_avaliacao";
    private static final Integer DATABASE_VERSION = 1;


    public DatabaseSQLITE(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + UTILIZADOR_TABLE_NAME +
                        "(utilizador_Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "utilizador_Nome TEXT, " +
                        "utilizador_Email TEXT," +
                        "utilizador_Data_de_nascimento DATE," +
                        "utilizador_Tipo INTEGER," + // 0 => Professor, 1 => Aluno
                        "utilizador_Password TEXT)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + DISCIPLINA_TABLE_NAME +
                        "(disciplina_Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "disciplina_Nome TEXT," +
                        "disciplina_Sigla TEXT," +
                        "disciplina_Data_de_Criacao DATE," +
                        "disciplina_Ano_letivo TEXT," +
                        "disciplina_Descricao TEXT, " +
                        "disciplina_Criada_por_utilizadorId INTEGER)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + AVALIACAO_TABLE_NAME +
                        "(avaliacao_Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "avaliacao_Nome TEXT," +
                        "avaliacao_Data_da_Avaliacao DATE," +
                        "avaliacao_Percentagem INTEGER, " + // Peso da avaliação na disciplina
                        "avaliacao_Criada_por_utilizadorId INTEGER)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + QUESTAO_TABLE_NAME +
                        "(questao_Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "questao_Descricao TEXT," +
                        "questao_Numero INTEGER," +
                        "questao_Titulo TEXT," +
                        "questao_Cotacao FLOAT," +
                        "questao_Tipo INTEGER," + // Escolha múltipla, desenvolvimento...
                        "questao_Descricao_do_tipo TEXT," +
                        "questao_Criada_por_utilizadorId INTEGER)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + RESPOSTA_TABLE_NAME +
                        "(resposta_Id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "resposta_Resposta TEXT," +
                        "resposta_UtilizadorId INTEGER, " + // Associacao a um aluno
                        "resposta_QuestaoId INTEGER)"  // Associação a uma questão
        );

        // Associação de um utilizador a uma disciplina. O utilizador tanto pode ser um Professor como um aluno
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + UTILIZADOR_DISCIPLINA_TABLE_NAME +
                        "(u_d_Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "u_d_UtilizadorId INTEGER," +
                        "u_d_DisciplinaId INTEGER)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + DISCIPLINA_AVALIACAO_TABLE_NAME +
                        "(d_a_Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "d_a_DisciplinaId INTEGER," +
                        "d_a_AvaliacaoId INTEGER)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + QUESTAO_AVALIACAO_TABLE_NAME +
                        "(q_a_Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "q_a_QuestaoId INTEGER," +
                        "q_a_AvaliacaoId INTEGER)"
        );
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + UTILIZADOR_AVALIACAO_TABLE_NAME +
                        "(u_a_Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "u_a_UtilizadorId INTEGER," +
                        "u_a_AvaliacaoId INTEGER, " +
                        "u_a_Resultado FLOAT DEFAULT -1)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + CORRECCAO_TABLE_NAME +
                        "(correcao_Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "correcao_RespostaId INTEGER," +
                        "correcao_Cotacao FLOAT," + // Dada depois da avaliação do professor (No caso de haver multiplas correções de múltiplos professor (diferentes cotações), apenas é contada a última)
                        "correcao_UtilizadorId INTEGER, "+ // Professor que a corrigiu (pode haver correções feitas por vários professores)
                        "correcao_Descricao TEXT, " + // No caso do professor querer adicionar algum reparo.
                        "correcao_Data DATE)" // Data da correção
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void populateDatabase() throws ParseException {

        // Inserir novos utilizadores
        long idAluno = insertUtilizador("Carlos Maria", "carlos_maria@outlook.pt", new Date(), 1, "DEDDDEDE");
        long idProfessor = insertUtilizador("Professor Manuel Pereira", "jojo@benfas.pt", new Date(), 0, "DEDDDEDE");

        // Inserir novas disciplinas
        long idDisciplinaIPC = insertDisciplina("Interação Pessoa-Computador", "IPC", new Date(), "2012/2013", "Esta é uma descrição da disciplina", (int) idProfessor );
        long idDisciplinaSD = insertDisciplina("Sistemas Distribuidos", "SD", new Date(), "20/21", "Sistemas distribuídos é muito fixe! sqn", (int) idProfessor );

        // Inserir novas avaliações
        long idAvaliacaoSD1 = insertAvaliacao("Primeira avaliação",  new GregorianCalendar(2020, 10, 2).getTime(), 50, (int) idProfessor );
        long idAvaliacaoSD2 = insertAvaliacao("Segunda avaliacção",  new GregorianCalendar(2020, 12, 20).getTime(), 50, (int) idProfessor );

        // Inseriar associação de disciplina a avaliacao;
        insertDisciplina_Avaliacao( (int) idDisciplinaIPC, (int) idAvaliacaoSD1 );
        insertDisciplina_Avaliacao( (int) idDisciplinaIPC, (int) idAvaliacaoSD2 );

        // Inserir associação de aluno a disciplina;
        insertUtilizador_Disciplina( (int) idAluno, (int) idDisciplinaIPC );
        insertUtilizador_Disciplina( (int) idAluno, (int) idDisciplinaSD );
        insertUtilizador_Disciplina( (int) idProfessor, (int) idDisciplinaSD );

    }



    // GET secção

    public ContentValues getUtilizadorPorNomeEPassword( String nome, String password) {

        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] whereArgs={ nome,password };
        Cursor res = db.rawQuery("select * from " + UTILIZADOR_TABLE_NAME + " where utilizador_Nome = ? AND password = ?", whereArgs);
        res.moveToFirst();

        contentValues.put("utilizador_Id", res.getInt(res.getColumnIndex("utilizador_Id")));
        contentValues.put("utilizador_Nome", res.getString(res.getColumnIndex("utilizador_Nome")));
        contentValues.put("utilizador_Email", res.getString(res.getColumnIndex("utilizador_Email")));
        contentValues.put("utilizador_Data_de_nascimento", res.getString(res.getColumnIndex("utilizador_Data_de_nascimento")));
        contentValues.put("utilizador_Tipo", res.getInt(res.getColumnIndex("utilizador_Tipo")));
        contentValues.put("utilizador_Password", res.getString(res.getColumnIndex("utilizador_Password")));

        res.close();
        db.close();

        if (res.getCount() != 0)
            return contentValues;
        else
            return null;
    }


    // Obter todos os utilizadores por disciplina
    public List<ContentValues> getUtilizadoresPorDisciplinaId( String disciplinaId ) {

        ArrayList<ContentValues> listContentValues = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] whereArgs={ disciplinaId };
        Cursor res = db.rawQuery("select u_d_UtilizadorId from " + UTILIZADOR_DISCIPLINA_TABLE_NAME + " WHERE u_d_DisciplinaId  = ?", whereArgs);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            contentValues = new ContentValues();
            contentValues.put("u_d_UtilizadorId", res.getInt(res.getColumnIndex("u_d_UtilizadorId")));
            listContentValues.add(contentValues);
            res.moveToNext();
        }


        ArrayList<ContentValues> listaDoConteudoDosUtilizadores = new ArrayList<>();
        for (ContentValues cv: listContentValues
        ) {
            // Get dos valores associados à disciplina à qual está associado o id obtido em cima
            whereArgs[0] = cv.getAsString("u_d_UtilizadorId");
            res = db.rawQuery("select * from " + UTILIZADOR_TABLE_NAME + " WHERE utilizador_Id = ?", whereArgs);
            res.moveToFirst();
            contentValues = new ContentValues();
            contentValues.put("utilizador_Id", res.getInt(res.getColumnIndex("utilizador_Id")));
            contentValues.put("utilizador_Nome", res.getString(res.getColumnIndex("utilizador_Nome")));
            contentValues.put("utilizador_Email", res.getString(res.getColumnIndex("utilizador_Email")));
            contentValues.put("utilizador_Data_de_nascimento", res.getString(res.getColumnIndex("utilizador_Data_de_nascimento")));
            contentValues.put("utilizador_Tipo", res.getInt(res.getColumnIndex("utilizador_Tipo")));
            contentValues.put("utilizador_Password", res.getString(res.getColumnIndex("utilizador_Password")));
            listaDoConteudoDosUtilizadores.add(contentValues);
        }

        res.close();
        db.close();

        return listaDoConteudoDosUtilizadores;

    }

    public ContentValues getUtilizadorPorId(String utilizadorId ) {

        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] whereArgs={ utilizadorId };
        Cursor res = db.rawQuery("select * from " + UTILIZADOR_TABLE_NAME + " where utilizador_Id = ?", whereArgs);
        res.moveToFirst();

        contentValues.put("utilizador_Id", res.getInt(res.getColumnIndex("utilizador_Id")));
        contentValues.put("utilizador_Nome", res.getString(res.getColumnIndex("utilizador_Nome")));
        contentValues.put("utilizador_Email", res.getString(res.getColumnIndex("utilizador_Email")));
        contentValues.put("utilizador_Data_de_nascimento", res.getString(res.getColumnIndex("utilizador_Data_de_nascimento")));
        contentValues.put("utilizador_Tipo", res.getInt(res.getColumnIndex("utilizador_Tipo")));
        contentValues.put("utilizador_Password", res.getString(res.getColumnIndex("utilizador_Password")));

        res.close();
        db.close();

        return contentValues;
    }


    // Obter todas as disciplinas
    public List<ContentValues> getAllDisciplinas( ) {

        ArrayList<ContentValues> listContentValues = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        Cursor res = db.rawQuery("select * from " + DISCIPLINA_TABLE_NAME + " WHERE 1", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            contentValues = new ContentValues();
            contentValues.put("disciplina_Id", res.getInt(res.getColumnIndex("disciplina_Id")));
            contentValues.put("disciplina_Nome", res.getString(res.getColumnIndex("disciplina_Nome")));
            contentValues.put("disciplina_Sigla", res.getString(res.getColumnIndex("disciplina_Sigla")));
            contentValues.put("disciplina_Data_de_Criacao", res.getString(res.getColumnIndex("disciplina_Data_de_Criacao")));
            contentValues.put("disciplina_Ano_letivo", res.getString(res.getColumnIndex("disciplina_Ano_letivo")));
            contentValues.put("disciplina_Descricao", res.getString(res.getColumnIndex("disciplina_Descricao")));
            listContentValues.add(contentValues);
            res.moveToNext();
        }

        res.close();
        db.close();

        return listContentValues;

    }


    public List<ContentValues> getDisciplinasPorUtilizadorId( String utilizadorId ) {
        // Get do id de cada disciplina associada ao utilizadador cujo Id foi passado por parametro
        ArrayList<ContentValues> listContentValues = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] whereArgs={ utilizadorId };
        Cursor res = db.rawQuery("select * from " + UTILIZADOR_DISCIPLINA_TABLE_NAME + " WHERE u_d_UtilizadorId = ?", whereArgs);

        if (res.getCount() == 0) // Este utilizador não tem nenhuma disciplina associada
            return listContentValues;

        res.moveToFirst();
        while (!res.isAfterLast()) {
            contentValues = new ContentValues();
            contentValues.put("u_d_DisciplinaId", res.getInt(res.getColumnIndex("u_d_DisciplinaId")));
            listContentValues.add(contentValues);
            res.moveToNext();
        }


        ArrayList<ContentValues> listaDoConteudoDasDisciplinas = new ArrayList<>();
        for (ContentValues cv: listContentValues
             ) {
            // Get dos valores associados à disciplina à qual está associado o id obtido em cima
            whereArgs[0] = cv.getAsString("u_d_DisciplinaId");
            res = db.rawQuery("select * from " + DISCIPLINA_TABLE_NAME + " WHERE disciplina_Id = ?", whereArgs);
            res.moveToFirst();
            contentValues = new ContentValues();
            contentValues.put("disciplina_Id", res.getInt(res.getColumnIndex("disciplina_Id")));
            contentValues.put("disciplina_Nome", res.getString(res.getColumnIndex("disciplina_Nome")));
            contentValues.put("disciplina_Sigla", res.getString(res.getColumnIndex("disciplina_Sigla")));
            contentValues.put("disciplina_Data_de_Criacao", res.getString(res.getColumnIndex("disciplina_Data_de_Criacao")));
            contentValues.put("disciplina_Ano_letivo", res.getString(res.getColumnIndex("disciplina_Ano_letivo")));
            contentValues.put("disciplina_Descricao", res.getString(res.getColumnIndex("disciplina_Descricao")));
            listaDoConteudoDasDisciplinas.add(contentValues);
        }

        res.close();
        db.close();

        return listaDoConteudoDasDisciplinas;

    }

    // Obter todas as avaliacoes associadas a uma disciplina
    public List<ContentValues> getAvaliacoesPorDisciplinaId( String disciplinaId ) {

        // Get do id de cada disciplina associada ao utilizadador cujo Id foi passado por parametro
        ArrayList<ContentValues> listContentValues = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] whereArgs={ disciplinaId };
        Cursor res = db.rawQuery("select * from " + DISCIPLINA_AVALIACAO_TABLE_NAME + " WHERE d_a_DisciplinaId = ?", whereArgs);

        if (res.getCount() == 0)
            return listContentValues;

        res.moveToFirst();
        while (!res.isAfterLast()) {
            contentValues = new ContentValues();
            contentValues.put("d_a_AvaliacaoId", res.getInt(res.getColumnIndex("d_a_AvaliacaoId")));
            listContentValues.add(contentValues);
            res.moveToNext();
        }


        ArrayList<ContentValues> listaDoConteudoDasAvaliacoes = new ArrayList<>();
        for (ContentValues cv: listContentValues
        ) {
            // Get dos valores associados à disciplina à qual está associado o id obtido em cima
            whereArgs[0] = cv.getAsString("d_a_AvaliacaoId");
            res = db.rawQuery("select * from " + AVALIACAO_TABLE_NAME + " WHERE avaliacao_Id = ?", whereArgs);
            res.moveToFirst();



            contentValues = new ContentValues();
            contentValues.put("avaliacao_Id", res.getInt(res.getColumnIndex("avaliacao_Id")));
            contentValues.put("avaliacao_Nome", res.getString(res.getColumnIndex("avaliacao_Nome")));
            contentValues.put("avaliacao_Data_da_Avaliacao", res.getString(res.getColumnIndex("avaliacao_Data_da_Avaliacao")));
            contentValues.put("avaliacao_Percentagem", res.getInt(res.getColumnIndex("avaliacao_Percentagem")));
            contentValues.put("avaliacao_Criada_por_utilizadorId", res.getInt(res.getColumnIndex("avaliacao_Criada_por_utilizadorId")));
            listaDoConteudoDasAvaliacoes.add(contentValues);
        }

        res.close();
        db.close();

        return listaDoConteudoDasAvaliacoes;
    }

    // Retorna todas as avaliaoes a que um utilizador está isncrito, dentro de uma disciplina especifica
    public List<ContentValues> getAvaliacoesPorDisciplinaIdEUtilizadorId( String disciplinaId, String utilizadorId ) {

        // Get do id de cada disciplina associada ao utilizadador cujo Id foi passado por parametro
        ArrayList<ContentValues> listContentValues = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] whereArgs={ disciplinaId };
        Cursor resAvaliacao = db.rawQuery("select d_a_AvaliacaoId from " + DISCIPLINA_AVALIACAO_TABLE_NAME + " WHERE d_a_DisciplinaId = ?", whereArgs); // Obter todos os id associados às avaliaoes da disciplina cujo id foi passado como parâmetro.

        if (resAvaliacao.getCount() == 0)
            return listContentValues;

        String[] whereArgsDois = {"",""};
        resAvaliacao.moveToFirst();
        while (!resAvaliacao.isAfterLast()) {
            whereArgsDois[0] = utilizadorId;
            whereArgsDois[1] = resAvaliacao.getString(resAvaliacao.getColumnIndex("d_a_AvaliacaoId"));
            Cursor resUtilizador = db.rawQuery("select u_a_AvaliacaoId from " + UTILIZADOR_AVALIACAO_TABLE_NAME  + " WHERE u_a_UtilizadorId = ? AND u_a_AvaliacaoId = ?", whereArgsDois); // Obter todos os id associados às avaliaoes da disciplina cujo id foi passado como parâmetro.

            while (!resUtilizador.isAfterLast())
            {
                ContentValues cv = new ContentValues();
                cv.put("u_a_AvaliacaoId", resUtilizador.getInt(resUtilizador.getColumnIndex("u_a_AvaliacaoId")));
                resUtilizador.moveToNext();
                listContentValues.add(cv);
            }
            resAvaliacao.moveToNext();
        }

        Cursor res = null;
        ContentValues contentValues;
        ArrayList<ContentValues> listaDoConteudoDasAvaliacoes = new ArrayList<>();
        for (ContentValues cv: listContentValues
        ) {
            // Get dos valores associados à disciplina à qual está associado o id obtido em cima
            whereArgs[0] = cv.getAsString("u_a_AvaliacaoId");
            res = db.rawQuery("select * from " + AVALIACAO_TABLE_NAME + " WHERE avaliacao_Id = ?", whereArgs);
            res.moveToFirst();
            contentValues = new ContentValues();
            contentValues.put("avaliacao_Id", res.getInt(res.getColumnIndex("avaliacao_Id")));
            contentValues.put("avaliacao_Nome", res.getString(res.getColumnIndex("avaliacao_Nome")));
            contentValues.put("avaliacao_Data_da_Avaliacao", res.getString(res.getColumnIndex("avaliacao_Data_da_Avaliacao")));
            contentValues.put("avaliacao_Percentagem", res.getInt(res.getColumnIndex("avaliacao_Percentagem")));
            contentValues.put("avaliacao_Criada_por_utilizadorId", res.getInt(res.getColumnIndex("avaliacao_Criada_por_utilizadorId")));
            listaDoConteudoDasAvaliacoes.add(contentValues);
        }


        resAvaliacao.close();
        if (res != null)
            res.close();

        db.close();

        return listaDoConteudoDasAvaliacoes;
    }







    // Obter todas as questoes associadas a uma avaliacao
    public List<ContentValues> getQuestoesPorAvaliacaoId( String avaliacaoId ) {

        // Get do id de cada disciplina associada ao utilizadador cujo Id foi passado por parametro
        ArrayList<ContentValues> listContentValues = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] whereArgs={ avaliacaoId };
        Cursor res = db.rawQuery("select q_a_QuestaoId from " + QUESTAO_AVALIACAO_TABLE_NAME + " WHERE q_a_AvaliacaoId = ?", whereArgs);

        if (res == null || res.getCount() == 0)
            return listContentValues;

        res.moveToFirst();
        while (!res.isAfterLast()) {
            contentValues = new ContentValues();
            contentValues.put("q_a_QuestaoId", res.getInt(res.getColumnIndex("q_a_QuestaoId")));
            listContentValues.add(contentValues);
            res.moveToNext();
        }


        ArrayList<ContentValues> listaDoConteudoDasQuestoes = new ArrayList<>();
        for (ContentValues cv: listContentValues
        ) {
            // Get dos valores associados à disciplina à qual está associado o id obtido em cima
            whereArgs[0] = cv.getAsString("q_a_QuestaoId");
            res = db.rawQuery("select * from " + QUESTAO_TABLE_NAME + " WHERE questao_Id = ?", whereArgs);
            res.moveToFirst();
            contentValues = new ContentValues();
            contentValues.put("questao_Id", res.getInt(res.getColumnIndex("questao_Id")));
            contentValues.put("questao_Descricao", res.getString(res.getColumnIndex("questao_Descricao")));
            contentValues.put("questao_Numero", res.getInt(res.getColumnIndex("questao_Numero")));
            contentValues.put("questao_Titulo", res.getString(res.getColumnIndex("questao_Titulo")));
            contentValues.put("questao_Cotacao", res.getFloat(res.getColumnIndex("questao_Cotacao")));
            contentValues.put("questao_Tipo", res.getInt(res.getColumnIndex("questao_Tipo")));
            contentValues.put("questao_Descricao_do_tipo", res.getString(res.getColumnIndex("questao_Descricao_do_tipo")));
            contentValues.put("questao_Criada_por_utilizadorId", res.getInt(res.getColumnIndex("questao_Criada_por_utilizadorId")));
            listaDoConteudoDasQuestoes.add(contentValues);
        }

        res.close();
        db.close();

        return listaDoConteudoDasQuestoes;
    }


    // Obtém todas as respostas de um aluno numa avaliação
    public List<ContentValues> getRespostasPorAluno(String utilizadorId, String avaliacaoId) {
        ArrayList<ContentValues> listContentValues = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] whereArgs1 = { utilizadorId };
        String[] whereArgs2 = { avaliacaoId };

        // Obtemos os IDs das questões da avaliação
        Cursor res = db.rawQuery("SELECT q_a_QuestaoId FROM " + QUESTAO_AVALIACAO_TABLE_NAME + " WHERE q_a_AvaliacaoId = ?", whereArgs2);

        if (res.getCount() == 0)
            return listContentValues;

        res.moveToFirst();
        while (!res.isAfterLast()) {
            contentValues = new ContentValues();
            contentValues.put("q_a_QuestaoId", res.getInt(res.getColumnIndex("q_a_QuestaoId")));
            listContentValues.add(contentValues);
            res.moveToNext();
        }

        // Obtemos a resposta para cada questão
        ArrayList<ContentValues> listaDasRespostas = new ArrayList<>();
        for (ContentValues cv: listContentValues) {
            String[] whereArgs3 = {cv.getAsString("q_a_QuestaoId"), utilizadorId};
            res = db.rawQuery("SELECT * FROM " + RESPOSTA_TABLE_NAME + " WHERE resposta_QuestaoId = ? AND resposta_UtilizadorId = ?", whereArgs3);
            res.moveToFirst();
            contentValues = new ContentValues();
            contentValues.put("resposta_Id", res.getInt(res.getColumnIndex("resposta_Id")));
            contentValues.put("resposta_Resposta", res.getString(res.getColumnIndex("resposta_Resposta")));
            listaDasRespostas.add(contentValues);
        }

        res.close();
        db.close();

        return listaDasRespostas;
    }


    // Obtém todos os alunos inscritos numa avaliação. Um aluno é considerado inscrito na avaliação se estiver inscrito na disciplina
    public List<ContentValues> getAlunosPorAvaliação(String avaliacaoId) {
        ArrayList<ContentValues> listContentValues = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] whereArgs = { avaliacaoId };
        ContentValues contentValues;

        // Obtemos a disciplina associada com a avaliação
        Cursor res = db.rawQuery("SELECT u_d_DisciplinaId FROM " + DISCIPLINA_AVALIACAO_TABLE_NAME + " WHERE d_a_AvaliacaoId = ?", whereArgs);
        if (res == null) {
            return null;
        }

        res.moveToFirst();
        int idDisciplina = res.getInt(res.getColumnIndex("d_a_DisciplinaId"));

        whereArgs[0] = Integer.toString(idDisciplina);
        res = db.rawQuery("SELECT u_d_UtilizadorId FROM " + UTILIZADOR_DISCIPLINA_TABLE_NAME + " WHERE u_d_DisciplinaId = ?", whereArgs);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            contentValues = new ContentValues();
            contentValues.put("u_d_UtilizadorId", res.getInt(res.getColumnIndex("u_d_UtilizadorId")));
            listContentValues.add(contentValues);
            res.moveToNext();
        }

        ArrayList<ContentValues> listaDeAlunos = new ArrayList<>();
        for (ContentValues cv: listContentValues
        ) {
            // Get dos valores associados à disciplina à qual está associado o id obtido em cima
            whereArgs[0] = cv.getAsString("u_d_UtilizadorId");
            res = db.rawQuery("SELECT * FROM " + UTILIZADOR_TABLE_NAME + " WHERE utilizador_Id = ?", whereArgs);
            res.moveToFirst();
            contentValues = new ContentValues();
            contentValues.put("utilizador_Id", res.getInt(res.getColumnIndex("utilizador_Id")));
            contentValues.put("utilizador_Nome", res.getString(res.getColumnIndex("utilizador_Nome")));
            contentValues.put("utilizador_Email", res.getInt(res.getColumnIndex("utilizador_Email")));
            listaDeAlunos.add(contentValues);
        }

        res.close();
        db.close();

        return listaDeAlunos;
    }


    // TODO: Get Respostas por Avaliacao

    // INSERT secção


    public long insertUtilizador( String utilizador_Nome, String utilizador_Email, Date utilizador_Data_de_nascimento, Integer utilizador_Tipo, String utilizador_Password ) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("utilizador_Nome", utilizador_Nome);
        contentValues.put("utilizador_Email", utilizador_Email);
        contentValues.put("utilizador_Data_de_nascimento", StringDateParser.parseString( utilizador_Data_de_nascimento));
        contentValues.put("utilizador_Tipo", utilizador_Tipo);
        contentValues.put("utilizador_Password", utilizador_Password);
        long id = db.insert(UTILIZADOR_TABLE_NAME, null, contentValues);
        db.close();

        return id;
    }


    public long insertUtilizadorBasic(String utilizador_Nome, String utilizador_Email) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("utilizador_Nome", utilizador_Nome);
        contentValues.put("utilizador_Email", utilizador_Email);
        long id = db.insert(UTILIZADOR_TABLE_NAME, null, contentValues);
        db.close();

        return id;
    }

    public void changeUtilizadorPassword(String utilizadorId, String password) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("utilizador_Password", password);

        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs = { utilizadorId };
        db.update(UTILIZADOR_TABLE_NAME, contentValues, "utilizador_Id = ?", whereArgs);
        db.close();
    }


    public Integer insertDisciplina( String disciplina_Nome, String disciplina_Sigla, Date disciplina_Data_de_Criacao, String disciplina_Ano_letivo, String disciplina_Descricao, Integer  disciplina_Criada_por_utilizadorId ) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("disciplina_Nome", disciplina_Nome);
        contentValues.put("disciplina_Sigla", disciplina_Sigla);
        contentValues.put("disciplina_Data_de_Criacao", StringDateParser.parseString( disciplina_Data_de_Criacao));
        contentValues.put("disciplina_Ano_letivo", disciplina_Ano_letivo);
        contentValues.put("disciplina_Descricao", disciplina_Descricao);
        contentValues.put("disciplina_Criada_por_utilizadorId", disciplina_Criada_por_utilizadorId);
        long id = db.insert(DISCIPLINA_TABLE_NAME, null, contentValues);
        db.close();

        return (int) id;

    }

    // Qualdo o utilizador se inscreve numa avaliacao
    public Integer insertInscricaoUtilizadorAvaliacao( Integer utilizadroId, Integer avaliacaoId ) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("u_a_UtilizadorId", utilizadroId);
        contentValues.put("u_a_AvaliacaoId", avaliacaoId);
        long id = db.insert( UTILIZADOR_AVALIACAO_TABLE_NAME, null, contentValues);
        db.close();

        return (int) id;

    }

    // Adicionar resultado ao utilizador
    public void atualizarInscricaoUtilizadorAvaliacaoResultado( Integer utilizadroId, Integer avaliacaoId, Float resultado ) throws ParseException {
        ContentValues contentValues = new ContentValues();
        contentValues.put("u_a_Resultado", resultado);

        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs = { utilizadroId.toString(), avaliacaoId.toString() };
        db.update(UTILIZADOR_AVALIACAO_TABLE_NAME, contentValues, "u_a_UtilizadorId = ? AND u_a_AvaliacaoId = ?", whereArgs);
        db.close();

    }

    public List<ContentValues> obterResultadosPorAvaliacaoId( Integer avaliacaoId )
    {

        ArrayList<ContentValues> listaDeADisciplinas = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] whereArgs = { avaliacaoId.toString() };
        ContentValues contentValues;

        // Obtemos a disciplina associada com a avaliação
        Cursor resUtilizadorAvaliacao = db.rawQuery("SELECT u_a_UtilizadorId, u_a_Resultado FROM " + UTILIZADOR_AVALIACAO_TABLE_NAME + " WHERE u_a_AvaliacaoId = ? AND u_a_Resultado != -1", whereArgs);
        if (resUtilizadorAvaliacao == null) {
            return listaDeADisciplinas;
        }



        Cursor res = null;
        resUtilizadorAvaliacao.moveToFirst();
        while (!resUtilizadorAvaliacao.isAfterLast()) {
            whereArgs[0] = resUtilizadorAvaliacao.getString(resUtilizadorAvaliacao.getColumnIndex("u_a_UtilizadorId"));
            res = db.rawQuery("SELECT * FROM " + UTILIZADOR_TABLE_NAME  + " WHERE utilizador_Id = ?", whereArgs);
            res.moveToFirst();
            contentValues = new ContentValues();
            contentValues.put("utilizador_Id", res.getInt(res.getColumnIndex("utilizador_Id")));
            contentValues.put("utilizador_Nome", res.getString(res.getColumnIndex("utilizador_Nome")));
            contentValues.put("utilizador_Email", res.getString(res.getColumnIndex("utilizador_Email")));
            contentValues.put("utilizador_Data_de_nascimento", res.getString(res.getColumnIndex("utilizador_Data_de_nascimento")));
            contentValues.put("utilizador_Tipo", res.getInt(res.getColumnIndex("utilizador_Tipo")));
            contentValues.put("utilizador_Password", res.getString(res.getColumnIndex("utilizador_Password")));
            contentValues.put("utilizador_Resultado", resUtilizadorAvaliacao.getFloat(resUtilizadorAvaliacao.getColumnIndex("u_a_Resultado")));
            resUtilizadorAvaliacao.moveToNext();
            listaDeADisciplinas.add(contentValues);
        }

        resUtilizadorAvaliacao.close();
        if (res != null)
            res.close();
        db.close();

        return listaDeADisciplinas;




    }



    public Integer insertAvaliacao( String avaliacao_nome, Date avaliacao_Data_da_Avaliacao, Integer avaliacao_Percentagem, Integer avaliacao_Criada_por_utilizadorId ) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("avaliacao_Nome", avaliacao_nome);
        contentValues.put("avaliacao_Data_da_Avaliacao", StringDateParser.parseString(avaliacao_Data_da_Avaliacao));
        contentValues.put("avaliacao_Percentagem", avaliacao_Percentagem );
        contentValues.put("avaliacao_Criada_por_utilizadorId", avaliacao_Criada_por_utilizadorId);
        long id = db.insert(AVALIACAO_TABLE_NAME, null, contentValues);
        db.close();

        return (int) id;
    }

    public long insertQuestao( String questao_descricao, Integer questao_numero, String questao_titulo, Float questao_cotacao, Integer questao_tipo, String questao_descricao_do_tipo, Integer questao_Criada_por_utilizadorId ) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("questao_Descricao", questao_descricao);
        contentValues.put("questao_Numero", questao_numero);
        contentValues.put("questao_Titulo", questao_titulo );
        contentValues.put("questao_Cotacao", questao_cotacao);
        contentValues.put("questao_Tipo", questao_tipo);
        contentValues.put("questao_Descricao_do_tipo", questao_descricao_do_tipo );
        contentValues.put("questao_Criada_por_utilizadorId", questao_Criada_por_utilizadorId);
        long id = db.insert(QUESTAO_TABLE_NAME, null, contentValues);
        db.close();

        return id;
    }

    public long insertResposta( String resposta_Resposta, Integer resposta_UtilizadorId, Integer resposta_QuestaoId ) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("resposta_Resposta", resposta_Resposta);
        contentValues.put("resposta_UtilizadorId", resposta_UtilizadorId);
        contentValues.put("resposta_QuestaoId", resposta_QuestaoId );
        long id = db.insert( RESPOSTA_TABLE_NAME, null, contentValues);
        db.close();

        return id;
    }

    public void insertCorrecao( Integer correcao_RespostaId, Float correcao_Cotacao, Integer correcao_UtilizadorId, String correcao_Descricao, Date correcao_Data ) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("correcao_RespostaId", correcao_RespostaId);
        contentValues.put("correcao_Cotacao", correcao_Cotacao);
        contentValues.put("correcao_UtilizadorId", correcao_UtilizadorId );
        contentValues.put("correcao_Descricao", correcao_Descricao);
        contentValues.put("correcao_Data", StringDateParser.parseString(correcao_Data) );
        db.insert(CORRECCAO_TABLE_NAME, null, contentValues);
        db.close();
    }

    public void insertQuestao_Avaliacao( Integer q_a_QuestaoId, Integer q_a_AvaliacaoId ) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("q_a_QuestaoId", q_a_QuestaoId);
        contentValues.put("q_a_AvaliacaoId", q_a_AvaliacaoId);
        db.insert(QUESTAO_AVALIACAO_TABLE_NAME, null, contentValues);
        db.close();
    }

    public void insertDisciplina_Avaliacao( Integer disciplina_id, Integer avaliacao_id ) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("d_a_DisciplinaId", disciplina_id);
        contentValues.put("d_a_AvaliacaoId", avaliacao_id);
        db.insert(DISCIPLINA_AVALIACAO_TABLE_NAME, null, contentValues);
        db.close();
    }

    public void insertUtilizador_Disciplina( Integer utilizador_id, Integer disciplina_id ) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("u_d_DisciplinaId", disciplina_id);
        contentValues.put("u_d_UtilizadorId", utilizador_id);
        db.insert(UTILIZADOR_DISCIPLINA_TABLE_NAME, null, contentValues);
        db.close();
    }


    public ContentValues procurarUtilizador(String username, String password) {


        SQLiteDatabase db = this.getReadableDatabase();
        String[] whereArgs={ username, password };

        Cursor res = db.rawQuery("select utilizador_Id, utilizador_Tipo from " + UTILIZADOR_TABLE_NAME + " where ( utilizador_Nome = ? AND utilizador_Password = ?)", whereArgs);
        res.moveToFirst();
        ContentValues cv = new ContentValues();
        while (!res.isAfterLast()) {
            cv.put("utilizador_Id", res.getString(res.getColumnIndex("utilizador_Id")));
            cv.put("utilizador_Tipo", res.getString(res.getColumnIndex("utilizador_Tipo")));
            res.moveToNext();
            break;
        }

        res.close();

        return cv;

    }

    public void changeUtilizadorTipo(String utilizador_id, Integer tipo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("utilizador_Tipo", tipo);

        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs = { utilizador_id };
        db.update(UTILIZADOR_TABLE_NAME, contentValues, "utilizador_Id = ?", whereArgs);
        db.close();
    }

    public void updateDisciplina(Integer id, String nome, String descricao) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("disciplina_Nome", nome);
        contentValues.put("disciplina_Descricao", descricao);

        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs = { id.toString() };
        db.update(DISCIPLINA_TABLE_NAME, contentValues, "disciplina_Id = ?", whereArgs);
        db.close();
    }

    public void desativarDisciplina(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs={ id.toString() };
        db.delete(DISCIPLINA_TABLE_NAME,"disciplina_Id = ?", whereArgs);
    }


    public void desativarAvaliacao(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs={ id.toString() };
        db.delete(AVALIACAO_TABLE_NAME,"avaliacao_Id = ?", whereArgs);
    }

    public void desativarQuestao(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs={ id.toString() };
        db.delete(QUESTAO_TABLE_NAME,"questao_Id = ?", whereArgs);
    }

    public void desativarUtilizadorDisciplina(Integer id, Integer utilizadorId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs={ id.toString() };
        db.delete(UTILIZADOR_DISCIPLINA_TABLE_NAME,"u_d_DisciplinaId = ?", whereArgs);
    }


    public void insertInscricao(Integer alunoId, Integer disciplinId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("u_d_DisciplinaId", disciplinId);
        contentValues.put("u_d_UtilizadorId", alunoId);
        db.insert(UTILIZADOR_DISCIPLINA_TABLE_NAME, null, contentValues);
        db.close();
    }

    public List<ContentValues> obterInscricoesDoAluno(Integer alunoId) {

        ArrayList<ContentValues> listaDeADisciplinas = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] whereArgs = { alunoId.toString() };
        ContentValues contentValues;

        // Obtemos a disciplina associada com a avaliação
        Cursor resUtilizadorDisciplina = db.rawQuery("SELECT u_d_DisciplinaId FROM " + UTILIZADOR_DISCIPLINA_TABLE_NAME + " WHERE u_d_UtilizadorId = ?", whereArgs);
        if (resUtilizadorDisciplina == null) {
            return listaDeADisciplinas;
        }



        Cursor res = null;
        resUtilizadorDisciplina.moveToFirst();
        while (!resUtilizadorDisciplina.isAfterLast()) {
            whereArgs[0] = resUtilizadorDisciplina.getString(resUtilizadorDisciplina.getColumnIndex("u_d_DisciplinaId"));
            res = db.rawQuery("SELECT * FROM " + DISCIPLINA_TABLE_NAME  + " WHERE disciplina_Id = ?", whereArgs);
            res.moveToFirst();
            contentValues = new ContentValues();
            contentValues.put("disciplina_Id", res.getInt(res.getColumnIndex("disciplina_Id")));
            contentValues.put("disciplina_Nome", res.getString(res.getColumnIndex("disciplina_Nome")));
            contentValues.put("disciplina_Sigla", res.getString(res.getColumnIndex("disciplina_Sigla")));
            contentValues.put("disciplina_Data_de_Criacao", res.getString(res.getColumnIndex("disciplina_Data_de_Criacao")));
            contentValues.put("disciplina_Ano_letivo", res.getString(res.getColumnIndex("disciplina_Ano_letivo")));
            contentValues.put("disciplina_Criada_por_utilizadorId", res.getInt(res.getColumnIndex("disciplina_Criada_por_utilizadorId")));
            contentValues.put("disciplina_Descricao", res.getString(res.getColumnIndex("disciplina_Descricao")));
            resUtilizadorDisciplina.moveToNext();
            listaDeADisciplinas.add(contentValues);
        }

        resUtilizadorDisciplina.close();
        if (res != null)
            res.close();
        db.close();

        return listaDeADisciplinas;


    }

    public List<ContentValues> getInscritosPorAvaliacaoId(Integer avaliacaoId) {

        ArrayList<ContentValues> listaDeAvaliacoes = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] whereArgs = { avaliacaoId.toString() };
        ContentValues contentValues;

        // Obtemos a disciplina associada com a avaliação
        Cursor resUtilizadorAvaliacao = db.rawQuery("SELECT u_a_UtilizadorId FROM " + UTILIZADOR_AVALIACAO_TABLE_NAME + " WHERE u_a_AvaliacaoId = ?", whereArgs);
        if (resUtilizadorAvaliacao == null) {
            return listaDeAvaliacoes;
        }

        Cursor res = null;
        resUtilizadorAvaliacao.moveToFirst();
        while (!resUtilizadorAvaliacao.isAfterLast()) {
            whereArgs[0] = resUtilizadorAvaliacao.getString(resUtilizadorAvaliacao.getColumnIndex("u_a_UtilizadorId"));
            res = db.rawQuery("SELECT * FROM " + UTILIZADOR_TABLE_NAME + " WHERE utilizador_Id = ?", whereArgs);
            res.moveToFirst();
            contentValues = new ContentValues();
            contentValues.put("utilizador_Id", res.getInt(res.getColumnIndex("utilizador_Id")));
            contentValues.put("utilizador_Nome", res.getString(res.getColumnIndex("utilizador_Nome")));
            contentValues.put("utilizador_Email", res.getString(res.getColumnIndex("utilizador_Email")));
            contentValues.put("utilizador_Data_de_nascimento", res.getString(res.getColumnIndex("utilizador_Data_de_nascimento")));
            contentValues.put("utilizador_Tipo", res.getInt(res.getColumnIndex("utilizador_Tipo")));
            contentValues.put("utilizador_Password", res.getString(res.getColumnIndex("utilizador_Password")));
            resUtilizadorAvaliacao.moveToNext();
            listaDeAvaliacoes.add(contentValues);
        }

        resUtilizadorAvaliacao.close();
        if (res != null)
            res.close();
        db.close();

        return listaDeAvaliacoes;

    }

    public void atualizarAvaliacao(Integer idDaAvaliacao, String nomeDaAvaliacaoCriada, Date dataSelecionada) {

        ContentValues contentValues = new ContentValues();
        contentValues.put("avaliacao_Nome", nomeDaAvaliacaoCriada);
        contentValues.put("avaliacao_Data_da_Avaliacao", dataSelecionada.toString());

        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs = { idDaAvaliacao.toString() };
        db.update(AVALIACAO_TABLE_NAME, contentValues, "avaliacao_Id = ?", whereArgs);
        db.close();


    }

    public void desativarDisciplinaAvaliacao(Integer avaliacaoId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs={ avaliacaoId.toString() };
        db.delete(DISCIPLINA_AVALIACAO_TABLE_NAME,"d_a_AvaliacaoId = ?", whereArgs);
    }

    public void desativarUtilizadorAvaliacao(Integer avaliacaoId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs={ avaliacaoId.toString() };
        db.delete(UTILIZADOR_AVALIACAO_TABLE_NAME,"u_a_AvaliacaoId = ?", whereArgs);
    }

    public void updateQuestao(Integer idDaQuestao, String sPergunta, String sCotacao) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("questao_Descricao", sPergunta);
        contentValues.put("questao_Titulo", sPergunta);
        contentValues.put("questao_Cotacao", Float.valueOf(sCotacao));

        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs = { idDaQuestao.toString() };
        db.update(QUESTAO_TABLE_NAME, contentValues, "questao_Id = ?", whereArgs);
        db.close();

    }

    public void desativarQuestaoAvaliacaoByAvaliacaoId(Integer avaliacaoId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs={ avaliacaoId.toString() };
        db.delete(QUESTAO_AVALIACAO_TABLE_NAME,"q_a_AvaliacaoId = ?", whereArgs);
    }

    public void desativarQuestaoAvaliacaoPorQuestaoId(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs={ id.toString() };
        db.delete(QUESTAO_AVALIACAO_TABLE_NAME,"q_a_QuestaoId = ?", whereArgs);
    }

    public void desativarRespostaPorQuestaoId(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs={ id.toString() };
        db.delete(RESPOSTA_TABLE_NAME,"resposta_QuestaoId = ?", whereArgs);
    }

    public void DesinscreverAlunoDeDisciplina(Integer idAluno, Integer idDisciplina) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs={ idAluno.toString(), idDisciplina.toString() };
        db.delete(UTILIZADOR_DISCIPLINA_TABLE_NAME,"u_d_UtilizadorId = ? AND u_d_DisciplinaId = ?", whereArgs);
    }

    public void DesinscreverAlunoDeAvaliacao(Integer idAluno, Integer idAvaliacao) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs={ idAluno.toString(), idAvaliacao.toString() };
        db.delete(UTILIZADOR_AVALIACAO_TABLE_NAME,"u_a_UtilizadorId = ? AND u_a_AvaliacaoId = ?", whereArgs);
    }



}

