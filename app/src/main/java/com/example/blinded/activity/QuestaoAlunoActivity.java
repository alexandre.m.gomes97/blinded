package com.example.blinded.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import  com.example.blinded.R;
import com.example.blinded.adapters.PesquisarDisciplinaAdapter;
import com.example.blinded.adapters.QuestoesAlunoAdapter;
import com.example.blinded.database.DatabaseManager;
import com.example.blinded.interfaces.SubmeterResposta;
import com.example.blinded.model.QuestaoAluno;
import com.example.blinded.model.Speaker;
import com.example.blinded.viewModels.AlunoViewModel;
import com.example.blinded.viewModels.ProfessorViewModel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class QuestaoAlunoActivity extends AppCompatActivity implements SubmeterResposta {

    private static final String SHARED_PREFS = "sharedPreferences";
    public static final String TEXT = "idDoUtilizador";
    private Integer idDoUtilziadorLogado;
    private ViewPager viewPager;
    private AlunoViewModel alunoViewModel;
    private List<QuestaoAluno> listaQuestoes;
    private String idDisciplina, idAvaliacao;
    private Speaker sk;
    private Button varBtnDesistirQuestaoAluno, varBtnHelpQuestaoAluno;
    private EditText varEditTextTimesQuestaoAluno;

    private CountDownTimer counter;
    private long timeLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questao_aluno);
        viewPager = findViewById(R.id.viewPagerQuestaoAluno);

        SharedPreferences sharedPreferences = getSharedPreferences( SHARED_PREFS, MODE_PRIVATE);
        idDoUtilziadorLogado = Integer.valueOf( sharedPreferences.getString(TEXT, "") );

        idDisciplina = getIntent().getStringExtra("idDisciplina");
        idAvaliacao = getIntent().getStringExtra("idAvaliacao");

        alunoViewModel = new AlunoViewModel(DatabaseManager.getHelper(this));

        listaQuestoes = alunoViewModel.getQuestoesPorAvaliacaoId( idAvaliacao );

        // Enviar dados para a view e instacialas
        viewPager.setAdapter(new QuestoesAlunoAdapter(this,listaQuestoes, this));

        timeLeft = 600000;
        counter = new CountDownTimer(timeLeft,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeft = millisUntilFinished;

                int minutes = (int) timeLeft / 60000;
                int seconds = (int) timeLeft % 60000 / 1000;

                String timeLeftText = String.valueOf(minutes);
                timeLeftText += ":";
                timeLeftText += String.valueOf(seconds);
                varEditTextTimesQuestaoAluno.setText(timeLeftText);
            }

            @Override
            public void onFinish() {

            }
        }.start();

        sk = new Speaker(this);
        varBtnDesistirQuestaoAluno = findViewById(R.id.btnDesistirQuestaoAluno);
        varBtnDesistirQuestaoAluno.setClickable(false);
        varEditTextTimesQuestaoAluno = findViewById(R.id.editTextTimesQuestaoAluno);
        varEditTextTimesQuestaoAluno.setClickable(false);
        varBtnHelpQuestaoAluno = findViewById(R.id.btnHelpQuestaoAluno);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a40));
                varBtnDesistirQuestaoAluno.setClickable(true);
                varEditTextTimesQuestaoAluno.setClickable(true);//speak after 1000ms
            }
        }, 1000);
    }

    public void onClickHelp(View view)
    {
        varBtnDesistirQuestaoAluno.setClickable(false);
        varBtnHelpQuestaoAluno.setClickable(false);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a40));
                varBtnDesistirQuestaoAluno.setClickable(true);
                varBtnHelpQuestaoAluno.setClickable(true);//speak after 1000ms
            }
        }, 1000);
    }

    public void onClickTimer(View view)
    {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(varEditTextTimesQuestaoAluno.getText().toString());
                varBtnDesistirQuestaoAluno.setClickable(true);
                varEditTextTimesQuestaoAluno.setClickable(true);//speak after 1000ms
            }
        }, 1000);
    }

    public  void onClick(View view){
        Intent intent = new Intent(getApplicationContext(), ListaDeAvaliacoesActivity.class);
        getApplicationContext().startActivity(intent);
    }

    @Override
    public void submeterDadosDaResposta(Integer idQuestao, String resposta) throws ParseException {
        alunoViewModel.inserirResposta( resposta , idDoUtilziadorLogado, Integer.valueOf( idQuestao ) );
    }
}