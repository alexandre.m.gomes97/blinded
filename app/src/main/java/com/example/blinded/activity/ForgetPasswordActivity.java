package com.example.blinded.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;

import com.example.blinded.R;
import com.example.blinded.model.Speaker;

public class ForgetPasswordActivity extends AppCompatActivity {

    private Speaker sk;
    private Button buttonEditar;
    private Button buttonCarinha;
    private Button buttonMensagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        sk = new Speaker(this);

        buttonCarinha = findViewById(R.id.button5);
        buttonEditar = findViewById(R.id.button4);
        buttonMensagem = findViewById(R.id.btnNext2);
        buttonCarinha.setClickable(false);
        buttonMensagem.setClickable(false);
        buttonEditar.setClickable(false);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a17));
                buttonCarinha.setClickable(true);
                buttonEditar.setClickable(true);
                buttonMensagem.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);
    }

    @Override
    protected void onStop(){
        super.onStop();

        sk.tts.stop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        sk.tts.stop();
    }
}
