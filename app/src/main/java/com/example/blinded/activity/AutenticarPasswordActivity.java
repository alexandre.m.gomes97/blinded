package com.example.blinded.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.blinded.R;
import com.example.blinded.database.DatabaseManager;
import com.example.blinded.model.Speaker;
import com.example.blinded.viewModels.UtilizadorViewModel;

import org.w3c.dom.Text;

import java.text.ParseException;

public class AutenticarPasswordActivity extends AppCompatActivity {

    private static final String SHARED_PREFS = "sharedPreferences";
    public static final String TEXT = "idDoUtilizador";
    public static final String TEXT_REGISTERING = "registando";
    private SharedPreferences sharedPreferences;
    private UtilizadorViewModel utilizadorViewModel;
    private String username = null;
    private Speaker sk;
    private Button btnHelpPasswordAutenticar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autenticar_password);
        sk = new Speaker(this);
        sharedPreferences = getSharedPreferences( SHARED_PREFS, MODE_PRIVATE);

        utilizadorViewModel = new UtilizadorViewModel(DatabaseManager.getHelper(this));

        btnHelpPasswordAutenticar=findViewById(R.id.btnHelpPassword2);


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a9));
                btnHelpPasswordAutenticar.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);



        Bundle extras = getIntent().getExtras();
        if ( extras != null )
            username = extras.getString("username");

        if ( username != null )
        {


        }

        Button button = findViewById(R.id.btnPasswordLeft3);
        button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                final EditText text = findViewById(R.id.editTextEmailRegistar3);

                Pair<Integer,Integer> returnExist = utilizadorViewModel.ProcurarUtilizador( username, text.getText().toString()); // Procura na base de dados um utilizador com as credênciais inseridas. Caso não encontre nenhum, retorna false

                if ( returnExist.first == null) // No caso de existir uma entrada na base de dados com as credenciais inseridas pelo utilizador.
                {
                    Toast.makeText(getBaseContext(), "Credenciais inválidas", Toast.LENGTH_LONG).show();
                    sk.useSpeaker("Credenciais inválidas");
                } else {

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(TEXT_REGISTERING, "false");
                    editor.putString(TEXT, returnExist.second.toString());
                    editor.apply();

                    if ( returnExist.first == 1) {
                        // Redirecionar para o menu inicial do aluno
                        Intent intent = new Intent(getApplicationContext(), MainActivityAluno.class);
                        startActivity(intent);
                    } else if ( returnExist.first == 0){
                        // Redirecionar para o menu inicial do Professor
                        Intent intent = new Intent(getApplicationContext(), Disciplina20Activity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getBaseContext(), "Erro na operação de autenticação", Toast.LENGTH_LONG).show();
                    }


                }


                return true;
            }
        });


        Button btnPasswordLeft3 = findViewById(R.id.btnPasswordLeft3);
        btnPasswordLeft3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final EditText text = findViewById(R.id.editTextEmailRegistar3);
                sk.useSpeaker("Esquerda");
                String s = text.getText().toString() + "E";
                text.setText( s );

            }
        });


        Button btnPasswordRight3 = findViewById(R.id.btnPasswordRight3);
        btnPasswordRight3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final EditText text = findViewById(R.id.editTextEmailRegistar3);
                sk.useSpeaker("Direita");
                String s = text.getText().toString() + "D";
                text.setText( s );

            }
        });


    }

    public void onclickBtnPassword2(View view){
        sk.useSpeaker(getResources().getString(R.string.a9));
        while ((sk.tts.isSpeaking()));

    }

    @Override
    protected void onStop(){
        super.onStop();

        sk.tts.stop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        sk.tts.stop();
    }
}
