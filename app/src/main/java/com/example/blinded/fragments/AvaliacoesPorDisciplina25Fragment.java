package com.example.blinded.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

public class AvaliacoesPorDisciplina25Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Avaliacao mAvaliacao;
    private Integer mAvaliacaoId;
    private Speaker sk;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        Disciplina d = bundle.getParcelable("dis");
        Avaliacao a = bundle.getParcelable("ava");
        mAvaliacao = a;
        sk = new Speaker(getContext());
        mAvaliacaoId = a.getId();

        View rootView = inflater.inflate(R.layout.fragment_avaliacoes_por_disciplina_25, container, false);

        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_25);
        title.setText( d.getNome());

//        final ImageView imagem = rootView.findViewById(R.id.imageViewDisciplina_25);
//        imagem.setImageResource( d.getImage() );

        final Button button = rootView.findViewById(R.id.buttonSelecionarAvaliacao25);
        button.setText( a.getNome() );
        button.setOnClickListener(this);

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        sk.useSpeaker("Avaliação: " + mAvaliacao.getNome() );
        cfaInterface.fragmentoAtual( 25 );
    }

    @Override
    public void onPause() {
        super.onPause();
        sk.tts.stop();
    }

    public void setAvaliacoesPorDisciplina25Fragment(Disciplina20Activity disciplina20Activity) {

        cfaInterface = disciplina20Activity;
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.buttonSelecionarAvaliacao25)
            cfaInterface.avaliacaoOnClick( mAvaliacaoId );
    }
}
