package com.example.blinded.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.IpSecManager;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.blinded.R;
import com.example.blinded.interfaces.ComunicarFragmentActivity;

public class ClickToCancelOperationDialog implements View.OnClickListener {

    private ComunicarFragmentActivity mComunicarFragmentActivity;

    private Activity mActivity;
    private AlertDialog mAlertDialog;
    private TextView tv;
    private Boolean isActive = false;

    public ClickToCancelOperationDialog( Activity activity, ComunicarFragmentActivity comunicarFragmentActivity ){
        mActivity = activity;
        mComunicarFragmentActivity = comunicarFragmentActivity;
    }

    public void startLoadingDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        LayoutInflater inflater = mActivity.getLayoutInflater();

        View rootView = inflater.inflate(R.layout.dialog_click_to_cancel_operation, null);

        builder.setView( rootView );
        builder.setCancelable(false);

        mAlertDialog = builder.create();
        mAlertDialog.getWindow().setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT) ); // Remove the background of the alert dialog

        // Move the dialog up
        Window window = mAlertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        rootView.setOnClickListener(this);

        mAlertDialog.show();

        isActive = true;

    }

    public void dismissDialog(){

        mAlertDialog.dismiss();
        isActive = false;
    }


    @Override
    public void onClick(View v) {

        mComunicarFragmentActivity.cancelarOperacao();

    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

}
