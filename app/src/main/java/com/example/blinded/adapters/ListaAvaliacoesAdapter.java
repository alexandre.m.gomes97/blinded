package com.example.blinded.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.example.blinded.R;
import com.example.blinded.activity.InicioAvaliacaoActivity;
import com.example.blinded.interfaces.FromListaAvaliacoesAdapter;
import com.example.blinded.interfaces.VoltarParaAActivityInformacao;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Speaker;

import java.text.ParseException;
import java.util.List;

public class ListaAvaliacoesAdapter extends PagerAdapter {
    private List<Avaliacao> menuOptionList;
    private Context context;
    FromListaAvaliacoesAdapter callback;
    private VoltarParaAActivityInformacao cfaInterface;
    private String disciplinaSelecionada;
    private Speaker sk;

    public ListaAvaliacoesAdapter(Context _context, List<Avaliacao> _menuOptionList, FromListaAvaliacoesAdapter callback, VoltarParaAActivityInformacao cfaInterface, String disciplinaSelecionada){
        this.menuOptionList = _menuOptionList;
        this.context = _context;
        this.callback = callback;
        this.cfaInterface = cfaInterface;
        this.disciplinaSelecionada = disciplinaSelecionada;
    }

    @Override
    public int getCount() {
        if (menuOptionList == null || menuOptionList.size()==0 )
            return 1;
        else
            return menuOptionList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object){
        container.removeView((View)object);
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position){
        if (menuOptionList == null || menuOptionList.size()==0 ) {

            cfaInterface.fragmentoAtualInfo( 16 );

            //chamar fragment
            View view = LayoutInflater.from(context).inflate(R.layout.fragment_informacao_000,container,false);
            final TextView title = (TextView)view.findViewById(R.id.textViewInformacao000);
            title.setText("Esta disciplina ainda não tem qualquer avaliação");

            container.addView(view);
            return view;
        }

        sk = new Speaker(context);

        //chamar fragment
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_avaliacao_lista_aluno,container,false);
        final TextView title = (TextView)view.findViewById(R.id.txbNomeAvaliacoes);
        title.setText(menuOptionList.get(position).getNome());
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sk.useSpeaker(menuOptionList.get(position).getNome());
                        //speak after 1000ms
                    }
                }, 1000);
            }
        });

        view.findViewById(R.id.btnAvaliacoesInscrever).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    callback.inscreverAlunoEmAvaliacao(menuOptionList.get(position).getId());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(context, InicioAvaliacaoActivity.class);
                intent.putExtra("idDisciplina", disciplinaSelecionada);
                intent.putExtra("idAvaliacao", menuOptionList.get(position).getId().toString() );
                context.startActivity(intent);
            }
        });

        container.addView(view);
        return view;
    }
}
