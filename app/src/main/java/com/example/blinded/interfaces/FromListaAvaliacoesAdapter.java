package com.example.blinded.interfaces;

import java.text.ParseException;

public interface FromListaAvaliacoesAdapter {

    void inscreverAlunoEmAvaliacao(Integer avaliacaoId) throws ParseException;

}
