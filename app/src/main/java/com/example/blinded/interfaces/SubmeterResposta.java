package com.example.blinded.interfaces;

import java.text.ParseException;

public interface SubmeterResposta {
    void submeterDadosDaResposta( Integer idQuestao, String resposta) throws ParseException;
}
