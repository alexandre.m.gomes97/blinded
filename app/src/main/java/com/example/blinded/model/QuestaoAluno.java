package com.example.blinded.model;

import android.os.Parcel;
import android.os.Parcelable;

public class QuestaoAluno implements Parcelable {
    private Integer id;
    private String titulo;
    private String tipo;
    private Integer numero;
    private Double cotacao;
    private String descricao;

    public QuestaoAluno(String _titulo,String _tipo){
        titulo = _titulo;
        tipo = _tipo;
    }

    public QuestaoAluno(Integer id, String titulo, Integer numero, Double cotacao, String descricao) {
        this.id = id;
        this.titulo = titulo;
        this.numero = numero;
        this.cotacao = cotacao;
        this.descricao = descricao;
    }


    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getTitulo(){return titulo;}

    public void setTitulo(String titulo){this.titulo = titulo;}

    public Integer getNumero() { return numero; }

    public void setNumero(Integer numero) { this.numero = numero; }

    public Double getCotacao() { return cotacao; }

    public void setCotacao(Double cotacao) { this.cotacao = cotacao; }

    public String getDescricao() { return descricao; }

    public void setDescricao(String descricao) { this.descricao = descricao; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(titulo);
        if (numero == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(numero);
        }
        if (cotacao == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(cotacao);
        }
        dest.writeString(descricao);
    }

    protected QuestaoAluno(Parcel in) {
        titulo = in.readString();
        if (in.readByte() == 0) {
            numero = null;
        } else {
            numero = in.readInt();
        }
        if (in.readByte() == 0) {
            cotacao = null;
        } else {
            cotacao = in.readDouble();
        }
        descricao = in.readString();
    }

    public static final Creator<QuestaoAluno> CREATOR = new Creator<QuestaoAluno>() {
        @Override
        public QuestaoAluno createFromParcel(Parcel in) {
            return new QuestaoAluno(in);
        }

        @Override
        public QuestaoAluno[] newArray(int size) {
            return new QuestaoAluno[size];
        }
    };
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
