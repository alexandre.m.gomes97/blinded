package com.example.blinded.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

public class PaginaInicialDisciplinaDesativer44Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Speaker sk;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        sk = new Speaker(getContext());


        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        Disciplina d = bundle.getParcelable("dis");

        View rootView = inflater.inflate(R.layout.fragment_pagina_inicial_disciplina_desativar_44, container, false);

        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_44);
        title.setText( d.getNome());

//        final ImageView imagem = rootView.findViewById(R.id.imageViewDisciplina_44);
//        imagem.setImageResource( d.getImage() );

        Button cl = rootView.findViewById(R.id.buttonDesativarDisciplina44);
        cl.setOnClickListener(this);

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        sk.useSpeaker("Desativar disciplina");
        cfaInterface.fragmentoAtual( 44 );
    }

    @Override
    public void onPause() {
        super.onPause();
        sk.tts.stop();
    }

    public void setPaginaInicialDisciplinaVerAvaliacoes24Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }

    @Override
    public void onClick(View v) {

        switch ( v.getId() )
        {
            case R.id.buttonDesativarDisciplina44:

                cfaInterface.esperarPorConfirmacao( 20, 1 );
                break;

        }

    }
}
