package com.example.blinded.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.QuestaoAluno;
import com.google.android.material.textfield.TextInputEditText;

import java.text.ParseException;

public class AvaliacaoQuestaoEditar30Fragment extends Fragment implements View.OnClickListener {
    private ComunicarFragmentActivity cfaInterface;
    private Disciplina mDisciplina;
    private Avaliacao mAvaliacao;
    private QuestaoAluno mQuestaoAluno;
    private TextInputEditText textInputEditTextPergunta_30, textInputEditTextCotacao_30;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        mDisciplina = bundle.getParcelable("dis");
        mAvaliacao = bundle.getParcelable("ava");
        mQuestaoAluno = bundle.getParcelable("que");


        View rootView = inflater.inflate(R.layout.fragmernt_avaliacao_questao_editar_30, container, false);

        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_30);
        title.setText( mDisciplina.getNome());

        final TextView titleAval = rootView.findViewById(R.id.textViewNomeDaAvaliacao_30);
        titleAval.setText( mAvaliacao.getNome());

        //final TextView editTextPergunta = rootView.findViewById(R.id.textInputEditTextPergunta_30);
        //editTextPergunta.setText( mQuestaoAluno.getTitulo());

        //final TextView editTextCotacao = rootView.findViewById(R.id.textInputEditTextCotacao_30);


        textInputEditTextPergunta_30 = rootView.findViewById(R.id.textInputEditTextPergunta_30);
        textInputEditTextPergunta_30.setText( mQuestaoAluno.getTitulo());

        textInputEditTextCotacao_30 = rootView.findViewById(R.id.textInputEditTextCotacao_30);
        String cota = String.valueOf( mQuestaoAluno.getCotacao() );
        textInputEditTextCotacao_30.setText( cota );


        Button cl = rootView.findViewById(R.id.buttonConfirmarEdicaoDeQuestao30);
        cl.setOnClickListener(this);

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        cfaInterface.fragmentoAtual(30);
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.buttonConfirmarEdicaoDeQuestao30) {
            // TODO: Enviar dados de edição da questao para a activity
            String sPergunta = textInputEditTextPergunta_30.getText().toString();
            String sCotacao = textInputEditTextCotacao_30.getText().toString();
            cfaInterface.editarQuestao( sPergunta, sCotacao );
            try {
                cfaInterface.alterarFragmento(27);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else if( v.getId() == R.id.textViewHitBoxEditQuestao){

        }

    }

    public void setAvaliacaoQuestaoEditar30Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }
}
