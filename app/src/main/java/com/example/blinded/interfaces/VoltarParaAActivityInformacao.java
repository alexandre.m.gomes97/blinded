package com.example.blinded.interfaces;

public interface VoltarParaAActivityInformacao {
    void voltar(Integer fragmentoAtual);
    void fragmentoAtualInfo(Integer fragment);
}
