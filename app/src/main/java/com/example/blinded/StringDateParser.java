package com.example.blinded;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class StringDateParser {

    public static Date parseDate(String date) throws ParseException
    {
        String format = "yyyy-MM-dd hh:mm:ss";

        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.UK);
        return formatter.parse(date);
    }

    public static String parseString(Date date) throws ParseException
    {
        String format = "yyyy-MM-dd hh:mm:ss";

        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.UK);
        return formatter.format(date);
    }

}
