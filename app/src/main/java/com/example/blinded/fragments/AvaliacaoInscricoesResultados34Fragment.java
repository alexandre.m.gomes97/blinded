package com.example.blinded.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Aluno;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;

import java.text.ParseException;

public class AvaliacaoInscricoesResultados34Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Disciplina mDisciplina;
    private Avaliacao mAvaliacao;
    private Aluno mAluno;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        mDisciplina = bundle.getParcelable("dis");
        mAvaliacao = bundle.getParcelable("ava");
        mAluno = bundle.getParcelable("alu");


        View rootView = inflater.inflate(R.layout.fragment_avaliacao_incricoes_resultados_34, container, false);

        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_34);
        title.setText( mDisciplina.getNome());

        final TextView titleAval = rootView.findViewById(R.id.textViewNomeDaAvaliacao_34);
        titleAval.setText( mAvaliacao.getNome());

        final TextView titleAluNome= rootView.findViewById(R.id.textViewNomeDoAluno_34);
        titleAluNome.setText( mAluno.getNome());

        final TextView titleAluCotacao = rootView.findViewById(R.id.textCotacaoDoAluno_34);
        String cotacao = String.valueOf( mAluno.getCotacao()) + " valores";
        titleAluCotacao.setText( cotacao );

        RelativeLayout cl = rootView.findViewById(R.id.relativeLayoutResultado);
        cl.setOnClickListener(this);

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        cfaInterface.fragmentoAtual(34);
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.relativeLayoutResultado) {
            cfaInterface.alunoOnClick(mAluno.getId());
            try {
                cfaInterface.alterarFragmento(36);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    public void setAvaliacaoInscricoesResultados34Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }
}
