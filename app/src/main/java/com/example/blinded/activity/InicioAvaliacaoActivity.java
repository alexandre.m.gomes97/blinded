package com.example.blinded.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import  com.example.blinded.R;
import com.example.blinded.model.AvaliacoesAluno;
import com.example.blinded.model.Speaker;

public class InicioAvaliacaoActivity extends AppCompatActivity {

    private AvaliacoesAluno avaliacao;
    private String idDisciplina, idAvaliacao;
    private Button varBtnInicioAvaliacaoIniciar;
    private Button varBtnBackInicioAvaliacao;
    private Speaker sk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_avaliacao);

        idDisciplina = getIntent().getStringExtra("idDisciplina");
        idAvaliacao = getIntent().getStringExtra("idAvaliacao");
        varBtnInicioAvaliacaoIniciar = findViewById(R.id.btnInicioAvaliacaoIniciar);
        varBtnBackInicioAvaliacao  = findViewById(R.id.btnBackInicioAvaliacao);

        sk = new Speaker(this);

        varBtnBackInicioAvaliacao.setClickable(false);
        varBtnInicioAvaliacaoIniciar.setClickable(false);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a39));
                varBtnBackInicioAvaliacao.setClickable(true);
                varBtnInicioAvaliacaoIniciar.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);
    }

    public void onClickHelp(View view)
    {
        varBtnBackInicioAvaliacao.setClickable(false);
        varBtnInicioAvaliacaoIniciar.setClickable(false);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a36));
                varBtnBackInicioAvaliacao.setClickable(true);
                varBtnInicioAvaliacaoIniciar.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);
    }

    public void onClickIniciar(View v) {
        Intent intent = new Intent(getApplicationContext(), QuestaoAlunoActivity.class);
        intent.putExtra("idDisciplina", idDisciplina);
        intent.putExtra("idAvaliacao", idAvaliacao );
        startActivity(intent);
    }

    @Override
    protected void onStop(){
        super.onStop();

        sk.tts.stop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        sk.tts.stop();
    }

    public void onClickInformacoes(View view)
    {

    }
}
