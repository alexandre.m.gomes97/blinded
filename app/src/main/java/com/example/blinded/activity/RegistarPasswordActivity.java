package com.example.blinded.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.blinded.R;
import com.example.blinded.database.DatabaseManager;
import com.example.blinded.model.Speaker;
import com.example.blinded.viewModels.UtilizadorViewModel;

import java.text.ParseException;

public class RegistarPasswordActivity extends AppCompatActivity {

    private static final String SHARED_PREFS = "sharedPreferences";
    public static final String TEXT = "idDoUtilizador";
    public static final String TEXT_REGISTERING = "registando";
    private UtilizadorViewModel utilizadorViewModel;

    private EditText text;
    private Speaker sk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registar_password);
        sk = new Speaker(this);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a15));
            }
        }, 1000);
        utilizadorViewModel = new UtilizadorViewModel(DatabaseManager.getHelper(this));

        text = findViewById(R.id.editTextEmailRegistar2);

        Button button = findViewById(R.id.btnPasswordLeft);
        button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                // Obter das shared preferences o id do utilizadro que se está a tentar armazenar
                SharedPreferences sharedPreferences = getSharedPreferences( SHARED_PREFS, MODE_PRIVATE);
                String loggedUserId = sharedPreferences.getString(TEXT, "");
                String registering = sharedPreferences.getString(TEXT_REGISTERING, "");

                if ( !registering.isEmpty() )
                    if ( registering.equals("true") ) {
                        try {
                            utilizadorViewModel.updateUtilizadorPassword( loggedUserId, text.getText().toString());// Update da password (text.getText().toString()) do utilizador com o id loggedUserId na base de dados.
                            Intent intent = new Intent(getApplicationContext(), RegistarSelecionarPerfilActivity.class);
                            startActivity(intent);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                return true;
            }
        });
    }

    @Override
    protected void onStop(){
        super.onStop();

        sk.tts.stop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        sk.tts.stop();
    }

    public void speakerOnClick(View view)
    {
        sk.useSpeaker(getResources().getString(R.string.a15));
    }

    public void onClick(View view) {
        String textString = text.getText().toString()+"E";
        text.setText(textString);
        sk.useSpeaker("Esquerda");

//        Intent intent = new Intent(getApplicationContext(), RegistarActivity.class);
//        startActivity(intent);
    }


    public void onClickBlue(View view){
        String textString = text.getText().toString()+"D";
        text.setText(textString);
        sk.useSpeaker("Direita");

//        Intent intent = new Intent(getApplicationContext(), RegistarSelecionarPerfilActivity.class);
//        startActivity(intent);
    }
}
