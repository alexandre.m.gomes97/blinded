package com.example.blinded.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.blinded.R;
import androidx.fragment.app.Fragment;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

import java.text.ParseException;

public class PaginaInicialDisciplinaVerAvaliacoes24Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Speaker sk;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        final Disciplina d = bundle.getParcelable("dis");

        View rootView = inflater.inflate(R.layout.fragment_pagina_inicial_disciplina_ver_avaliacoes_24, container, false);
        sk = new Speaker(getContext());
        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_24);
        title.setText(d.getNome());

        Button cl = rootView.findViewById(R.id.buttonVerAvaliacoes24);
        cl.setOnClickListener(this);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker("Disciplina: " + d.getNome() );
                sk.useSpeaker(getResources().getString(R.string.a47_2));
                sk.useSpeaker("Nesta página pode ver as avaliações, ao cliclar no fundo do ecra.");
            }
        }, 1000);
        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        sk.useSpeaker("Nesta página pode ver as avaliações, ao cliclar no fundo do ecra.");
        cfaInterface.fragmentoAtual(24);
    }

    @Override
    public void onPause() {
        super.onPause();
        sk.tts.stop();
        sk.useSpeaker("Encontra-se na lista da avaliações, faça swipe para a esquerda e para a direiita para ouvir as diferentes avaliações.");
    }

    public void setPaginaInicialDisciplinaVerAvaliacoes24Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }

    @Override
    public void onClick(View v) {

        switch ( v.getId() )
        {
            case R.id.buttonVerAvaliacoes24:
                try {
                    sk.tts.stop();

                    cfaInterface.alterarFragmento( 25 );
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;

        }

    }

//    // Vai ser usado para dizer à activity, qual o fragmento que está a ser representado em cada mometo, de forma a poder apresentar o audio correto. Visto em https://stackoverflow.com/a/25392549/13149102
//    public interface ComunicarFragmentActivity {
//
//        void fragmentoAtual( Integer fragment );
//
//    }

}



