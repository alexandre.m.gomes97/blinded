package com.example.blinded.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import com.example.blinded.R;
import com.example.blinded.adapters.MenuAlunoAdapter;
import com.example.blinded.model.AulaAluno;
import com.example.blinded.model.Speaker;

import java.util.ArrayList;
import java.util.List;

public class MainActivityAluno extends AppCompatActivity {

    private ViewPager viewPager;
    private List<AulaAluno> menuOptionList;
    private Speaker sk;

    private Button help;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_aluno);

        viewPager = findViewById(R.id.ViewPagerMenuAluno);
        menuOptionList = new ArrayList<AulaAluno>();
        this.fillMenuItens();
        //Definir Adapatador
        viewPager.setAdapter(new MenuAlunoAdapter(this ,menuOptionList));

        sk = new Speaker(this);

        help = findViewById(R.id.btnHelp2);
        help.setClickable(false);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a34));
                help.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);
    }

    public void fillMenuItens(){
        menuOptionList.add(new AulaAluno("Pesquisar Disciplina"));
        menuOptionList.add(new AulaAluno("Minhas Disciplinas"));
    }

    private void helpOnClick(View view)
    {
        help.setClickable(false);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a34));
                help.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);
    }

    @Override
    protected void onStop(){
        super.onStop();

        sk.tts.stop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        sk.tts.stop();
    }
}
