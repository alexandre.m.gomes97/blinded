package com.example.blinded.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.QuestaoAluno;
import com.example.blinded.model.Speaker;

import java.text.ParseException;

public class AvaliacaoQuestao27Fragment extends Fragment implements View.OnClickListener {


    private ComunicarFragmentActivity cfaInterface;
    private Integer mAvaliacaoId;
    private QuestaoAluno mQuestaoAluno;
    private Speaker sk;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sk = new Speaker(getContext());
        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        Disciplina d = bundle.getParcelable("dis");
        Avaliacao a = bundle.getParcelable("ava");
        mQuestaoAluno = bundle.getParcelable("que");

        mAvaliacaoId = a.getId();

        View rootView = inflater.inflate(R.layout.fragment_avaliacao_questao_27, container, false);

        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_27);
        title.setText( d.getNome());

        final TextView titleNomeDaAvaliacao = rootView.findViewById(R.id.textViewNomeDaAvaliacao_27);
        titleNomeDaAvaliacao.setText( a.getNome());

        final TextView nomeDaQuestao = rootView.findViewById(R.id.textViewNumeroDaQuestao_27);
        nomeDaQuestao.setText( mQuestaoAluno.getTitulo());

        final TextView cotacaoDaQuestao = rootView.findViewById(R.id.textViewCotacao_27);
        String cotacao = "(" + mQuestaoAluno.getCotacao() + " valores)";
        cotacaoDaQuestao.setText( cotacao );

        final TextView conteudoDaQuestao = rootView.findViewById(R.id.textViewQuestaoConteudo_27);
        conteudoDaQuestao.setText( mQuestaoAluno.getDescricao());

        final Button buttonEditar = rootView.findViewById(R.id.buttonEditarQuestao_27);
        final Button buttonEliminar = rootView.findViewById(R.id.buttonEliminarQuestao_27);

        buttonEditar.setOnClickListener(this);
        buttonEliminar.setOnClickListener(this);

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        cfaInterface.fragmentoAtual( 27 );
    }

    @Override
    public void onClick(View v) {

        cfaInterface.questaoOnClick( mQuestaoAluno.getId() );

        switch (v.getId())
        {
            case R.id.buttonEditarQuestao_27:
                try {
                    sk.useSpeaker("Editar questão");
                    cfaInterface.alterarFragmento( 30 );
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.buttonEliminarQuestao_27:
                sk.useSpeaker("Eliminar questão");
                cfaInterface.esperarPorConfirmacao( 26, 3 );
                break;

        }

    }

    public void setAvaliacaoQuestao27Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }
}
