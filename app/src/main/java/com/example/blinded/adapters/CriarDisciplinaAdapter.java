package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.DisciplinaCriada22Fragment;
import com.example.blinded.fragments.ProfessorCriarDisciplina21Fragment;
import com.example.blinded.model.Disciplina;

import java.util.ArrayList;

public class CriarDisciplinaAdapter extends FragmentStatePagerAdapter {

    public CriarDisciplinaAdapter(FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        ProfessorCriarDisciplina21Fragment pagina21 = new ProfessorCriarDisciplina21Fragment();
        return pagina21;
    }

    @Override
    public int getCount() {
        return 1;
    }
}
