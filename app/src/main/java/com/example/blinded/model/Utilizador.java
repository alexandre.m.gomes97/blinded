package com.example.blinded.model;

public class Utilizador {
    private String nome;
    private String email;
    private String data_de_nascimento;
    private Boolean tipo;
    private String password_sequence;

    public Utilizador() {
    }

    public Utilizador(String nome, String email, String data_de_nascimento, Boolean tipo, String password_sequence) {
        this.nome = nome;
        this.email = email;
        this.data_de_nascimento = data_de_nascimento;
        this.tipo = tipo;
        this.password_sequence = password_sequence;
    }

    public Utilizador(String nome, Boolean tipo, String password_sequence) {
        this.nome = nome;
        this.tipo = tipo;
        this.password_sequence = password_sequence;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getData_de_nascimento() {
        return data_de_nascimento;
    }

    public void setData_de_nascimento(String data_de_nascimento) {
        this.data_de_nascimento = data_de_nascimento;
    }

    public Boolean getTipo() {
        return tipo;
    }

    public void setTipo(Boolean tipo) {
        this.tipo = tipo;
    }

    public String getPassword_sequence() {
        return password_sequence;
    }

    public void setPassword_sequence(String password_sequence) {
        this.password_sequence = password_sequence;
    }
}
