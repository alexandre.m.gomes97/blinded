package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.PaginaInicialDisciplinaCriarAvaliacao41Fragment;
import com.example.blinded.fragments.PaginaInicialDisciplinaDesativer44Fragment;
import com.example.blinded.fragments.PaginaInicialDisciplinaEditar42Fragment;
import com.example.blinded.fragments.PaginaInicialDisciplinaVerAvaliacoes24Fragment;
import com.example.blinded.model.Disciplina;

public class DisciplinaSwipeAdapter extends FragmentStatePagerAdapter {

    private Disciplina mDisciplina;

    public DisciplinaSwipeAdapter( FragmentManager fm, Disciplina d) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mDisciplina = d;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                PaginaInicialDisciplinaVerAvaliacoes24Fragment pagina24 = new PaginaInicialDisciplinaVerAvaliacoes24Fragment();
                Bundle bundle24 = new Bundle();
                bundle24.putParcelable( "dis", mDisciplina );
                pagina24.setArguments(bundle24);
                return pagina24;
            case 1:
                PaginaInicialDisciplinaCriarAvaliacao41Fragment pagina41 = new PaginaInicialDisciplinaCriarAvaliacao41Fragment();
                Bundle bundle41 = new Bundle();
                bundle41.putParcelable( "dis", mDisciplina );
                pagina41.setArguments(bundle41);
                return pagina41;
            case 2:
                PaginaInicialDisciplinaEditar42Fragment pagina42 = new PaginaInicialDisciplinaEditar42Fragment();
                Bundle bundle42 = new Bundle();
                bundle42.putParcelable( "dis", mDisciplina );
                pagina42.setArguments(bundle42);
                return pagina42;
            case 3:
                PaginaInicialDisciplinaDesativer44Fragment pagina44 = new PaginaInicialDisciplinaDesativer44Fragment();
                Bundle bundle44 = new Bundle();
                bundle44.putParcelable( "dis", mDisciplina );
                pagina44.setArguments(bundle44);
                return pagina44;
            default:
                PaginaInicialDisciplinaVerAvaliacoes24Fragment pagina24d = new PaginaInicialDisciplinaVerAvaliacoes24Fragment();
                Bundle bundle24d = new Bundle();
                bundle24d.putParcelable( "dis", mDisciplina );
                pagina24d.setArguments(bundle24d);
                return pagina24d;
        }
    }




    @Override
    public int getCount() {
        return 4;
    }
}
