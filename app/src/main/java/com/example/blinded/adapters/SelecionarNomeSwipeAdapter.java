package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.AvaliacaoCriarAvaliacaoSelecionarNome411Fragment;
import com.example.blinded.fragments.AvaliacaoInscricoesResultadosVerCotacao36Fragment;
import com.example.blinded.model.Aluno;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.QuestaoAluno;

import java.util.ArrayList;

public class SelecionarNomeSwipeAdapter extends FragmentStatePagerAdapter {

    private Disciplina mDisciplina;

    public SelecionarNomeSwipeAdapter(FragmentManager fm, Disciplina d) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mDisciplina = d;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        AvaliacaoCriarAvaliacaoSelecionarNome411Fragment pagina411 = new AvaliacaoCriarAvaliacaoSelecionarNome411Fragment();
        Bundle bundle411 = new Bundle();
        bundle411.putParcelable( "dis", mDisciplina );
        pagina411.setArguments(bundle411);
        return pagina411;
    }




    @Override
    public int getCount() {
        return 1;
    }
}
