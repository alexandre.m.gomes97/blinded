package com.example.blinded.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.interfaces.VoltarParaAActivityInformacao;
import com.example.blinded.model.Disciplina;

public class Informacao000Fragment extends Fragment {

    private VoltarParaAActivityInformacao cfaInterface;
    private Integer mCurrentFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        assert bundle != null;
        String s = bundle.getString("data");
        mCurrentFragment= bundle.getInt("fragAtual");

        View rootView = inflater.inflate(R.layout.fragment_informacao_000, container, false);

        final TextView title = rootView.findViewById(R.id.textViewInformacao000);
        title.setText( s);


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        cfaInterface.fragmentoAtualInfo( mCurrentFragment );
    }



    public void setInformacao000Fragment(Disciplina20Activity disciplina20Activity) {

        cfaInterface = disciplina20Activity;

    }

//    public void setInformacao000Fragment(Disciplina20Activity disciplina20Activity) {
//
//        cfaInterface = disciplina20Activity;
//
//    }
//
//    public void setInformacao000Fragment(Disciplina20Activity disciplina20Activity) {
//
//        cfaInterface = disciplina20Activity;
//
//    }


}
