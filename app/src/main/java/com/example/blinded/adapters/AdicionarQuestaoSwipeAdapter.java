package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.AvaliacaoApagarAvaliacao39Fragment;
import com.example.blinded.fragments.AvaliacaoEditarAvaliacao38Fragment;
import com.example.blinded.fragments.AvaliacaoQuestaoAdicionarQuestao32Fragment;
import com.example.blinded.fragments.AvaliacaoVerInscricoes33Fragment;
import com.example.blinded.fragments.AvaliacaoVerQuestoes26Fragment;
import com.example.blinded.fragments.AvaliacaoVerResultadosMenu341Fragment;
import com.example.blinded.fragments.AvalicaoMenuAdicionarQuestao31Fragment;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;

public class AdicionarQuestaoSwipeAdapter extends FragmentStatePagerAdapter {

    private Avaliacao mAvaliacao;
    private Disciplina mDisciplina;

    public AdicionarQuestaoSwipeAdapter(FragmentManager fm, Avaliacao a, Disciplina d) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mAvaliacao = a;
        mDisciplina = d;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        AvaliacaoQuestaoAdicionarQuestao32Fragment pagina32 = new AvaliacaoQuestaoAdicionarQuestao32Fragment();
        Bundle bundle32 = new Bundle();
        bundle32.putParcelable( "dis", mDisciplina );
        bundle32.putParcelable( "ava", mAvaliacao );
        pagina32.setArguments(bundle32);
        return pagina32;

    }




    @Override
    public int getCount() {
        return 1;
    }

}
