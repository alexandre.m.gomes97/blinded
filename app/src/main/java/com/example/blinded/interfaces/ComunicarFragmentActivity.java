package com.example.blinded.interfaces;

import android.text.Editable;

import com.example.blinded.model.Disciplina;

import java.text.ParseException;
import java.util.Date;

// Vai ser usado para dizer à activity, qual o fragmento que está a ser representado em cada mometo, de forma a poder apresentar o audio correto. Visto em https://developer.android.com/training/basics/fragments/communicating e https://stackoverflow.com/a/51789807/13149102
public interface ComunicarFragmentActivity {

    void fragmentoAtual( Integer fragment );

    void disciplinaOnClick( Integer idDaDisciplina );

    void avaliacaoOnClick( Integer idDaAvaliacaoSelecionada);

    void alunoOnClick( Integer idDoAlunoSelecionado);

    void questaoOnClick( Integer idDaQuestaoSelecionado);

    void alterarFragmento( Integer fragDestino ) throws ParseException;

    void dataSelecionada( Date d );

    void updateOrCreateDisciplina( Disciplina d) throws ParseException;

    void cancelarOperacao();

    void esperarPorConfirmacao( Integer fragmentoDestino, Integer tipo );

    void nomeSelecionado(String nomeDaAvaliacaoCriada, Integer editar) throws ParseException;

    void criarDisciplina(String nome, String descricao) throws ParseException;

    void adicionarQuestao( String pergunta, Double cotacao) throws ParseException;

    void editarQuestao(String sPergunta, String sCotacao);
}