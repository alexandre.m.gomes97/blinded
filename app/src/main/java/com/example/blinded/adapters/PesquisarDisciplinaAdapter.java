package com.example.blinded.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.SearchEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.example.blinded.R;
import com.example.blinded.activity.RegistarActivity;
import com.example.blinded.interfaces.FragmentToActivityCode;
import com.example.blinded.model.AulaAluno;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

import java.util.List;

public class PesquisarDisciplinaAdapter extends PagerAdapter {
    private List<Disciplina> menuOptionList;
    Context context;
    FragmentToActivityCode callback;
    private Speaker sk;

    public PesquisarDisciplinaAdapter(Context _context, List<Disciplina> _menuOptionList, FragmentToActivityCode callback){
        this.menuOptionList = _menuOptionList;
        this.context = _context;
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return menuOptionList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object){
        container.removeView((View)object);
    }

    @Override
    public Object instantiateItem(@NonNull final ViewGroup container, final int position){
        View view = LayoutInflater.from(context).inflate(R.layout.pesquisar_disciplina_fragment,container,false);
        final Button varBtnPesquisarDisciplinarInscrever = (Button)view.findViewById(R.id.btnPesquisarDisciplinarInscrever);
        varBtnPesquisarDisciplinarInscrever.setClickable(false);
        final TextView title = (TextView)view.findViewById(R.id.txbNomeDisciplinaPesquisar);
        title.setText(menuOptionList.get(position).getNome());
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sk.useSpeaker(menuOptionList.get(position).getNome());
                        varBtnPesquisarDisciplinarInscrever.setClickable(true);
                        //speak after 1000ms
                    }
                }, 1000);
            }
        });

        Button btnPesquisarDisciplinarInscrever = view.findViewById(R.id.btnPesquisarDisciplinarInscrever);
        btnPesquisarDisciplinarInscrever.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callback.fragmentCodeEDisciplinaId(14, menuOptionList.get(position).getId());

            }
        });

        container.addView(view);
        return view;
    }

}
