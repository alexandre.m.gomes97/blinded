package com.example.blinded.viewModels;

import android.content.ContentValues;
import android.media.MediaDataSource;

import androidx.fragment.app.Fragment;

import com.example.blinded.StringDateParser;
import com.example.blinded.database.DatabaseSQLITE;
import com.example.blinded.model.Aluno;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.QuestaoAluno;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ProfessorViewModel {

    private ArrayList<Disciplina> disciplinasDoUtilizadorAtual;
    private Integer idDoUtilizadorAtual;

    private DatabaseSQLITE mDatabase;

    public ProfessorViewModel(DatabaseSQLITE database, Integer idDoUtilizadorAtual) throws ParseException {
        mDatabase = database;
        this.idDoUtilizadorAtual = idDoUtilizadorAtual;
        disciplinasDoUtilizadorAtual = getDisciplinasPorUtilizadorId(idDoUtilizadorAtual);

    }

    public ArrayList<Disciplina> getDisciplinasDoUtilizadorAtual() throws ParseException {
        return disciplinasDoUtilizadorAtual;
    }

    public void setDisciplinasDoUtilizadorAtual(ArrayList<Disciplina> disciplinasDoUtilizadorAtual) {
        this.disciplinasDoUtilizadorAtual = disciplinasDoUtilizadorAtual;
    }

    public ArrayList<Disciplina> getDisciplinasPorUtilizadorId( Integer userId) throws ParseException {
        ArrayList<Disciplina> listaDeDisciplinas = new ArrayList<>();
        Disciplina disciplina;
        for (ContentValues cv: mDatabase.getDisciplinasPorUtilizadorId(userId.toString())
             ) {
            disciplina = new Disciplina(
                    cv.getAsInteger("disciplina_Id"),
                    cv.getAsString("disciplina_Nome"),
                    StringDateParser.parseDate(cv.getAsString("disciplina_Data_de_Criacao")),
                    cv.getAsString("disciplina_Ano_letivo"),
                    cv.getAsString("disciplina_Descricao")
            );
            listaDeDisciplinas.add(disciplina);
        }

        return listaDeDisciplinas;
    }




    public void insertUtilizador(String utilizador_Nome, String utilizador_Email, Date utilizador_Data_de_nascimento, Integer utilizador_Tipo, String utilizador_Password) throws ParseException {

        mDatabase.insertUtilizador(utilizador_Nome, utilizador_Email, utilizador_Data_de_nascimento, utilizador_Tipo, utilizador_Password);

    }

    public long insertDisciplina( String disciplina_Nome, String disciplina_Sigla, Date disciplina_Data_de_Criacao, String disciplina_Ano_letivo, String disciplina_Descricao, Integer  disciplina_Criada_por_utilizadorId ) throws ParseException {

        return mDatabase.insertDisciplina( disciplina_Nome, disciplina_Sigla, disciplina_Data_de_Criacao, disciplina_Ano_letivo, disciplina_Descricao, disciplina_Criada_por_utilizadorId);

    }

    public void associarUtilizadorAtualADisciplina( Integer disciplina_id) throws ParseException {
        mDatabase.insertUtilizador_Disciplina(idDoUtilizadorAtual, disciplina_id);
    }


    public void insertUtilizador_Disciplina(long utilizadorId, long disciplinaId) throws ParseException {
        mDatabase.insertUtilizador_Disciplina( (int) utilizadorId, (int) disciplinaId);
    }

    public long insertAvaliacao(String nomeDaAvaliacaoCriada, String s, Date dataSelecionada, String s1, String s2, Integer idDoUtilziadorLogado) throws ParseException {
        return mDatabase.insertAvaliacao(nomeDaAvaliacaoCriada, dataSelecionada, 100, idDoUtilziadorLogado);
    }

    public void insertAvaliacao_Disciplina(long idAvaliacao, Integer id) throws ParseException {

        mDatabase.insertDisciplina_Avaliacao( id, (int) idAvaliacao);

    }

    public ArrayList<Avaliacao> getAvaliacoesPorByDisciplina(Integer idDaDisciplinaAtual) throws ParseException {

        ArrayList<Avaliacao> listaDeDisciplinas = new ArrayList<>();
        Avaliacao avaliacao;
        for (ContentValues cv: mDatabase.getAvaliacoesPorDisciplinaId(idDaDisciplinaAtual.toString())
        ) {
            avaliacao = new Avaliacao(
                    cv.getAsInteger("avaliacao_Id"),
                    cv.getAsString("avaliacao_Nome"),
                    new Date()
                    //new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.UK).parse(cv.getAsString("avaliacao_Data_da_Avaliacao"))
            //StringDateParser.parseDate() // TODO: Analsar data
            );
            listaDeDisciplinas.add(avaliacao);
        }

        return listaDeDisciplinas;

    }

    public void atualizarDisciplina(Integer id, String nome, String descricao) {

        mDatabase.updateDisciplina(id, nome, descricao); // TODO: Testar

    }

    public void desativarDisciplina(Integer id, Integer utilizadorId) {

        mDatabase.desativarDisciplina( id ); // TODO: Testar
        mDatabase.desativarUtilizadorDisciplina( id, utilizadorId);
        //mDatabase.seativarAvaliacoesPorDisciplina( id ); //TODO: Implementar este método.


    }

    public void desativarAvaliacao(Integer avaliacaoId) {

        mDatabase.desativarAvaliacao( avaliacaoId ); // TODO: Testar
        mDatabase.desativarDisciplinaAvaliacao( avaliacaoId );
        mDatabase.desativarUtilizadorAvaliacao( avaliacaoId );
        mDatabase.desativarQuestaoAvaliacaoByAvaliacaoId(avaliacaoId);


    }

    public void desativarQuestao(Integer id) {

        mDatabase.desativarQuestao( id ); // TODO: Testar
        mDatabase.desativarQuestaoAvaliacaoPorQuestaoId(id);
        mDatabase.desativarRespostaPorQuestaoId(id);
    }

    public ArrayList<QuestaoAluno> getQuestoesPorAvaliacao(Integer id) {

        ArrayList<QuestaoAluno> listaDeDisciplinas = new ArrayList<>();
        QuestaoAluno questaoAluno;
        for (ContentValues cv: mDatabase.getQuestoesPorAvaliacaoId(id.toString())
        ) {
            questaoAluno = new QuestaoAluno(
                    cv.getAsInteger("questao_Id"),
                    cv.getAsString("questao_Titulo"),
                    cv.getAsInteger("questao_Numero"),
                    cv.getAsDouble("questao_Cotacao"),
                    cv.getAsString("questao_Descricao")
                    );

            listaDeDisciplinas.add(questaoAluno);
        }

        return listaDeDisciplinas;

    }

    public long insertQuestao(Integer idDoUtilizadorAtual, String pergunta, Double cotacao) throws ParseException {

        return mDatabase.insertQuestao(pergunta, 1, pergunta, cotacao.floatValue() , 0, "", idDoUtilizadorAtual );


    }

    public void insertQuestao_Avaliacao(long idDaQuestao, Integer idDaAvaliacao) throws ParseException {

        mDatabase.insertQuestao_Avaliacao( (int) idDaQuestao, idDaAvaliacao );

    }

    public ArrayList<Aluno> getInscritosPorAvaliacaoId(Integer avaliacaoId) {

        ArrayList<Aluno> listaDeAlunos = new ArrayList<>();
        Aluno aluno;
        for (ContentValues cv: mDatabase.getInscritosPorAvaliacaoId(avaliacaoId)
        ) {
            aluno = new Aluno(
                    cv.getAsInteger("utilizador_Id"),
                    cv.getAsString("utilizador_Nome"),
                    //cv.getAsString("utilizador_Email"),
                    //cv.getAsString("utilizador_Data_de_nascimento"),
                    //cv.getAsString("utilizador_Tipo"),
                    //cv.getAsString("utilizador_Password")
                    10
            );

            listaDeAlunos.add(aluno);
        }

        return listaDeAlunos;

    }


    public void atualizarAvaliacao(Integer idDaAvaliacao, String nomeDaAvaliacaoCriada, String s, Date dataSelecionada, String s1, String s2, Integer idDoUtilziadorLogado) {

        mDatabase.atualizarAvaliacao( idDaAvaliacao, nomeDaAvaliacaoCriada, dataSelecionada);

    }

    public ArrayList<Aluno> getResultadosPorAvaliacaoId(Integer avaliacaoId) {

        ArrayList<Aluno> listaDeAlunos = new ArrayList<>();
        Aluno aluno;
        for (ContentValues cv: mDatabase.obterResultadosPorAvaliacaoId(avaliacaoId)
        ) {
            aluno = new Aluno(
                    cv.getAsInteger("utilizador_Id"),
                    cv.getAsString("utilizador_Nome"),
                    //cv.getAsString("utilizador_Email"),
                    //cv.getAsString("utilizador_Data_de_nascimento"),
                    //cv.getAsString("utilizador_Tipo"),
                    //cv.getAsString("utilizador_Password")
                    cv.getAsFloat("utilizador_Resultado")
            );

            listaDeAlunos.add(aluno);
        }

        return listaDeAlunos;

    }

    public void editarQuestao(Integer idDaQuestao, String sPergunta, String sCotacao) {

        mDatabase.updateQuestao( idDaQuestao, sPergunta, sCotacao);

    }
}
