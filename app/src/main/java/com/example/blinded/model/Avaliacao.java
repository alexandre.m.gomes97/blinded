package com.example.blinded.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Avaliacao implements Parcelable {

    private Integer id;
    private String nome;
    private Date data;

    /**
     * Constructor
     * @param nome
     * @param data
     *
     */

    public Avaliacao() {
    }

    public Avaliacao(Integer id, String nome, Date data) {
        this.id = id;
        this.nome = nome;
        this.data = data;
    }

    /**
     * Gtters & Setters
     *
     */

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getNome() { return nome; }

    public void setNome(String nome) { this.nome = nome; }

    public Date getData() { return data; }

    public void setData(Date data) { this.data = data; }


    /**
     * Implementações - parcelable
     *
     */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(nome);
    }

    protected Avaliacao(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        nome = in.readString();
    }

    public static final Creator<Avaliacao> CREATOR = new Creator<Avaliacao>() {
        @Override
        public Avaliacao createFromParcel(Parcel in) {
            return new Avaliacao(in);
        }

        @Override
        public Avaliacao[] newArray(int size) {
            return new Avaliacao[size];
        }
    };
}
