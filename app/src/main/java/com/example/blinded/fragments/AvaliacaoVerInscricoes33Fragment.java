package com.example.blinded.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

import java.text.ParseException;

public class AvaliacaoVerInscricoes33Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Disciplina mDisciplina;
    private Avaliacao mAvaliacao;
    private Speaker sk;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        mDisciplina = bundle.getParcelable("dis");
        mAvaliacao = bundle.getParcelable("ava");
        sk = new Speaker(getContext());

        View rootView = inflater.inflate(R.layout.fragment_avaliacao_verincricoes_33, container, false);

        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_33);
        title.setText( mDisciplina.getNome());

        final TextView titleAval = rootView.findViewById(R.id.textViewNomeDaAvaliacao_33);
        titleAval.setText( mAvaliacao.getNome());

        Button cl = rootView.findViewById(R.id.buttonVerInscricoes33);
        cl.setOnClickListener(this);

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        sk.useSpeaker("Lista de inscrições");
        cfaInterface.fragmentoAtual(33);
    }

    @Override
    public void onPause() {
        super.onPause();
        sk.tts.stop();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.buttonVerInscricoes33) {
            try {
                cfaInterface.alterarFragmento(331);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    public void setAvaliacaoVerInscricoes33Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }
}
