package com.example.blinded.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.example.blinded.R;
import com.example.blinded.activity.DisciplinaAlunoActivity;
import com.example.blinded.activity.PesquisarDisciplinaAlunoActivity;
import com.example.blinded.activity.RegistarActivity;
import com.example.blinded.model.AulaAluno;
import com.example.blinded.model.Speaker;

import java.util.List;

public class MenuAlunoAdapter extends PagerAdapter {
    private List<AulaAluno> menuOptionList;
    private Context context;

    public MenuAlunoAdapter(Context _context, List<AulaAluno> _menuOptionList){
        this.menuOptionList = _menuOptionList;
        this.context = _context;
    }

    @Override
    public int getCount() {
        return menuOptionList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object){
        container.removeView((View)object);
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position){
        // Buscar a view                                                 AQUI!!!
        View view = LayoutInflater.from(context).inflate(R.layout.menu_aluno_cardview,container,false);
        // Buscar os objectos da view
        final TextView title = (TextView)view.findViewById(R.id.txbMenuCardView);
        title.setText(menuOptionList.get(position).getTitulo());

        //Adicionar click à view
        view.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View view){
                if(title.getText() == "Pesquisar Disciplina"){
                    Intent intent = new Intent(context, PesquisarDisciplinaAlunoActivity.class);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
                else if(title.getText() == "Minhas Disciplinas"){
                    Intent intent = new Intent(context, DisciplinaAlunoActivity.class);
                    context.startActivity((intent));
                }
            }
        });

        container.addView(view);
        return view;
    }
}
