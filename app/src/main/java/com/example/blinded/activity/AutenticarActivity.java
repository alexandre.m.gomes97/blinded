package com.example.blinded.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;
import androidx.biometric.BiometricPrompt.PromptInfo;
import androidx.fragment.app.FragmentActivity;

import com.example.blinded.R;
import com.example.blinded.database.DatabaseManager;
import com.example.blinded.model.Speaker;
import com.example.blinded.viewModels.UtilizadorViewModel;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import android.os.Handler;

public class AutenticarActivity extends AppCompatActivity {

    private UtilizadorViewModel utilizadorViewModel;
    private EditText mEditText;
    private Speaker sk;

    private Button btnHelpAutenticar;
    private Button btnAutenticarNext;
    private  Button btnRecuperarPassword;
    private Button btnAutenticarApagar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autenticar);
        sk = new Speaker(this);

        utilizadorViewModel = new UtilizadorViewModel(DatabaseManager.getHelper(this));
        mEditText = findViewById(R.id.editTextNomeAutenticao);

        // Inicializar as variaveis
        btnHelpAutenticar = findViewById(R.id.btnHelpAutenticar);
        btnAutenticarNext = findViewById(R.id.btnAutenticarNext);
        btnRecuperarPassword = findViewById(R.id.btnRecuperarPass);
        btnAutenticarApagar = findViewById(R.id.btnLapisAutenticar);
        btnHelpAutenticar.setClickable(false);
        btnAutenticarNext.setClickable(false);
        btnRecuperarPassword.setClickable(false);
        btnAutenticarApagar.setClickable(false);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a4));
                btnHelpAutenticar.setClickable(true);
                btnAutenticarNext.setClickable(true);
                btnRecuperarPassword.setClickable(true);
                btnAutenticarApagar.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);
    }

    public void onClickNext(View view) {

        // Verificar se existe algum utilizador na base de dados, com esse nome de utilizador.
        if ( utilizadorViewModel.ProcurarNomeDeUtilizador(mEditText.getText().toString()) ) {
            String aux = mEditText.getText().toString();
            Intent intent = new Intent(getApplicationContext(), AutenticarPasswordActivity.class);
            intent.putExtra("username", mEditText.getText().toString() );
            startActivity(intent);
        } else {
            Toast.makeText(getBaseContext(), "Não existe qualquer turilizador com esse username", Toast.LENGTH_LONG).show();
        }
    }

    public void helpAutenticar(View view){
        sk.useSpeaker(getResources().getString(R.string.a4));

        while ((sk.tts.isSpeaking()));
        getSpeechInput(view);
    }

    @Override
    protected void onStop(){
        super.onStop();

        sk.tts.stop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        sk.tts.stop();
    }

    public void onClickForgetPasswword(View view) {
        Intent intent = new Intent(getApplicationContext(), ForgetPasswordActivity.class);

        startActivity(intent);
    }

    public void onClickDelete(View view){
        sk.useSpeaker(mEditText.getText().toString());
        while(sk.tts.isSpeaking()){}
        String aux = removeLastCharacter(mEditText.getText().toString());
        mEditText.setText(aux);
    }

    public void OnTextClick(View view){
        sk.tts.stop();

        try{
            Thread.sleep(100);
        }catch(Exception e){
        }
        sk.tts.stop();
        sk.useSpeaker("Diga o nome ou número de utilizador, um segundo após esta frase terminar");
        while(sk.tts.isSpeaking()){}

        getSpeechInput(view);
    }

    public void getSpeechInput(View view){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        startActivityForResult(intent,10);
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 10:
                if(resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    mEditText.setText(result.get(0));
                }
                break;
        }
    }

    public static String removeLastCharacter(String str) {
        String result = null;
        if ((str != null) && (str.length() > 0)) {
            result = str.substring(0, str.length() - 1);
        }
        return result;
    }
    public void onClickAuthentication(View view)
    {
        //Biometric Authenticator
        Executor newExecutor = Executors.newSingleThreadExecutor();

        Activity activity = this;

        final BiometricPrompt myBiometricPrompt = new BiometricPrompt((FragmentActivity) activity, newExecutor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                if(errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON){
                    Log.d("BiometricPrompt","Negative Button");
                }
                else{
                    Log.d("BiometricPrompt", "An unrecoverable error occurred");
                }
            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                Log.d("BiometricPrompt", "Fingerprint recognised successfully");
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Log.d("BiometricPrompt", "Fingerprint not recognised");
            }
        });

        final PromptInfo promptInfo = new PromptInfo.Builder()
                .setTitle("Enfia o dedo aqui.")
                .setSubtitle("Não tenhas medo.")
                .setDescription("Vais gostar.")
                .setNegativeButtonText("Prometo").build();

        myBiometricPrompt.authenticate(promptInfo);
    }
}
