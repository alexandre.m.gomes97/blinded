package com.example.blinded.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

import java.text.ParseException;

public class PaginaInicialDisciplinaEditar42Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Speaker sk;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        Disciplina d = bundle.getParcelable("dis");
        sk = new Speaker(getContext());
        View rootView = inflater.inflate(R.layout.fragment_pagina_inicial_disciplina_editar_42, container, false);

        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_42);
        title.setText( d.getNome());

//        final ImageView imagem = rootView.findViewById(R.id.imageViewDisciplina_42);
//        imagem.setImageResource( d.getImage() );

        final Button button = rootView.findViewById(R.id.buttonEditarAvaliacao);
        button.setOnClickListener(this);

        //cfaInterface.fragmentoAtual( 42 );

        return rootView;

    }


    @Override
    public void onResume() {
        super.onResume();
        sk.useSpeaker("Editar disciplina.");
        cfaInterface.fragmentoAtual( 42 );
    }

    @Override
    public void onStop() {
        super.onStop();
        sk.tts.stop();
    }

    public void setPaginaInicialDisciplinaVerAvaliacoes24Fragment(Disciplina20Activity disciplina20Activity) {

        cfaInterface = disciplina20Activity;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.buttonEditarAvaliacao:
                try {
                    cfaInterface.alterarFragmento( 43 );
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;

        }



    }
}
