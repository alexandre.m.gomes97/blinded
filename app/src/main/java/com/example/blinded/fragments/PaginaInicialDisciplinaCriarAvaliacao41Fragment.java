package com.example.blinded.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class PaginaInicialDisciplinaCriarAvaliacao41Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Integer numeroDaUnidadeAtual = 0;
    private Calendar mData;
    private TextView tvDate;
    private View rootView;
    private Disciplina mDisciplina;
    private Avaliacao mAvaliacao;
    private Speaker sk;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mDisciplina = getArguments().getParcelable("dis");
        mAvaliacao = getArguments().getParcelable("ava");
        sk = new Speaker(getContext());

        mData = GregorianCalendar.getInstance();

        rootView = inflater.inflate(R.layout.fragment_avaliacao_criaravaliacao_41, container, false);

        TextView tv = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_41);
        tv.setText(mDisciplina.getNome());

        tvDate = rootView.findViewById(R.id.textViewSelecionarData41);
        tvDate.setText( mData.getTime().toString());

        Button bAdd = rootView.findViewById(R.id.buttonSlecionarDataAdicionarUnidade41);
        Button bSub = rootView.findViewById(R.id.buttonSlecionarDataSubtrairUnidade41);
        Button bConfirm = rootView.findViewById(R.id.buttonSlecionarDataConfirmarData41);

        bAdd.setOnClickListener(this);
        bSub.setOnClickListener(this);
        bConfirm.setOnClickListener(this);

        if ( mAvaliacao != null ) {
            mData.setTime( mAvaliacao.getData() );
        }

        setTextView();

        return rootView;

    }

    private void setTextView(  ) {

        int year, month, day, hour, minute;

        year = mData.get(Calendar.YEAR);
        month = mData.get(Calendar.MONTH) +1; // Note: zero based!
        day = mData.get(Calendar.DAY_OF_MONTH);
        hour = mData.get(Calendar.HOUR_OF_DAY);
        minute = mData.get(Calendar.MINUTE);

        String all = year + "/" + month + "/" + day + " " + hour + ":" + minute;

        tvDate.setText( all  );

    }

    @Override
    public void onResume() {
        super.onResume();
        sk.useSpeaker(getResources().getString(R.string.a51));
        cfaInterface.fragmentoAtual( 41 );
    }

    @Override
    public void onPause() {
        super.onPause();
        sk.tts.stop();
    }

    public void setPaginaInicialDisciplinaVerAvaliacoes24Fragment(Disciplina20Activity disciplina20Activity) {

        cfaInterface = disciplina20Activity;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.buttonSlecionarDataAdicionarUnidade41:

                switch ( numeroDaUnidadeAtual )
                {
                    case 0:
                        sk.tts.stop();
                        mData.add(Calendar.YEAR, 1);
                        sk.useSpeaker(String.valueOf(mData.get(Calendar.YEAR)));
                        break;
                    case 1:
                        sk.tts.stop();
                        mData.add(Calendar.MONTH, 1);
                        int aux = mData.get(Calendar.MONTH);
                        sk.useSpeaker(String.valueOf(aux +1));
                        break;
                    case 2:
                        sk.tts.stop();
                        mData.add(Calendar.DAY_OF_MONTH, 1);
                        sk.useSpeaker(String.valueOf(mData.get(Calendar.DAY_OF_MONTH)));
                        break;
                    case 3:
                        sk.tts.stop();
                        mData.add(Calendar.HOUR_OF_DAY, 1);
                        sk.useSpeaker(String.valueOf(mData.get(Calendar.HOUR_OF_DAY)));
                        break;
                    case 4:
                        sk.tts.stop();
                        mData.add(Calendar.MINUTE, 1);
                        sk.useSpeaker(String.valueOf(mData.get(Calendar.MINUTE)));
                        break;
                }

                setTextView();
                rootView.invalidate();

                break;
            case R.id.buttonSlecionarDataSubtrairUnidade41:

                switch ( numeroDaUnidadeAtual )
                {
                    case 0:
                        sk.tts.stop();
                        mData.add(Calendar.YEAR, -1);
                        sk.useSpeaker(String.valueOf(mData.get(Calendar.YEAR )));
                        break;
                    case 1:
                        sk.tts.stop();
                        mData.add(Calendar.MONTH, -1);
                        int aux = mData.get(Calendar.MONTH);
                        sk.useSpeaker(String.valueOf(aux +1));
                        break;
                    case 2:
                        sk.tts.stop();
                        mData.add(Calendar.DAY_OF_MONTH, -1);
                        sk.useSpeaker(String.valueOf(mData.get(Calendar.DAY_OF_MONTH)));
                        break;
                    case 3:
                        sk.tts.stop();
                        mData.add(Calendar.HOUR, -1);
                        sk.useSpeaker(String.valueOf(mData.get(Calendar.HOUR_OF_DAY)));
                        break;
                    case 4:
                        sk.tts.stop();
                        mData.add(Calendar.MINUTE, -1);
                        sk.useSpeaker(String.valueOf(mData.get(Calendar.MINUTE)));
                        break;

                }

                setTextView();
                rootView.invalidate();
                break;
            case R.id.buttonSlecionarDataConfirmarData41:
                if ( numeroDaUnidadeAtual == 4 ) {

                    SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.UK);

                    String formatted = format1.format(Calendar.getInstance().getTime().getTime());

                    Date data = null;

                    try {
                        data = format1.parse(formatted);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    cfaInterface.dataSelecionada(data);

                    int year = mData.get(Calendar.YEAR);
                    int month = mData.get(Calendar.MONTH) +1; // Note: zero based!
                    int day = mData.get(Calendar.DAY_OF_MONTH);
                    int hour = mData.get(Calendar.HOUR_OF_DAY);
                    int minute = mData.get(Calendar.MINUTE);
                    try{
                        Thread.sleep(100);

                    }catch (Exception e){

                    }
                    sk.tts.stop();
                    sk.useSpeaker("Data final: Ano:" + "dia " + day + "do mês " + month + "do ano: " + year+ "às " + hour +" horas" + " ao minuto " + minute);
                    while(sk.tts.isSpeaking()){}

                    if ( mAvaliacao != null ) {
                        try {
                            cfaInterface.alterarFragmento(-1);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            cfaInterface.alterarFragmento(411);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }



                } else{
                    if(numeroDaUnidadeAtual == 0){
                        sk.tts.stop();
                        sk.useSpeaker("Ano confirmado para: " + String.valueOf(mData.get(Calendar.YEAR)));
                    }
                    if(numeroDaUnidadeAtual == 1){
                        int aux = mData.get(Calendar.MONTH);
                        sk.tts.stop();
                        sk.useSpeaker("Mês confirmado para: " + String.valueOf(aux +1));
                    }
                    if(numeroDaUnidadeAtual == 2){
                        sk.tts.stop();
                        sk.useSpeaker("Dia confirmado para: " + String.valueOf(mData.get(Calendar.DAY_OF_MONTH)));
                    }
                    if(numeroDaUnidadeAtual == 3){
                        sk.tts.stop();
                        sk.useSpeaker("Hora confirmado para: " + String.valueOf(mData.get(Calendar.HOUR_OF_DAY)));
                    }

                    numeroDaUnidadeAtual++;
                }


                rootView.invalidate();
                break;



        }
    }

}
