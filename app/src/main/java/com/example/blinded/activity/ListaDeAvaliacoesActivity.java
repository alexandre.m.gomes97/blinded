package com.example.blinded.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import  com.example.blinded.R;
import com.example.blinded.adapters.ListaAvaliacoesAdapter;
import com.example.blinded.database.DatabaseManager;
import com.example.blinded.interfaces.FromListaAvaliacoesAdapter;
import com.example.blinded.interfaces.VoltarParaAActivityInformacao;
import com.example.blinded.model.AulaAluno;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.AvaliacoesAluno;
import com.example.blinded.model.Speaker;
import com.example.blinded.viewModels.AlunoViewModel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class ListaDeAvaliacoesActivity extends AppCompatActivity implements FromListaAvaliacoesAdapter, VoltarParaAActivityInformacao {

    private ViewPager viewPager;
    private List<Avaliacao> listaAvaliacoes;
    private AlunoViewModel mAlunoViewModel;
    private static final String SHARED_PREFS = "sharedPreferences";
    public static final String TEXT = "idDoUtilizador";
    private SharedPreferences sharedPreferences;
    private String disciplinaSelecionada;
    private Button varBtnBackListaAvaliacoes;
    private Speaker sk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_avaliacoes);
        viewPager = findViewById(R.id.viewPagerListaAvaliacoesAluno);

        sharedPreferences = getSharedPreferences( SHARED_PREFS, MODE_PRIVATE);

        mAlunoViewModel = new AlunoViewModel(DatabaseManager.getHelper(this.getApplicationContext()));

        Bundle extras = getIntent().getExtras();
        assert extras != null;
        disciplinaSelecionada = String.valueOf( extras.get("disciplinaSelecionada") );

        try {
            assert disciplinaSelecionada != null;
            listaAvaliacoes = mAlunoViewModel.obterAvaliacoesPorDisciplina( Integer.valueOf( disciplinaSelecionada ) );
        } catch (ParseException e) {
            e.printStackTrace();
        }


        viewPager.setAdapter(new ListaAvaliacoesAdapter(this, listaAvaliacoes, this, this, disciplinaSelecionada));

        sk = new Speaker(this);
        varBtnBackListaAvaliacoes = findViewById(R.id.btnBackListaAvaliacoes);
        varBtnBackListaAvaliacoes.setClickable(false);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a38));
                varBtnBackListaAvaliacoes.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);
    }

    public void onClickHelp(View view)
    {
        varBtnBackListaAvaliacoes.setClickable(false);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a38));
                varBtnBackListaAvaliacoes.setClickable(true);
                //speak after 1000ms
            }
        }, 1000);
    }

    @Override
    protected void onStop(){
        super.onStop();

        sk.tts.stop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        sk.tts.stop();
    }

    @Override
    public void inscreverAlunoEmAvaliacao(Integer avaliacaoId) throws ParseException {

        String loggedUser = sharedPreferences.getString(TEXT, "");

        mAlunoViewModel.inscreverAlunoEmAvaliacao( Integer.valueOf(loggedUser), avaliacaoId );
    }

    @Override
    public void voltar(Integer fragmentoAtual) {

    }

    @Override
    public void fragmentoAtualInfo(Integer fragment) {

    }
}