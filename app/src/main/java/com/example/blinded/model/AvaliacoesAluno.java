package com.example.blinded.model;

public class AvaliacoesAluno {
    private String titulo;

    public AvaliacoesAluno(String _titulo){
        titulo = _titulo;
    }

    public String getTitulo(){return titulo;}
    public void setTitulo(String titulo){this.titulo = titulo;}
}
