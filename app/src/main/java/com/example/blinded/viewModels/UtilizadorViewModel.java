package com.example.blinded.viewModels;

import android.content.ContentValues;
import android.graphics.Paint;
import android.util.Pair;

import com.example.blinded.StringDateParser;
import com.example.blinded.database.DatabaseSQLITE;
import com.example.blinded.model.Disciplina;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

public class UtilizadorViewModel {

    private DatabaseSQLITE mDatabase;

    public UtilizadorViewModel(DatabaseSQLITE database, Integer idDoUtilizadorAtual) throws ParseException {
        mDatabase = database;
    }

    public UtilizadorViewModel(DatabaseSQLITE database) {
        mDatabase = database;
    }


    public void insertUtilizador(String utilizador_Nome, String utilizador_Email, Date utilizador_Data_de_nascimento, Integer utilizador_Tipo, String utilizador_Password) throws ParseException {

        mDatabase.insertUtilizador(utilizador_Nome, utilizador_Email, utilizador_Data_de_nascimento, utilizador_Tipo, utilizador_Password);

    }

    public long insertUtilizadorNomeEEmail(String utilizador_Nome, String utilizador_Email) throws ParseException {

        return mDatabase.insertUtilizadorBasic(utilizador_Nome, utilizador_Email);
    }


    public void updateUtilizadorPassword(String utilizador_id, String utilizador_Password ) throws ParseException {

        mDatabase.changeUtilizadorPassword( utilizador_id, utilizador_Password);

    }

    public void updateUtilizadorTipo( String utilizador_id, Integer tipo ) throws ParseException {

        mDatabase.changeUtilizadorTipo( utilizador_id, tipo);

    }


    // Retorna, se existir, se é professor ou aluno (0: Professor, 1: Aluno ou ao contrário) ou se não existe nenhuma entrada na base de dados com essas credênciais (-1) na primeira parte do pair. Na segunda, caso exista, retoana o id do utilziador (caso contrário -1).
    public Pair<Integer, Integer> ProcurarUtilizador(String username, String password) {

        ContentValues cv = mDatabase.procurarUtilizador( username, password );

        return new Pair<>( cv.getAsInteger("utilizador_Tipo"),cv.getAsInteger("utilizador_Id"));

    }

    // Verifica na base de dados se existe algum utilizador com o nome de utilziador passado como prâmetro. Retorna true se existir, false se não existir
    public Boolean ProcurarNomeDeUtilizador(String username) {
        //mDatabase.ProcurarNomeDeUtilizador( username); // TODO: COlocar método correto
        return true;
    }
}
