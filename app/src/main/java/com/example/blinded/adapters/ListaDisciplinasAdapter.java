package com.example.blinded.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.activity.DisciplinaAlunoActivity;
import com.example.blinded.activity.InicioAvaliacaoActivity;
import com.example.blinded.fragments.Informacao000Fragment;
import com.example.blinded.interfaces.FromListDisciplinasAdapter;
import com.example.blinded.interfaces.VoltarParaAActivityInformacao;
import com.example.blinded.model.AvaliacoesAluno;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

import java.util.List;

public class ListaDisciplinasAdapter extends PagerAdapter {
    private List<Disciplina> menuOptionList;
    Context context;
    FromListDisciplinasAdapter callback;
    private VoltarParaAActivityInformacao cfaInterface;
    private Speaker sk;

    public ListaDisciplinasAdapter(Context _context, List<Disciplina> _menuOptionList, FromListDisciplinasAdapter callback, VoltarParaAActivityInformacao cfaInterface){
        this.menuOptionList = _menuOptionList;
        this.context = _context;
        this.callback = callback;
        this.cfaInterface = cfaInterface;
    }


    @Override
    public int getCount() {

        if (menuOptionList == null || menuOptionList.size()==0 )
            return 1;
        else
            return menuOptionList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object){
        container.removeView((View)object);
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position){

        if (menuOptionList == null || menuOptionList.size()==0 ) {

            cfaInterface.fragmentoAtualInfo( 131 );

            //chamar fragment
            View view = LayoutInflater.from(context).inflate(R.layout.fragment_informacao_000,container,false);
            final TextView title = (TextView)view.findViewById(R.id.textViewInformacao000);
            title.setText("Não está inscrito em qualquer disciplina.");

            container.addView(view);
            return view;


        }

        sk = new Speaker(context);

        //chamar fragment
        View view = LayoutInflater.from(context).inflate(R.layout.perfil_disciplina_fragment,container,false);

        final Button varBtnDesinscreverPerfilDisciplina = (Button)view.findViewById(R.id.btnDesinscreverPerfilDisciplina);
        final Button varBtnVerAvaliacoesPerfilDisciplina = (Button)view.findViewById(R.id.btnVerAvaliacoesPerfilDisciplina);
        varBtnVerAvaliacoesPerfilDisciplina.setClickable(false);
        varBtnDesinscreverPerfilDisciplina.setClickable(false);

        final TextView title = (TextView)view.findViewById(R.id.textViewPerfilDisciplina);
        title.setText(menuOptionList.get(position).getNome());
        title.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sk.useSpeaker(menuOptionList.get(position).getNome());
                        varBtnVerAvaliacoesPerfilDisciplina.setClickable(true);
                        varBtnDesinscreverPerfilDisciplina.setClickable(true);
                        //speak after 1000ms
                    }
                }, 1000);
            }
        });

        final TextView info = (TextView) view.findViewById(R.id.textViewInformacoesDisciplina);
        info.setText(menuOptionList.get(position).getDescricao());
        info.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sk.useSpeaker(menuOptionList.get(position).getDescricao());
                        varBtnVerAvaliacoesPerfilDisciplina.setClickable(true);
                        varBtnDesinscreverPerfilDisciplina.setClickable(true);
                        //speak after 1000ms
                    }
                }, 1000);
            }
        });

        Button btnVerAvaliacoesPerfilDisciplina = view.findViewById(R.id.btnVerAvaliacoesPerfilDisciplina);
        btnVerAvaliacoesPerfilDisciplina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.buttonCodeFragmentCodeEDisciplinaId(0, 15, menuOptionList.get(position).getId() );
            }
        });


        Button btnDesinscreverPerfilDisciplina = view.findViewById(R.id.btnDesinscreverPerfilDisciplina);
        btnDesinscreverPerfilDisciplina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.buttonCodeFragmentCodeEDisciplinaId(1, 15, menuOptionList.get(position).getId() );

                Intent intent = new Intent(context, DisciplinaAlunoActivity.class);
                context.startActivity(intent);

            }
        });

        container.addView(view);
        return view;
    }

}