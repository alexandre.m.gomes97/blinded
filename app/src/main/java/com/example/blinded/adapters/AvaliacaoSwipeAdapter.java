package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.AvaliacaoApagarAvaliacao39Fragment;
import com.example.blinded.fragments.AvaliacaoEditarAvaliacao38Fragment;
import com.example.blinded.fragments.AvaliacaoVerInscricoes33Fragment;
import com.example.blinded.fragments.AvaliacaoVerQuestoes26Fragment;
import com.example.blinded.fragments.AvaliacaoVerResultadosMenu341Fragment;
import com.example.blinded.fragments.AvalicaoMenuAdicionarQuestao31Fragment;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;

public class AvaliacaoSwipeAdapter extends FragmentStatePagerAdapter {

    private Avaliacao mAvaliacao;
    private Disciplina mDisciplina;
    private Boolean mTemAvaliacoes;

    public AvaliacaoSwipeAdapter(FragmentManager fm, Avaliacao a, Disciplina d, Boolean temAvaliacoes) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mAvaliacao = a;
        mDisciplina = d;
        mTemAvaliacoes = temAvaliacoes;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                AvaliacaoVerQuestoes26Fragment pagina26 = new AvaliacaoVerQuestoes26Fragment();
                Bundle bundle26 = new Bundle();
                bundle26.putParcelable( "dis", mDisciplina );
                bundle26.putParcelable( "ava", mAvaliacao );
                pagina26.setArguments(bundle26);
                return pagina26;
            case  1:
                AvalicaoMenuAdicionarQuestao31Fragment pagina31 = new AvalicaoMenuAdicionarQuestao31Fragment();
                Bundle bundle31 = new Bundle();
                bundle31.putParcelable( "dis", mDisciplina );
                bundle31.putParcelable( "ava", mAvaliacao );
                pagina31.setArguments(bundle31);
                return pagina31;
            case 2:
                AvaliacaoVerInscricoes33Fragment pagina33 = new AvaliacaoVerInscricoes33Fragment();
                Bundle bundle33 = new Bundle();
                bundle33.putParcelable( "dis", mDisciplina );
                bundle33.putParcelable( "ava", mAvaliacao );
                pagina33.setArguments(bundle33);
                return pagina33;
            case 3:
                AvaliacaoEditarAvaliacao38Fragment pagina38 = new AvaliacaoEditarAvaliacao38Fragment();
                Bundle bundle38 = new Bundle();
                bundle38.putParcelable( "dis", mDisciplina );
                bundle38.putParcelable( "ava", mAvaliacao );
                pagina38.setArguments(bundle38);
                return pagina38;
            case 4:
                AvaliacaoApagarAvaliacao39Fragment pagina39 = new AvaliacaoApagarAvaliacao39Fragment();
                Bundle bundle39 = new Bundle();
                bundle39.putParcelable( "dis", mDisciplina );
                bundle39.putParcelable( "ava", mAvaliacao );
                pagina39.setArguments(bundle39);
                return pagina39;
            case 5:
                AvaliacaoVerResultadosMenu341Fragment pagina341 = new AvaliacaoVerResultadosMenu341Fragment();
                Bundle bundle341 = new Bundle();
                bundle341.putParcelable( "dis", mDisciplina );
                bundle341.putParcelable( "ava", mAvaliacao );
                pagina341.setArguments(bundle341);
                return pagina341;
            default:
                AvaliacaoVerQuestoes26Fragment pagina26d = new AvaliacaoVerQuestoes26Fragment();
                Bundle bundle26d = new Bundle();
                bundle26d.putParcelable( "dis", mDisciplina );
                bundle26d.putParcelable( "ava", mAvaliacao );
                pagina26d.setArguments(bundle26d);
                return pagina26d;
        }
    }




    @Override
    public int getCount() {
        if ( mTemAvaliacoes )
            return 6;
        else
            return 5;
    }
}
