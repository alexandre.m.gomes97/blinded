package com.example.blinded.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.blinded.R;
import com.example.blinded.database.DatabaseManager;
import com.example.blinded.fragments.LoginUsernameFragment;
import com.example.blinded.fragments.RegistarEmailFragment;
import com.example.blinded.model.Speaker;
import com.example.blinded.model.Utilizador;
import com.example.blinded.viewModels.UtilizadorViewModel;

import java.text.ParseException;

public class RegistarActivity extends AppCompatActivity {

    private LoginUsernameFragment usernameLoginFragment;
    private RegistarEmailFragment registarEmailFragment;
    private FragmentTransaction transaction;
    private String currentFragment;
    private Utilizador mUtilizador;
    private static final String SHARED_PREFS = "sharedPreferences";
    public static final String TEXT = "idDoUtilizador";
    public static final String TEXT_REGISTERING = "registando";
    private UtilizadorViewModel utilizadorViewModel;
    private Speaker tts;
    private TextView textInputAtual;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registar);
        usernameLoginFragment = new LoginUsernameFragment();
        registarEmailFragment = new RegistarEmailFragment();
        currentFragment = new String();
        mUtilizador = new Utilizador();

        utilizadorViewModel = new UtilizadorViewModel(DatabaseManager.getHelper(this));

        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout, usernameLoginFragment);
        currentFragment = "userLogin";
        tts = new Speaker(this);
        transaction.commit();
    }

    @Override
    protected void onStop(){
        super.onStop();

        tts.tts.stop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        tts.tts.stop();
    }

    //speaker
    public void speakerOnClick(View view)
    {
        if(currentFragment == "userLogin")
            tts.useSpeaker(getResources().getString(R.string.a12));
        else if(currentFragment == "registarEmail")
            tts.useSpeaker(getResources().getString(R.string.a13));
    }

    public void penOnClick(View view){
        switch (currentFragment){
            case "userLogin":
                textInputAtual = findViewById(R.id.editText);
                onClickDelete(view);
                break;
            case "registarEmail":
                textInputAtual = findViewById(R.id.editTextEmailRegistar);
                onClickDelete(view);
                break;
            default:
                break;
        }
    }

    public void onClickDelete(View view){
        tts.useSpeaker(textInputAtual.getText().toString());
        while(tts.tts.isSpeaking()){}
        String aux = removeLastCharacter(textInputAtual.getText().toString());
        textInputAtual.setText(aux);
    }

    public static String removeLastCharacter(String str) {
        String result = null;
        if ((str != null) && (str.length() > 0)) {
            result = str.substring(0, str.length() - 1);
        }
        return result;
    }

    public void OnButtonNextClick(View view) throws ParseException {
        switch (currentFragment){
            case "userLogin":
                // Set nome do utilizador
                LoginUsernameFragment fragment = (LoginUsernameFragment) getSupportFragmentManager().findFragmentById(R.id.frameLayout);
                assert fragment != null;
                mUtilizador.setNome(  fragment.manageClick());

                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frameLayout, registarEmailFragment);
                currentFragment = "registarEmail";
                transaction.commit();
                break;
            case "registarEmail":
                RegistarEmailFragment fragmentEmail = (RegistarEmailFragment) getSupportFragmentManager().findFragmentById(R.id.frameLayout);
                assert fragmentEmail != null;
                mUtilizador.setEmail(  fragmentEmail.manageClick());

                long insertUserReturn = utilizadorViewModel.insertUtilizadorNomeEEmail(mUtilizador.getNome(), mUtilizador.getEmail()); // Tentar armazenar o utilizador na base de dados e  obter o id
                if (insertUserReturn != -1) {

                    // Caso tenha sido armazenado com sucesso, armazena o id do utilziador nas sharedPreferences
                    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    String loggedUserId = String.valueOf( insertUserReturn );
                    Boolean registering = true;
                    editor.putString(TEXT, loggedUserId);
                    editor.putString(TEXT_REGISTERING, registering.toString());
                    editor.apply();
                }


                Intent intent = new Intent(getApplicationContext(), RegistarPasswordActivity.class);
                startActivity(intent);
                break;
        }
    }
}
