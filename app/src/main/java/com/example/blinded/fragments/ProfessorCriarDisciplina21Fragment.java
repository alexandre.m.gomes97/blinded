package com.example.blinded.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Speaker;
import com.google.android.material.textfield.TextInputEditText;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

public class ProfessorCriarDisciplina21Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private EditText textInputEditTextNomeDadisciplina;
    private EditText textInputEditTextDescricao;
    private Speaker sk;
    private TextView textViewCriarDisciplina;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_criar_disciplina_21, container, false);
        sk = new Speaker(getContext());

        textInputEditTextNomeDadisciplina = rootView.findViewById(R.id.textInputEditTextNomeDadisciplina);
        textInputEditTextDescricao = rootView.findViewById(R.id.textInputEditTextDescricao);

        Button cl = rootView.findViewById(R.id.criarDisciplina21);
        textViewCriarDisciplina = rootView.findViewById(R.id.textViewCriarDisciplina);

        textViewCriarDisciplina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sk.useSpeaker("Diga o nome da disciplina, um segundo após esta frase terminar");
                while(sk.tts.isSpeaking()){}
                getSpeechInput(v);
            }
        });
        cl.setOnClickListener(this);


        return rootView;

    }


    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 10:
                if(resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    textInputEditTextNomeDadisciplina.setText(result.get(0));
                    sk.useSpeaker("Agora diga a descrição, um segundo após esta frase terminar");
                    while(sk.tts.isSpeaking()){}
                    getSpeechInput2();
                }
                break;
            case 20:
                if(resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    textInputEditTextDescricao.setText(result.get(0));
                }
                break;
        }
    }

    public void getSpeechInput(View view){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        startActivityForResult(intent,10);
    }

    public void getSpeechInput2(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        startActivityForResult(intent,20);
    }
    @Override
    public void onResume() {
        super.onResume();
        cfaInterface.fragmentoAtual(21);
        sk.useSpeaker(getResources().getString(R.string.a45));
    }

    @Override
    public void onStop() {
        super.onStop();
        sk.tts.stop();
    }

    @Override
    public void onClick(View v) {

        String d = textInputEditTextNomeDadisciplina.getText().toString();
        String l = textInputEditTextDescricao.getText().toString();


        if (v.getId() == R.id.criarDisciplina21) {
            try{
                Thread.sleep(200);
            }catch (Exception e){}
            sk.tts.stop();
            sk.useSpeaker(" A disciplina " + d + ", com a descrição" + l + ", foi criada com sucesso!");
            try {
                    cfaInterface.criarDisciplina(d, l);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    public void setProfessorCriarDisciplina21Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }
}
