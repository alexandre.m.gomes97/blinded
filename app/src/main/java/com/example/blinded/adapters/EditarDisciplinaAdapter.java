package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.AvaliacoesPorDisciplina25Fragment;
import com.example.blinded.fragments.PaginaInicialDisciplinaEditarFormulario43Fragment;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;

import java.util.ArrayList;

public class EditarDisciplinaAdapter extends FragmentStatePagerAdapter {

    private Disciplina mDisciplina;

    public EditarDisciplinaAdapter(FragmentManager fm, Disciplina d) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mDisciplina = d;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        PaginaInicialDisciplinaEditarFormulario43Fragment pagina43 = new PaginaInicialDisciplinaEditarFormulario43Fragment();
        Bundle bundle43 = new Bundle();
        bundle43.putParcelable( "dis", mDisciplina );
        pagina43.setArguments(bundle43);
        return pagina43;

    }

    @Override
    public int getCount() {
        return 1;
    }
}
