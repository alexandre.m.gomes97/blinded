package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.AvaliacaoInscricoesResultados34Fragment;
import com.example.blinded.fragments.AvaliacaoInscricoesResultadosVerCotacao36Fragment;
import com.example.blinded.fragments.AvaliacaoVerInscricoesInscrito331Fragment;
import com.example.blinded.fragments.Informacao000Fragment;
import com.example.blinded.model.Aluno;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;

import java.util.ArrayList;

public class ResultadosSwipeAdapter extends FragmentStatePagerAdapter {

    private Avaliacao mAvaliacao;
    private Disciplina mDisciplina;
    private ArrayList<Aluno> mAlunos;

    public ResultadosSwipeAdapter(FragmentManager fm, Avaliacao a, Disciplina d, ArrayList<Aluno> alunos ) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mAvaliacao = a;
        mDisciplina = d;
        mAlunos = alunos;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        if ( mAlunos == null || mAlunos.size() == 0 ) {
            Informacao000Fragment pagina000 = new Informacao000Fragment();
            Bundle bundle00 = new Bundle();
            bundle00.putString( "data", "Esta avaliação ainda não tem qualquer resultado" );
            bundle00.putInt( "fragAtual", 34);
            pagina000.setArguments(bundle00);
            return pagina000;
        } else {
            AvaliacaoInscricoesResultados34Fragment pagina34 = new AvaliacaoInscricoesResultados34Fragment();
            Bundle bundle34 = new Bundle();
            bundle34.putParcelable("dis", mDisciplina);
            bundle34.putParcelable("ava", mAvaliacao);
            bundle34.putParcelable("alu", mAlunos.get(position));
            pagina34.setArguments(bundle34);
            return pagina34;
        }
    }




    @Override
    public int getCount() {
        if ( mAlunos == null || mAlunos.size() == 0 ) {
            return 1;
        } else {
            return mAlunos.size();
        }
    }

}
