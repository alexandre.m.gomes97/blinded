package com.example.blinded.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Aluno;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.QuestaoAluno;

import java.text.ParseException;

public class AvaliacaoInscricoesResultadosVerCotacao36Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Disciplina mDisciplina;
    private Avaliacao mAvaliacao;
    private Aluno mAluno;
    private QuestaoAluno mQuestaoAluno;
    private Integer clickes = 0;
    private EditText titlePercentagemDada, questao;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        mDisciplina = bundle.getParcelable("dis");
        mAvaliacao = bundle.getParcelable("ava");
        mAluno = bundle.getParcelable("alu");
        mQuestaoAluno = bundle.getParcelable("que");


        View rootView = inflater.inflate(R.layout.fragment_avaliacao_incricoes_resultados_vercotacao_36, container, false);

        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_36);
        title.setText( mDisciplina.getNome());

        final TextView titleAval = rootView.findViewById(R.id.textViewNomeDaAvaliacao_36);
        titleAval.setText( mAvaliacao.getNome());

        final TextView titleAluNome= rootView.findViewById(R.id.textViewNomeDoAluno_36);
        titleAluNome.setText( mAluno.getNome());

        final TextView titleQuestaoNome= rootView.findViewById(R.id.textViewNumeroDaQuestao_36);
        String numeroQuestao =  "Pergunta " + String.valueOf( mQuestaoAluno.getNumero());
        titleQuestaoNome.setText( numeroQuestao );

        final TextView titleAluCotacao = rootView.findViewById(R.id.textViewCotacaoDaPergunta_36);
        String cotacao = String.valueOf( mQuestaoAluno.getCotacao()) + " valores";
        titleAluCotacao.setText( cotacao );

        questao= rootView.findViewById(R.id.textInputEditTextPergunta_36);
        questao.setText( mQuestaoAluno.getTitulo());

        titlePercentagemDada= rootView.findViewById(R.id.textInputEditTextCotacaoDadaAoAluno_36);
        titlePercentagemDada.setText( "100%");

        Button cl = rootView.findViewById(R.id.buttonEditarCotacaoDaPergunta_36);
        cl.setOnClickListener(this);

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        cfaInterface.fragmentoAtual(36);
    }


    @Override
    public void onClick(View v) {

        clickes++;

        if ( clickes == 1 ) {
            ((Button) v).setText("Confirmar alterações");
            titlePercentagemDada.setEnabled(true); questao.setEnabled(true);
        } else if (v.getId() == R.id.buttonEditarCotacaoDaPergunta_36) {
            // TODO: passar dados alterados para a activity
            try {
                cfaInterface.alterarFragmento(34);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    public void setAvaliacaoInscricoesResultadosVerCotacao36Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }
}
