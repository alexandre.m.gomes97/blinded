package com.example.blinded.viewModels;

import android.content.ContentValues;

import com.example.blinded.StringDateParser;
import com.example.blinded.database.DatabaseSQLITE;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.QuestaoAluno;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AlunoViewModel {
    private DatabaseSQLITE mDatabase;
    private Integer idDoUtilizadorAtual;
    private Integer idDaDisciplinaSeleconada = -1;

    public AlunoViewModel(DatabaseSQLITE database, Integer idDoUtilizadorAtual) throws ParseException {
        mDatabase = database;
        this.idDoUtilizadorAtual = idDoUtilizadorAtual;
    }

    public AlunoViewModel(DatabaseSQLITE database) {
        mDatabase = database;
    }


    // TODO: Testar
    public List<Disciplina> obterTodasAsDisciplinas() throws ParseException {

        List<Disciplina> disciplinaList = new ArrayList<>();

        for ( ContentValues cv : mDatabase.getAllDisciplinas()
             ) {
            Disciplina d = new Disciplina();
            d.setId(cv.getAsInteger("disciplina_Id"));
            d.setNome(cv.getAsString("disciplina_Nome"));
            //d.setData( new SimpleDateFormat("dd/MM/yyyy", Locale.UK).parse(cv.getAsString("disciplina_Data_de_Criacao")) );
            d.setDescricao( cv.getAsString("disciplina_Descricao"));
            disciplinaList.add(d);
        }

        return disciplinaList;
    }

    public void isncreverAluno( Integer alunoId, Integer disciplinId ) {

        mDatabase.insertInscricao( alunoId, disciplinId ); // TODO: Colocar método correto

    }

    public void desinscreverALunoDeDisciplina(Integer idAluno, Integer disciplinaId) {

        mDatabase.DesinscreverAlunoDeDisciplina( idAluno, disciplinaId );

    }

    public void desinscreverALunoDeAvaliacao(Integer idAluno, Integer avalaicaoId) {

        mDatabase.DesinscreverAlunoDeAvaliacao( idAluno, avalaicaoId );

    }

    public List<Disciplina> obterInscricoesDoAluno( Integer alunoId ) throws ParseException {

        List<Disciplina> disciplinaList = new ArrayList<>();

        for ( ContentValues cv : mDatabase.obterInscricoesDoAluno( alunoId ) // TODO: Colocar método correto
        ) {
            Disciplina d = new Disciplina();
            d.setId(cv.getAsInteger("disciplina_Id"));
            d.setNome(cv.getAsString("disciplina_Nome"));
            //d.setData( new SimpleDateFormat("dd/MM/yyyy", Locale.UK).parse(cv.getAsString("disciplina_Data_de_Criacao")) );
            d.setDescricao( cv.getAsString("disciplina_Descricao"));
            disciplinaList.add(d);
        }

        return disciplinaList;

    }

    public List<Avaliacao> obterAvaliacoesPorDisciplina(Integer disciplinaId) throws ParseException {

        idDaDisciplinaSeleconada = disciplinaId;

        ArrayList<Avaliacao> listaDeDisciplinas = new ArrayList<>();
        Avaliacao avaliacao;
        for (ContentValues cv: mDatabase.getAvaliacoesPorDisciplinaId(idDaDisciplinaSeleconada.toString())
        ) {
            avaliacao = new Avaliacao(
                    cv.getAsInteger("avaliacao_Id"),
                    cv.getAsString("avaliacao_Nome"),
                    StringDateParser.parseDate(cv.getAsString("avaliacao_Data_da_Avaliacao"))
            );
            listaDeDisciplinas.add(avaliacao);
        }

        return listaDeDisciplinas;

    }

    public List<Avaliacao> obterAvaliacoesPorDisciplinaIdEUtilizadorId(Integer disciplinaId, Integer utilizadorId) throws ParseException {

        idDaDisciplinaSeleconada = disciplinaId;

        ArrayList<Avaliacao> listaDeDisciplinas = new ArrayList<>();
        Avaliacao avaliacao;
        for (ContentValues cv: mDatabase.getAvaliacoesPorDisciplinaIdEUtilizadorId(idDaDisciplinaSeleconada.toString(), utilizadorId.toString()) // TODO: Testar, Testar, Testar, Testar!!!!
        ) {
            avaliacao = new Avaliacao(
                    cv.getAsInteger("avaliacao_Id"),
                    cv.getAsString("avaliacao_Nome"),
                    StringDateParser.parseDate(cv.getAsString("avaliacao_Data_da_Avaliacao"))
            );
            listaDeDisciplinas.add(avaliacao);
        }

        return listaDeDisciplinas;

    }




    public void inscreverAlunoEmAvaliacao(Integer alunoId, Integer avaliacaoId) throws ParseException {

        mDatabase.insertInscricaoUtilizadorAvaliacao( alunoId, avaliacaoId);

    }


    public void inserirResposta( String resposta, Integer utilizadorId, Integer questaoId) throws ParseException {

        mDatabase.insertResposta( resposta, utilizadorId, questaoId); // TODO: Testart

    }


    public ArrayList<QuestaoAluno> getQuestoesPorAvaliacaoId( String avaliacaoId) {

        ArrayList<QuestaoAluno> listaDeDisciplinas = new ArrayList<>();
        QuestaoAluno questaoAluno;
        for (ContentValues cv: mDatabase.getQuestoesPorAvaliacaoId( avaliacaoId ) // TODO testar
        ) {
            questaoAluno = new QuestaoAluno(
                    cv.getAsInteger("questao_Id"),
                    cv.getAsString("questao_Titulo"),
                    cv.getAsInteger("questao_Numero"),
                    cv.getAsDouble("questao_Cotacao"),
                    cv.getAsString("questao_Descricao")
            );

            listaDeDisciplinas.add(questaoAluno);
        }

        return listaDeDisciplinas;

    }




    public List<Disciplina> obterTodasAsDisciplinasEmQueOUtilizadorNaoEstaInscrito(Integer alunoId) {

        List<Disciplina> retorno = new ArrayList<>();


        List<Disciplina> disciplinaList = new ArrayList<>();

        for ( ContentValues cv : mDatabase.getAllDisciplinas()
        ) {
            Disciplina d = new Disciplina();
            d.setId(cv.getAsInteger("disciplina_Id"));
            d.setNome(cv.getAsString("disciplina_Nome"));
            //d.setData( new SimpleDateFormat("dd/MM/yyyy", Locale.UK).parse(cv.getAsString("disciplina_Data_de_Criacao")) );
            d.setDescricao( cv.getAsString("disciplina_Descricao"));
            disciplinaList.add(d);
        }


        List<Disciplina> inscricoes = new ArrayList<>();

        for ( ContentValues cv : mDatabase.obterInscricoesDoAluno( alunoId )
        ) {
            Disciplina d = new Disciplina();
            d.setId(cv.getAsInteger("disciplina_Id"));
            d.setNome(cv.getAsString("disciplina_Nome"));
            //d.setData( new SimpleDateFormat("dd/MM/yyyy", Locale.UK).parse(cv.getAsString("disciplina_Data_de_Criacao")) );
            d.setDescricao( cv.getAsString("disciplina_Descricao"));
            inscricoes.add(d);
        }

        int count = 0;

        for (Disciplina d:
        disciplinaList) {

            for (Disciplina d1:
            inscricoes) {

                if (d.getId().equals( d1.getId())) {
                    break;
                }

                count++;
            }

            if (count == inscricoes.size())
                retorno.add(d);
            count = 0;
        }

        return retorno;




    }
}
