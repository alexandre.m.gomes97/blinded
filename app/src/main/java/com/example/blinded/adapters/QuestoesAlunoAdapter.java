package com.example.blinded.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.example.blinded.R;
import com.example.blinded.activity.ListaDeAvaliacoesActivity;
import com.example.blinded.activity.MainActivityAluno;
import com.example.blinded.activity.QuestaoAlunoActivity;
import com.example.blinded.fragments.Informacao000Fragment;
import com.example.blinded.interfaces.SubmeterResposta;
import com.example.blinded.model.QuestaoAluno;
import com.example.blinded.model.Speaker;

import java.text.ParseException;
import java.util.List;

public class QuestoesAlunoAdapter extends PagerAdapter {
    private List<QuestaoAluno> menuOptionList;
    private Context context;
    private SubmeterResposta submeterInterface;
    private Speaker sk;

    public QuestoesAlunoAdapter(Context _context, List<QuestaoAluno> _menuOptionList, SubmeterResposta submeterInterface){
        this.menuOptionList = _menuOptionList;
        this.context = _context;
        this.submeterInterface = submeterInterface;
    }

    @Override
    public int getCount() {
        if ( menuOptionList == null || menuOptionList.size() == 0 )
            return 1;
        else
            return menuOptionList.size() + 1;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object){
        container.removeView((View)object);
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position){
        View view;


        if ( menuOptionList == null || menuOptionList.size() == 0 ) {

            //if (position == 0) {

                //chamar fragment
                view = LayoutInflater.from(context).inflate(R.layout.fragment_informacao_000,container,false);
                final TextView title = (TextView)view.findViewById(R.id.textViewInformacao000);
                title.setText("Esta avaliacao não contem qualquer questao associada! Clique em qualquer ponto do ecrã para voltar ao menu de avaliaações.");

                view.findViewById(R.id.tudo).setOnClickListener(new View.OnClickListener(){
                    @Override
                    public  void onClick(View view){
                        Intent intent = new Intent(context, MainActivityAluno.class);
                        context.startActivity(intent);
                    }
                });

                container.addView(view);
                return view;

//            }else {
//                view = LayoutInflater.from(context).inflate(R.layout.fragment_avaliacao_finalizar,container,false);
//                final TextView title = (TextView)view.findViewById(R.id.textViewInfoFinaisAvaliacao);
//                title.setText(menuOptionList.get(position).getTitulo());
//
//                view.findViewById(R.id.buttonFinalizarAvaliacao).setOnClickListener(new View.OnClickListener(){
//                    @Override
//                    public  void onClick(View view){
//                        Intent intent = new Intent(context, ListaDeAvaliacoesActivity.class);
//                        context.startActivity(intent);
//                    }
//                });
//
//                container.addView(view);
//                return view;
//            }



        } else {
            sk = new Speaker(context);

            //chamar fragment
            //verificar se a posição contem questao ou se contem fim da avalicao
            if (position < menuOptionList.size()) {
                view = LayoutInflater.from(context).inflate(R.layout.fragment_avaliacao_questao_responder_desenvolvimento, container, false);
                final EditText resposta = (EditText) view.findViewById(R.id.editText_Resposta);

                final TextView title = (TextView) view.findViewById(R.id.textViewQuestao);
                title.setText(menuOptionList.get(position).getTitulo());
                title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                sk.useSpeaker(menuOptionList.get(position).getTitulo());
                                //speak after 1000ms
                            }
                        }, 1000);
                    }
                });


                view.findViewById(R.id.btnQuestoesSubmeterReposta).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view1) {
                        String s = resposta.getText().toString();
                        try {
                            submeterInterface.submeterDadosDaResposta( menuOptionList.get(position).getId(), s );
                            Toast.makeText(context, "Mensagem submetida com sucesso", Toast.LENGTH_LONG).show();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } else {
                view = LayoutInflater.from(context).inflate(R.layout.fragment_avaliacao_finalizar, container, false);
                final TextView title = (TextView) view.findViewById(R.id.textViewInfoFinaisAvaliacao);
                title.setText("Finalizar");
                title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                sk.useSpeaker(title.getText().toString());
                                //speak after 1000ms
                            }
                        }, 1000);
                    }
                });

                view.findViewById(R.id.buttonFinalizarAvaliacao).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, ListaDeAvaliacoesActivity.class);
                        context.startActivity(intent);
                    }
                });
            }


            container.addView(view);
            return view;

        }
    }
}
