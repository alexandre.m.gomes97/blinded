package com.example.blinded.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import com.example.blinded.R;
import com.example.blinded.database.DatabaseManager;
import com.example.blinded.model.Speaker;
import com.example.blinded.viewModels.UtilizadorViewModel;

import java.text.ParseException;

public class RegistarSelecionarPerfilActivity extends AppCompatActivity {

    private static final String SHARED_PREFS = "sharedPreferences";
    public static final String TEXT = "idDoUtilizador";
    public static final String TEXT_REGISTERING = "registando";
    private UtilizadorViewModel utilizadorViewModel;
    private SharedPreferences sharedPreferences;
    private String loggedUserId;
    private Speaker sk;
    private Button btnProfessor;
    private Button btnAluno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registar_selecionar_perfil);

        // Obter das shared preferences o id do utilizadro que se está a tentar armazenar
        sharedPreferences = getSharedPreferences( SHARED_PREFS, MODE_PRIVATE);
        loggedUserId = sharedPreferences.getString(TEXT, "");

        btnProfessor = findViewById(R.id.btnPasswordLeft2);
        btnAluno = findViewById(R.id.btnRegistarProfessor);
        btnProfessor.setClickable(false);
        btnAluno.setClickable(false);

        sk = new Speaker(this);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a16));
                btnProfessor.setClickable(true);
                btnAluno.setClickable(true);
            }
        }, 1000);
        utilizadorViewModel = new UtilizadorViewModel(DatabaseManager.getHelper(this));
        utilizadorViewModel = new UtilizadorViewModel(DatabaseManager.getHelper(this));
    }

    @Override
    protected void onStop(){
        super.onStop();

        sk.tts.stop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        sk.tts.stop();
    }

    public void onClickHelp(View view)
    {
        btnProfessor.setClickable(false);
        btnAluno.setClickable(false);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a15));
                btnProfessor.setClickable(true);
                btnAluno.setClickable(true);
            }
        }, 1000);
        utilizadorViewModel = new UtilizadorViewModel(DatabaseManager.getHelper(this));
    }

    public void onClickAluno(View view) throws ParseException {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TEXT_REGISTERING, "false");
        editor.apply();

        utilizadorViewModel.updateUtilizadorTipo(loggedUserId, 1);

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    public void onClickProfessor(View view) throws ParseException {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TEXT_REGISTERING, "false");
        editor.apply();

        utilizadorViewModel.updateUtilizadorTipo(loggedUserId, 0);

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

}
