package com.example.blinded.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.google.android.material.textfield.TextInputEditText;
import com.example.blinded.model.Speaker;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class AvaliacaoQuestaoAdicionarQuestao32Fragment extends Fragment implements View.OnClickListener {
    private ComunicarFragmentActivity cfaInterface;
    private Disciplina mDisciplina;
    private Avaliacao mAvaliacao;
    private TextView title, titleAval;
    private TextInputEditText pergunta, cotacao;
    private TextView AdicionarQuestao;
    private EditText editTextPergunta;
    private EditText editTextCotacao;
    private Speaker sk;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        mDisciplina = bundle.getParcelable("dis");
        mAvaliacao = bundle.getParcelable("ava");
        sk = new Speaker(getContext());


        View rootView = inflater.inflate(R.layout.fragment_avaliacao_questao_adicionar_questao_32, container, false);

        title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_32);
        title.setText( mDisciplina.getNome());

        pergunta = rootView.findViewById(R.id.textInputEditTextPergunta_32);

        cotacao = rootView.findViewById(R.id.textInputEditTextCotacao_32);

        titleAval = rootView.findViewById(R.id.textViewNomeDaAvaliacao_32);
        titleAval.setText( mAvaliacao.getNome());

        Button cl = rootView.findViewById(R.id.buttonAdicionarQuestao32);
        cl.setOnClickListener(this);
        editTextPergunta = rootView.findViewById(R.id.textInputEditTextPergunta_32);
        editTextCotacao = rootView.findViewById(R.id.textInputEditTextCotacao_32);

        AdicionarQuestao = rootView.findViewById(R.id.textViewAdicionarQuestao_32);

        AdicionarQuestao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sk.useSpeaker("Diga a questão, um segundo após esta frase terminar");
                while(sk.tts.isSpeaking()){}
                getSpeechInput(v);
            }
        });
        return rootView;

    }

    public void getSpeechInput(View view){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        startActivityForResult(intent,10);
    }

    public void getSpeechInput2(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        startActivityForResult(intent,20);
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 10:
                if(resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    editTextPergunta.setText(result.get(0));
                    sk.useSpeaker("Agora diga a cotação, um segundo após esta frase terminar");
                    while(sk.tts.isSpeaking()){}
                    getSpeechInput2();
                }
                break;
            case 20:
                if(resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    editTextCotacao.setText(result.get(0));
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        cfaInterface.fragmentoAtual(32);
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.buttonAdicionarQuestao32) {
            // Enviar dados de criação de nova questao para a activity
            String perguntaTexto = pergunta.getText().toString();
            String cotacaoTexto = cotacao.getText().toString();
            try {
                cfaInterface.adicionarQuestao( perguntaTexto , Double.valueOf( cotacaoTexto ));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {
                cfaInterface.alterarFragmento(31);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    public void setAvaliacaoQuestaoAdicionarQuestao32Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }


}
