package com.example.blinded.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

public class DisciplinaCriada22Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Integer mDiscipinaId;
    private Speaker sk;
    private  String nomeDisciplina;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        final Disciplina d = bundle.getParcelable("dis");

        sk = new Speaker(getContext());

        mDiscipinaId = d.getId();
        nomeDisciplina = d.getNome();

        View rootView = inflater.inflate(R.layout.fragment_disciplina_criada_22, container, false);

        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada);
        title.setText( d.getNome());


//        final ImageView imagem = rootView.findViewById(R.id.imageViewDisciplina);
//        imagem.setImageResource( d.getImage() );


        ConstraintLayout cl = rootView.findViewById(R.id.constraintLayout22);
        cl.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        cfaInterface.fragmentoAtual( 22 );
        sk.useSpeaker(nomeDisciplina);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.constraintLayout22:
                cfaInterface.disciplinaOnClick( mDiscipinaId );
                break;
        }

    }


    public void setDisciplinaCriada22Fragment(Disciplina20Activity disciplina20Activity) {

        cfaInterface = disciplina20Activity;

    }
}
