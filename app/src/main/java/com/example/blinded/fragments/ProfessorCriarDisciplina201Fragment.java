package com.example.blinded.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

import java.text.ParseException;

public class ProfessorCriarDisciplina201Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Speaker sk;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_professor_criar_disciplina_menu_201, container, false);

        sk = new Speaker(getContext());
        Button cl = rootView.findViewById(R.id.buttonCriarDisciplina201);
        cl.setOnClickListener(this);

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        cfaInterface.fragmentoAtual(201);
        sk.useSpeaker("Criar disciplina");
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.buttonCriarDisciplina201) {
            try {
                cfaInterface.alterarFragmento( 21 );
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public void setProfessorCriarDisciplina201Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }


}
