package com.example.blinded.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.blinded.R;
import com.example.blinded.adapters.AdicionarQuestaoSwipeAdapter;
import com.example.blinded.adapters.AvaliacaoSwipeAdapter;
import com.example.blinded.adapters.AvaliacoesSwipeAdapter;
import com.example.blinded.adapters.CotacaoPorPerguntaSwipeAdapter;
import com.example.blinded.adapters.CriarDisciplinaAdapter;
import com.example.blinded.adapters.DisciplinaSwipeAdapter;
import com.example.blinded.adapters.DisciplinasSwipeAdapter;
import com.example.blinded.adapters.EditarAvaliacaoSwipeAdapter;
import com.example.blinded.adapters.EditarDisciplinaAdapter;
import com.example.blinded.adapters.EditarQuestaoSwipeAdapter;
import com.example.blinded.adapters.IncricoesSwipeAdapter;
import com.example.blinded.adapters.QuestoesSwipeAdapter;
import com.example.blinded.adapters.ResultadosSwipeAdapter;
import com.example.blinded.adapters.SelecionarNomeSwipeAdapter;
import com.example.blinded.database.DatabaseManager;
import com.example.blinded.dialogs.ClickToCancelOperationDialog;
import com.example.blinded.fragments.AvaliacaoApagarAvaliacao39Fragment;
import com.example.blinded.fragments.AvaliacaoCriarAvaliacaoSelecionarNome411Fragment;
import com.example.blinded.fragments.AvaliacaoEditarAvaliacao38Fragment;
import com.example.blinded.fragments.AvaliacaoInscricoesResultados34Fragment;
import com.example.blinded.fragments.AvaliacaoInscricoesResultadosVerCotacao36Fragment;
import com.example.blinded.fragments.AvaliacaoQuestao27Fragment;
import com.example.blinded.fragments.AvaliacaoQuestaoAdicionarQuestao32Fragment;
import com.example.blinded.fragments.AvaliacaoQuestaoEditar30Fragment;
import com.example.blinded.fragments.AvaliacaoVerInscricoes33Fragment;
import com.example.blinded.fragments.AvaliacaoVerInscricoesInscrito331Fragment;
import com.example.blinded.fragments.AvaliacaoVerQuestoes26Fragment;
import com.example.blinded.fragments.AvaliacaoVerResultadosMenu341Fragment;
import com.example.blinded.fragments.AvaliacoesPorDisciplina25Fragment;
import com.example.blinded.fragments.AvalicaoMenuAdicionarQuestao31Fragment;
import com.example.blinded.fragments.DisciplinaCriada22Fragment;
import com.example.blinded.fragments.Informacao000Fragment;
import com.example.blinded.fragments.PaginaInicialDisciplinaCriarAvaliacao41Fragment;
import com.example.blinded.fragments.PaginaInicialDisciplinaDesativer44Fragment;
import com.example.blinded.fragments.PaginaInicialDisciplinaEditar42Fragment;
import com.example.blinded.fragments.PaginaInicialDisciplinaEditarFormulario43Fragment;
import com.example.blinded.fragments.PaginaInicialDisciplinaVerAvaliacoes24Fragment;
import com.example.blinded.fragments.ProfessorCriarDisciplina201Fragment;
import com.example.blinded.fragments.ProfessorCriarDisciplina21Fragment;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.interfaces.VoltarParaAActivityInformacao;
import com.example.blinded.model.Aluno;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.QuestaoAluno;
import com.example.blinded.model.Speaker;
import com.example.blinded.viewModels.ProfessorViewModel;
import com.google.android.material.tabs.TabLayout;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

// A rotatividade no ViewPager foi feito através desta questão https://stackoverflow.com/a/13787543/13149102 e deste reposotório https://github.com/antonyt/InfiniteViewPager
// A construção de um View pager com multiplos fragmentos distintos foi baseada neste video https://www.youtube.com/watch?v=cKweRL0rHBc

public class Disciplina20Activity extends AppCompatActivity implements ComunicarFragmentActivity, VoltarParaAActivityInformacao {

    private static final String SHARED_PREFS = "sharedPreferences";
    public static final String TEXT = "idDoUtilizador";
    private ProfessorViewModel professorViewModel;
    private Integer idDoUtilziadorLogado;
    private Date dataSelecionada;

    private ViewPager fragmentContainer;
    private TabLayout mTabLayout;
    private Integer mFragmentoAtual;
    private ArrayList<Disciplina> mDisciplina;
    private ArrayList<Avaliacao> mAvaliacoes;
    private ArrayList<QuestaoAluno> mQuestoes;
    private Integer mDisciplinaSelecionada;
    private Integer mAvaliacaoSelecionada;
    private Integer mQuestaoSelecionada;
    private Integer mAlunoSelecionado;
    private ArrayList<Aluno> mAlunos;
    private Speaker sk;


    private ClickToCancelOperationDialog mClickToCancelDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disciplina_20);
        sk = new Speaker(this);
//        SharedPreferences sharedPreferences = getSharedPreferences( SHARED_PREFS, MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        String loggedUserId = "1";
//        editor.putString(TEXT,loggedUserId);
//        editor.apply();

        // Obter das sharedPreferences o id do utilizador, que foi guaradado quando iniciou sessão na aplicação. Este Id vai ser utilizado para fazer o acesso à base de dados.
        SharedPreferences sharedPreferences = getSharedPreferences( SHARED_PREFS, MODE_PRIVATE);
        idDoUtilziadorLogado = Integer.valueOf( sharedPreferences.getString(TEXT, "") );


        try {
            professorViewModel = new ProfessorViewModel(DatabaseManager.getHelper(this), 1);

            //professorViewModel.insertUtilizador("Manel Coiso", "email@dele", new Date(), 1, "password" );
            //professorViewModel.insertDisciplina("base de dados", "BD", new Date(), "2020", "descrição da disciplina", 1 );
            //professorViewModel.associarUtilizadorAtualADisciplina( 1 );

            mDisciplina = professorViewModel.getDisciplinasPorUtilizadorId( idDoUtilziadorLogado );

        } catch (ParseException e) {
            e.printStackTrace();
        }
        fragmentContainer = findViewById(R.id.viewPagerToPlaceFragment);
        //mTabLayout = findViewById(R.id.tabLayout);
        mostrarMenuDisciplinas( );
        //mTabLayout.setupWithViewPager(fragmentContainer,true);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker(getResources().getString(R.string.a100));
            }
        }, 1000);
    }

    @Override
    protected void onStop() {
        super.onStop();
        sk.tts.stop();
    }

    private void dadosParaDepoisSerApagado() {

        mDisciplina = new ArrayList<>();
        Disciplina d1 = new Disciplina(1, "Interação Pessoa-Computador", new Date(), R.drawable.ic_ipc_para_depois_remover, "Descrição à sprte"); // TODO: Remover - Objeto apenas criado para testes.
        Disciplina d2 = new Disciplina(2, "Sistemas Distribuídos", new Date(), R.drawable.ic_sd_para_depois_remover, "Segunda descrição à sorte"); // TODO: Remover - Objeto apenas criado para testes.
        mDisciplina.add(d1);
        mDisciplina.add(d2);

        mAvaliacoes = new ArrayList<>();
        Avaliacao a1 = new Avaliacao(1, "Avaliação de maio", new Date());
        Avaliacao a2 = new Avaliacao(2, "Avaliação oral", new Date());
        Avaliacao a3 = new Avaliacao(3, "Teste prático - 2", new Date());
        mAvaliacoes.add(a1);
        mAvaliacoes.add(a2);
        mAvaliacoes.add(a3);


        mQuestoes = new ArrayList<>();
        QuestaoAluno q1 = new QuestaoAluno(1, "Pergunta 1", 1, 0.5, "O que é um sistema distribuído?" );
        QuestaoAluno q2 = new QuestaoAluno(2, "Pergunta que se não souberes, já foste", 3, 10d, "Hoje é sábado?" );
        QuestaoAluno q3 = new QuestaoAluno(3, "Pergunta final", 2, 5d, "O que é uma galinha?" );
        mQuestoes.add(q1);
        mQuestoes.add(q2);
        mQuestoes.add(q3);


        mAlunos = new ArrayList<>();
        Aluno al1 = new Aluno(1, "António Varela", 18 );
        Aluno al2 = new Aluno(2, "Carlos Manuel Rocha Carvalho", 15 );
        Aluno al3 = new Aluno(3, "Cristiano Ronaldo dos Santos Aveiro", 7.77d );
        mAlunos.add(al1);
        mAlunos.add(al2);
        mAlunos.add(al3);



    }

    @Override
    public void onAttachFragment(@NonNull Fragment fragment) {

        if (fragment instanceof PaginaInicialDisciplinaVerAvaliacoes24Fragment) {
            PaginaInicialDisciplinaVerAvaliacoes24Fragment pidaf = (PaginaInicialDisciplinaVerAvaliacoes24Fragment) fragment;
            pidaf.setPaginaInicialDisciplinaVerAvaliacoes24Fragment(this);
        } else if (fragment instanceof PaginaInicialDisciplinaCriarAvaliacao41Fragment) {
            PaginaInicialDisciplinaCriarAvaliacao41Fragment pidaf = (PaginaInicialDisciplinaCriarAvaliacao41Fragment) fragment;
            pidaf.setPaginaInicialDisciplinaVerAvaliacoes24Fragment(this);
        }  else if (fragment instanceof PaginaInicialDisciplinaEditar42Fragment) {
            PaginaInicialDisciplinaEditar42Fragment pidaf = (PaginaInicialDisciplinaEditar42Fragment) fragment;
            pidaf.setPaginaInicialDisciplinaVerAvaliacoes24Fragment(this);
        } else if (fragment instanceof PaginaInicialDisciplinaDesativer44Fragment) {
            PaginaInicialDisciplinaDesativer44Fragment pidaf = (PaginaInicialDisciplinaDesativer44Fragment) fragment;
            pidaf.setPaginaInicialDisciplinaVerAvaliacoes24Fragment(this);
        } else if (fragment instanceof DisciplinaCriada22Fragment) {
            DisciplinaCriada22Fragment pidaf = (DisciplinaCriada22Fragment) fragment;
            pidaf.setDisciplinaCriada22Fragment(this);
        } else if (fragment instanceof AvaliacoesPorDisciplina25Fragment) {
            AvaliacoesPorDisciplina25Fragment pidaf = (AvaliacoesPorDisciplina25Fragment) fragment;
            pidaf.setAvaliacoesPorDisciplina25Fragment(this);
        } else if (fragment instanceof AvaliacaoCriarAvaliacaoSelecionarNome411Fragment) {
            AvaliacaoCriarAvaliacaoSelecionarNome411Fragment pidaf = (AvaliacaoCriarAvaliacaoSelecionarNome411Fragment) fragment;
            pidaf.setAvaliacaoCriarAvaliacaoSelecionarData411(this);
        } else if (fragment instanceof PaginaInicialDisciplinaEditarFormulario43Fragment) {
            PaginaInicialDisciplinaEditarFormulario43Fragment pidaf = (PaginaInicialDisciplinaEditarFormulario43Fragment) fragment;
            pidaf.setPaginaInicialDisciplinaEditarFormulario43Fragment(this);
        } else if (fragment instanceof AvaliacaoVerInscricoes33Fragment) {
            AvaliacaoVerInscricoes33Fragment pidaf = (AvaliacaoVerInscricoes33Fragment) fragment;
            pidaf.setAvaliacaoVerInscricoes33Fragment(this);
        } else if (fragment instanceof AvaliacaoEditarAvaliacao38Fragment) {
            AvaliacaoEditarAvaliacao38Fragment pidaf = (AvaliacaoEditarAvaliacao38Fragment) fragment;
            pidaf.setAvaliacaoEditarAvaliacao38Fragment(this);
        } else if (fragment instanceof AvaliacaoApagarAvaliacao39Fragment) {
            AvaliacaoApagarAvaliacao39Fragment pidaf = (AvaliacaoApagarAvaliacao39Fragment) fragment;
            pidaf.setAvaliacaoApagarAvaliacao39Fragment(this);
        } else if (fragment instanceof AvaliacaoVerQuestoes26Fragment) {
            AvaliacaoVerQuestoes26Fragment pidaf = (AvaliacaoVerQuestoes26Fragment) fragment;
            pidaf.setAvaliacaoVerQuestoes26Fragment(this);
        } else if (fragment instanceof AvaliacaoQuestao27Fragment) {
            AvaliacaoQuestao27Fragment pidaf = (AvaliacaoQuestao27Fragment) fragment;
            pidaf.setAvaliacaoQuestao27Fragment(this);
        } else if (fragment instanceof AvaliacaoVerResultadosMenu341Fragment) {
            AvaliacaoVerResultadosMenu341Fragment pidaf = (AvaliacaoVerResultadosMenu341Fragment) fragment;
            pidaf.setAvaliacaoVerResultadosMenu34Fragment(this);
        } else if (fragment instanceof AvaliacaoVerInscricoesInscrito331Fragment) {
            AvaliacaoVerInscricoesInscrito331Fragment pidaf = (AvaliacaoVerInscricoesInscrito331Fragment) fragment;
            pidaf.setAvaliacaoVerInscricoesInscrito331Fragment(this);
        } else if (fragment instanceof AvaliacaoInscricoesResultados34Fragment) {
            AvaliacaoInscricoesResultados34Fragment pidaf = (AvaliacaoInscricoesResultados34Fragment) fragment;
            pidaf.setAvaliacaoInscricoesResultados34Fragment(this);
        } else if (fragment instanceof AvaliacaoInscricoesResultadosVerCotacao36Fragment) {
            AvaliacaoInscricoesResultadosVerCotacao36Fragment pidaf = (AvaliacaoInscricoesResultadosVerCotacao36Fragment) fragment;
            pidaf.setAvaliacaoInscricoesResultadosVerCotacao36Fragment(this);
        }  else if (fragment instanceof AvalicaoMenuAdicionarQuestao31Fragment) {
            AvalicaoMenuAdicionarQuestao31Fragment pidaf = (AvalicaoMenuAdicionarQuestao31Fragment) fragment;
            pidaf.setAvalicaoMenuAdicionarQuestao31Fragment(this);
        }   else if (fragment instanceof AvaliacaoQuestaoAdicionarQuestao32Fragment) {
            AvaliacaoQuestaoAdicionarQuestao32Fragment pidaf = (AvaliacaoQuestaoAdicionarQuestao32Fragment) fragment;
            pidaf.setAvaliacaoQuestaoAdicionarQuestao32Fragment(this);
        } else if (fragment instanceof AvaliacaoQuestaoEditar30Fragment) {
            AvaliacaoQuestaoEditar30Fragment pidaf = (AvaliacaoQuestaoEditar30Fragment) fragment;
            pidaf.setAvaliacaoQuestaoEditar30Fragment(this);
        } else if (fragment instanceof ProfessorCriarDisciplina201Fragment) {
            ProfessorCriarDisciplina201Fragment pidaf = (ProfessorCriarDisciplina201Fragment) fragment;
            pidaf.setProfessorCriarDisciplina201Fragment(this);
        } else if (fragment instanceof ProfessorCriarDisciplina21Fragment) {
            ProfessorCriarDisciplina21Fragment pidaf = (ProfessorCriarDisciplina21Fragment) fragment;
            pidaf.setProfessorCriarDisciplina21Fragment(this);
        }  else if (fragment instanceof Informacao000Fragment) {
            Informacao000Fragment pidaf = (Informacao000Fragment) fragment;
            pidaf.setInformacao000Fragment(this);
        }

    }


    @Override
    public void fragmentoAtual(Integer fragment) {
        Toast.makeText(getApplicationContext(), "Fragmento: " + fragment, Toast.LENGTH_SHORT).show();
        mFragmentoAtual = fragment;
    }

    // Interface que recebe o id da disciplina que o utilizador selecionou.
    @Override
    public void disciplinaOnClick(Integer idDaDisciplina) {
        Toast.makeText(getApplicationContext(), "Clicou na disciplina " + idDaDisciplina, Toast.LENGTH_SHORT).show();

        int count = 0;

        for (Disciplina d: mDisciplina
             ) {
            if ( d.getId() == idDaDisciplina ) {
                break;
            }
            count++;
        }
        mDisciplinaSelecionada = count;
        mostrarMenuDisciplina( );

    }

    @Override
    public void avaliacaoOnClick(Integer idDaAvaliacaoSelecionada) {

        int count = 0;

        for (Avaliacao a: mAvaliacoes
        ) {
            if ( a.getId() == idDaAvaliacaoSelecionada ) {
                break;
            }
            count++;
        }
        mAvaliacaoSelecionada = count;

        mostrarAvaliacao();
    }

    @Override
    public void alunoOnClick(Integer idDoAlunoSelecionado) {

        int count = 0;

        for (Aluno a: mAlunos
        ) {
            if ( a.getId() == idDoAlunoSelecionado ) {
                break;
            }
            count++;
        }
        mAlunoSelecionado = count;
    }

    @Override
    public void questaoOnClick(Integer idDaQuestaoSelecionada) {
        int count = 0;

        for (QuestaoAluno q: mQuestoes
        ) {
            if ( q.getId() == idDaQuestaoSelecionada ) {
                break;
            }
            count++;
        }
        mQuestaoSelecionada = count;
    }

    @Override
    public void alterarFragmento(Integer fragDestino) throws ParseException {

        switch (fragDestino)
        {
            case -1:
                fragmentContainer.setCurrentItem(1);
                break;
            case 20:
                mostrarMenuDisciplinas( );
                break;
            case 21:
                mostrarCriarAvaliacao( );
                break;
            case 25:
                mostrarAvaliacoes( );
                break;
            case 26:
                mostrarAvaliacoes();
                break;
            case 30:
                mostrarEditarQuestao( );
                break;
            case 31:
                mostrarAvaliacao( );
                break;
            case 32:
                mostrarAdicionarQuestao();
                break;
            case 331:
                mostrarInscricoes();
                break;
            case 34:
                mostrarResultados();
                break;
            case 36:
                mostrarEditarResultados();
                break;
            case 27:
                mostrarQuestoes();
                break;
            case 41:
                mostrarEditarAvaliacao();
                break;
            case 411:
                mostrarSelecionaNome();
                break;
            case 43:
                mostrarEditarDisciplina();
                break;

        }

    }

    private void mostrarCriarAvaliacao() {
        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        CriarDisciplinaAdapter dsa = new CriarDisciplinaAdapter( getSupportFragmentManager() );
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );

    }


    private void mostrarMenuDisciplina( ) {

        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        DisciplinaSwipeAdapter dsa = new DisciplinaSwipeAdapter( getSupportFragmentManager(), mDisciplina.get(mDisciplinaSelecionada) );
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );

    }


    private void mostrarMenuDisciplinas( ) {

        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        DisciplinasSwipeAdapter dsa = new DisciplinasSwipeAdapter( getSupportFragmentManager(), mDisciplina,this );
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );

    }


    private void mostrarAvaliacoes( ) throws ParseException {

        mAvaliacoes = professorViewModel.getAvaliacoesPorByDisciplina(mDisciplina.get(mDisciplinaSelecionada).getId());

        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        AvaliacoesSwipeAdapter dsa = new AvaliacoesSwipeAdapter( getSupportFragmentManager(), mAvaliacoes, mDisciplina.get(mDisciplinaSelecionada) );
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );

    }

    private void mostrarQuestoes( ) {

        mQuestoes = professorViewModel.getQuestoesPorAvaliacao(mAvaliacoes.get(mAvaliacaoSelecionada).getId());

        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        QuestoesSwipeAdapter dsa = new QuestoesSwipeAdapter( getSupportFragmentManager(), mAvaliacoes.get(mAvaliacaoSelecionada), mDisciplina.get(mDisciplinaSelecionada), mQuestoes );
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );

    }

    private void mostrarEditarQuestao( ) {

        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        EditarQuestaoSwipeAdapter dsa = new EditarQuestaoSwipeAdapter( getSupportFragmentManager(), mAvaliacoes.get(mAvaliacaoSelecionada), mDisciplina.get(mDisciplinaSelecionada), mQuestoes.get(mQuestaoSelecionada) );
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );

    }

    private void mostrarSelecionaNome() {

        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        SelecionarNomeSwipeAdapter dsa = new SelecionarNomeSwipeAdapter( getSupportFragmentManager(), mDisciplina.get(mDisciplinaSelecionada));
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );
    }

    private void mostrarAdicionarQuestao() {

        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        AdicionarQuestaoSwipeAdapter dsa = new AdicionarQuestaoSwipeAdapter( getSupportFragmentManager(), mAvaliacoes.get(mAvaliacaoSelecionada), mDisciplina.get(mDisciplinaSelecionada));
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );

    }

    private void mostrarResultados() {
        mAlunos = professorViewModel.getResultadosPorAvaliacaoId(mAvaliacoes.get(mAvaliacaoSelecionada).getId());
        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        ResultadosSwipeAdapter dsa = new ResultadosSwipeAdapter( getSupportFragmentManager(), mAvaliacoes.get(mAvaliacaoSelecionada), mDisciplina.get(mDisciplinaSelecionada), mAlunos);
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );
    }

    private void mostrarEditarResultados() {

        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        CotacaoPorPerguntaSwipeAdapter dsa = new CotacaoPorPerguntaSwipeAdapter( getSupportFragmentManager(), mAvaliacoes.get(mAvaliacaoSelecionada), mDisciplina.get(mDisciplinaSelecionada), mAlunos.get(mAlunoSelecionado), mQuestoes );
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );
    }


    private void mostrarAvaliacao( ) {

        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        AvaliacaoSwipeAdapter dsa = new AvaliacaoSwipeAdapter( getSupportFragmentManager(),  mAvaliacoes.get(mAvaliacaoSelecionada), mDisciplina.get(mDisciplinaSelecionada), true );
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );
    }

    private void mostrarInscricoes() {

        mAlunos = professorViewModel.getInscritosPorAvaliacaoId(mAvaliacoes.get(mAvaliacaoSelecionada).getId());

        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        IncricoesSwipeAdapter dsa = new IncricoesSwipeAdapter( getSupportFragmentManager(), mAvaliacoes.get(mAvaliacaoSelecionada), mDisciplina.get(mDisciplinaSelecionada), mAlunos );
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );
    }

    private void mostrarEditarDisciplina() {

        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        EditarDisciplinaAdapter dsa = new EditarDisciplinaAdapter( getSupportFragmentManager(), mDisciplina.get(mDisciplinaSelecionada) );
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );

    }

    private void mostrarEditarAvaliacao() {
        eliminarTodosOsFragmentosAtuais();
        fragmentContainer.setOffscreenPageLimit(1);
        EditarAvaliacaoSwipeAdapter dsa = new EditarAvaliacaoSwipeAdapter( getSupportFragmentManager(), mDisciplina.get(mDisciplinaSelecionada), mAvaliacoes.get(mAvaliacaoSelecionada) );
        //PagerAdapter wrappedAdapter = new InfinitePagerAdapter(dsa);
        fragmentContainer.setAdapter( dsa );
        fragmentContainer.setCurrentItem( 0 );
    }

    private void eliminarTodosOsFragmentosAtuais() {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }
    }


    @Override
    public void dataSelecionada(Date d) {
        // Guarda a data selecionada.


        dataSelecionada = d;
    }

    @Override
    public void updateOrCreateDisciplina(Disciplina d) throws ParseException {
        // Atualizar a Disciplina passado como parametro na base de dados
        professorViewModel.atualizarDisciplina( d.getId(), d.getNome(), d.getDescricao() );
        mDisciplina = professorViewModel.getDisciplinasPorUtilizadorId( idDoUtilziadorLogado );
        mostrarMenuDisciplina();
    }

    @Override
    public void cancelarOperacao() {
        mClickToCancelDialog.dismissDialog();
    }

    @Override
    public void esperarPorConfirmacao( Integer fragmentoDestino , Integer tipo ) {

        mostrarDialogDeConfirmacao(fragmentoDestino, tipo);

    }

    @Override
    public void nomeSelecionado(String nomeDaAvaliacaoCriada, Integer editar) throws ParseException {

       if (editar == 1) // Editar
       {
           professorViewModel.atualizarAvaliacao( mAvaliacoes.get(mAvaliacaoSelecionada).getId(), nomeDaAvaliacaoCriada, "", dataSelecionada, "","",idDoUtilziadorLogado );
           mAvaliacoes = professorViewModel.getAvaliacoesPorByDisciplina(mDisciplina.get(mDisciplinaSelecionada).getId());
       } else {
           // Guardar avaliacao na base de dados.
           long idAvaliacao = professorViewModel.insertAvaliacao(nomeDaAvaliacaoCriada, "", dataSelecionada, "","",idDoUtilziadorLogado );
           professorViewModel.insertAvaliacao_Disciplina( idAvaliacao, mDisciplina.get(mDisciplinaSelecionada).getId());
           mAvaliacoes = professorViewModel.getAvaliacoesPorByDisciplina(mDisciplina.get(mDisciplinaSelecionada).getId());
       }


    }

    @Override
    public void criarDisciplina(String nome, String descricao) throws ParseException {
        long id = professorViewModel.insertDisciplina(nome, "", new Date(), "", descricao, idDoUtilziadorLogado );
        professorViewModel.insertUtilizador_Disciplina( idDoUtilziadorLogado, id);
        mDisciplina = professorViewModel.getDisciplinasPorUtilizadorId( idDoUtilziadorLogado );
        alterarFragmento(20);
    }

    @Override
    public void adicionarQuestao(String pergunta, Double cotacao) throws ParseException {
        // Adicionar questão à base de dados;
        long id = professorViewModel.insertQuestao( idDoUtilziadorLogado, pergunta, cotacao);
        professorViewModel.insertQuestao_Avaliacao( id, mAvaliacoes.get(mAvaliacaoSelecionada).getId());
        // Obter todas as questoes associadas à avalaiacao selecionada, da base de dados.
        mQuestoes = professorViewModel.getQuestoesPorAvaliacao( mAvaliacoes.get(mAvaliacaoSelecionada).getId() );
    }

    @Override
    public void editarQuestao(String sPergunta, String sCotacao) {

        professorViewModel.editarQuestao(mQuestoes.get(mQuestaoSelecionada).getId(), sPergunta, sCotacao);
        mQuestoes = professorViewModel.getQuestoesPorAvaliacao( mAvaliacoes.get(mAvaliacaoSelecionada).getId() );
    }


    public void buttonBackClick(View view) throws ParseException {

        if ( mFragmentoAtual == 24 || mFragmentoAtual == 41 || mFragmentoAtual == 42 || mFragmentoAtual == 44 || mFragmentoAtual == 411 || mFragmentoAtual == 21) {
            mostrarMenuDisciplinas( );
        } else if ( mFragmentoAtual == 26 || mFragmentoAtual == 31 || mFragmentoAtual == 33 || mFragmentoAtual == 38 || mFragmentoAtual == 39 ) {
            mostrarAvaliacoes();
        } else if ( mFragmentoAtual == 27 || mFragmentoAtual == 331 || mFragmentoAtual == 34 || mFragmentoAtual == 32  )
        {
            mostrarAvaliacoes();
        } else if ( mFragmentoAtual == 25 || mFragmentoAtual == 43 || mFragmentoAtual == 251 )
        {
            mostrarMenuDisciplina( );
        } else if ( mFragmentoAtual == 30 )
        {
            mostrarQuestoes();
        } else if ( mFragmentoAtual == 36 )
        {
            mostrarResultados();
        }  else if ( mFragmentoAtual == 22 || mFragmentoAtual == 201 )
        {
            // TODO: Mostar as páginas anteriores.
        }

    }


    public void mostrarDialogDeConfirmacao( final Integer fragmentoDestino, final Integer tipo ) {

        mClickToCancelDialog = new ClickToCancelOperationDialog( Disciplina20Activity.this, this );
        mClickToCancelDialog.startLoadingDialog();

        // https://stackoverflow.com/a/3039718/13149102
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if ( mClickToCancelDialog.getActive()) {
                    mClickToCancelDialog.dismissDialog();

                    switch (tipo)
                    {
                        case 1:
                            // Desativar/ eliminar disciplina.
                            professorViewModel.desativarDisciplina(mDisciplina.get(mDisciplinaSelecionada).getId(), idDoUtilziadorLogado);
                            try {
                                mDisciplina = professorViewModel.getDisciplinasPorUtilizadorId( idDoUtilziadorLogado );

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 2:
                            // Desativar/ eliminar avalaição.
                            professorViewModel.desativarAvaliacao(mAvaliacoes.get(mAvaliacaoSelecionada).getId());
                            try {
                                mAvaliacoes = professorViewModel.getAvaliacoesPorByDisciplina(mDisciplina.get(mDisciplinaSelecionada).getId());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 3:
                            // Desativar/ eliminar questão.

                            if (mQuestoes != null)
                                professorViewModel.desativarQuestao(mQuestoes.get(mQuestaoSelecionada).getId());
                            mQuestoes = professorViewModel.getQuestoesPorAvaliacao(mDisciplina.get(mDisciplinaSelecionada).getId());

                            break;

                    }
                    // TODO: Remover disciplina
                    try {
                        alterarFragmento( fragmentoDestino );
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 5000);
    }


    public void buttonAudioClick(View view) {

    }

    @Override
    public void voltar(Integer fragmentoAtual) {
        if (fragmentoAtual == 251 )
            mostrarMenuDisciplina();
        else if (fragmentoAtual == 34)
            mostrarAvaliacao();

    }

    @Override
    public void fragmentoAtualInfo(Integer fragment) {
        mFragmentoAtual = 251;
    }
}
