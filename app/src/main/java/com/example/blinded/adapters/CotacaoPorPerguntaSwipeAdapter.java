package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.AvaliacaoInscricoesResultados34Fragment;
import com.example.blinded.fragments.AvaliacaoInscricoesResultadosVerCotacao36Fragment;
import com.example.blinded.model.Aluno;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.QuestaoAluno;

import java.util.ArrayList;

public class CotacaoPorPerguntaSwipeAdapter extends FragmentStatePagerAdapter {

    private Avaliacao mAvaliacao;
    private Disciplina mDisciplina;
    private Aluno mAluno;
    private ArrayList<QuestaoAluno> mQuestoesAluno;

    public CotacaoPorPerguntaSwipeAdapter(FragmentManager fm, Avaliacao a, Disciplina d, Aluno aluno, ArrayList<QuestaoAluno> q) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mAvaliacao = a;
        mDisciplina = d;
        mAluno = aluno;
        mQuestoesAluno = q;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        AvaliacaoInscricoesResultadosVerCotacao36Fragment pagina36 = new AvaliacaoInscricoesResultadosVerCotacao36Fragment();
        Bundle bundle36 = new Bundle();
        bundle36.putParcelable( "dis", mDisciplina );
        bundle36.putParcelable( "ava", mAvaliacao );
        bundle36.putParcelable( "alu", mAluno );
        bundle36.putParcelable( "que", mQuestoesAluno.get(position) );
        pagina36.setArguments(bundle36);
        return pagina36;



    }




    @Override
    public int getCount() {
            return mQuestoesAluno.size();
    }
}
