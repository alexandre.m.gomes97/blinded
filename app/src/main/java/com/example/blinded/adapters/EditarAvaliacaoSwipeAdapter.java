package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.AvaliacaoCriarAvaliacaoSelecionarNome411Fragment;
import com.example.blinded.fragments.PaginaInicialDisciplinaCriarAvaliacao41Fragment;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;

public class EditarAvaliacaoSwipeAdapter extends FragmentStatePagerAdapter {
    private Disciplina mDisciplina;
    private Avaliacao mAvaliacao;

    public EditarAvaliacaoSwipeAdapter(FragmentManager fm, Disciplina d, Avaliacao a) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mDisciplina = d;
        mAvaliacao = a;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                PaginaInicialDisciplinaCriarAvaliacao41Fragment pagina41s = new PaginaInicialDisciplinaCriarAvaliacao41Fragment();
                Bundle bundle41s = new Bundle();
                bundle41s.putParcelable( "dis", mDisciplina );
                bundle41s.putParcelable( "ava", mAvaliacao );
                pagina41s.setArguments(bundle41s);
                return pagina41s;
            case 1:
                AvaliacaoCriarAvaliacaoSelecionarNome411Fragment pagina411s = new AvaliacaoCriarAvaliacaoSelecionarNome411Fragment();
                Bundle bundle411s = new Bundle();
                bundle411s.putParcelable( "dis", mDisciplina );
                bundle411s.putParcelable( "ava", mAvaliacao );
                pagina411s.setArguments(bundle411s);
                return pagina411s;
            default:
                PaginaInicialDisciplinaCriarAvaliacao41Fragment pagina41sn = new PaginaInicialDisciplinaCriarAvaliacao41Fragment();
                Bundle bundle41sn = new Bundle();
                bundle41sn.putParcelable( "dis", mDisciplina );
                bundle41sn.putParcelable( "ava", mAvaliacao );
                pagina41sn.setArguments(bundle41sn);
                return pagina41sn;
        }

    }


    @Override
    public int getCount() {
        return 2;
    }
}
