package com.example.blinded.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.blinded.R;
import com.example.blinded.activity.Disciplina20Activity;
import com.example.blinded.interfaces.ComunicarFragmentActivity;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Disciplina;
import com.example.blinded.model.Speaker;

import java.text.ParseException;

public class AvaliacaoVerQuestoes26Fragment extends Fragment implements View.OnClickListener {

    private ComunicarFragmentActivity cfaInterface;
    private Disciplina mDisciplina;
    private Avaliacao mAvaliacao;
    private Speaker sk;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Através de um bundle vai ser passado para o fragmento, o id, o nome e o id da imagem da disciplina que o utilizador selecionou.
        Bundle bundle = getArguments();
        mDisciplina = bundle.getParcelable("dis");
        mAvaliacao = bundle.getParcelable("ava");
        sk = new Speaker(getContext());

        View rootView = inflater.inflate(R.layout.fragment_avaliacao_ver_questoes_26, container, false);

        final TextView title = rootView.findViewById(R.id.textViewNomeDaDisciplinaDepoisDeCriada_26);
        title.setText( mDisciplina.getNome());

        final TextView titleAval = rootView.findViewById(R.id.textViewNomeDaAvaliacao_26);
        titleAval.setText( mAvaliacao.getNome());

        Button cl = rootView.findViewById(R.id.buttonVerQuestoes26);
        cl.setOnClickListener(this);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sk.useSpeaker("Encontra-se na avaliação" + mAvaliacao.getNome() + ",faça swipe para a esquerda ou direita para ouvir as opções");
            }
        }, 1000);
        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        sk.useSpeaker("Lista de questões");
        cfaInterface.fragmentoAtual(26);
    }

    @Override
    public void onPause() {
        super.onPause();
        sk.tts.stop();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.buttonVerQuestoes26) {
            try {
                cfaInterface.alterarFragmento(27);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    public void setAvaliacaoVerQuestoes26Fragment(Disciplina20Activity disciplina20Activity) {
        cfaInterface = disciplina20Activity;
    }
}
