package com.example.blinded.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.blinded.fragments.AvaliacoesPorDisciplina25Fragment;
import com.example.blinded.fragments.DisciplinaCriada22Fragment;
import com.example.blinded.fragments.Informacao000Fragment;
import com.example.blinded.model.Avaliacao;
import com.example.blinded.model.Avaliacoes;
import com.example.blinded.model.Disciplina;

import java.util.ArrayList;

public class AvaliacoesSwipeAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Avaliacao> mAvaliacoes;
    private Disciplina mDisciplina;

    public AvaliacoesSwipeAdapter(FragmentManager fm, ArrayList<Avaliacao> a, Disciplina d) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mAvaliacoes = a;
        mDisciplina = d;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {


        if ( mAvaliacoes == null || mAvaliacoes.size() == 0 ) {
            Informacao000Fragment pagina000 = new Informacao000Fragment();
            Bundle bundle00 = new Bundle();
            bundle00.putString( "data", "Esta disciplina não tem qualquer avaliação associada!" );
            bundle00.putInt( "fragAtual", 251);
            pagina000.setArguments(bundle00);
            return pagina000;
        } else {
            AvaliacoesPorDisciplina25Fragment pagina25 = new AvaliacoesPorDisciplina25Fragment();
            Bundle bundle25 = new Bundle();
            bundle25.putParcelable( "dis", mDisciplina );
            bundle25.putParcelable( "ava", mAvaliacoes.get(position));
            pagina25.setArguments(bundle25);
            return pagina25;
        }



    }

    @Override
    public int getCount() {

        if (mAvaliacoes == null || mAvaliacoes.size() == 0)
            return 1;
        else
            return mAvaliacoes.size();
    }
}
